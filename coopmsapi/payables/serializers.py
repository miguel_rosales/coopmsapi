from django.utils import timezone
from rest_framework import serializers
from rest_framework.serializers import ValidationError
from coopmsapi.locale.serializers import LocalizedTextSerializer
from coopmsapi.accounting.banking_models import PaymentMethod
from coopmsapi.totalizers.serializers import BalanceDisplayShortSerializer
from coopmsapi.accounting.banking_serializers import PaymentMethodDisplaySerializer

from .models import (
    Invoice,
    InvoiceItem,
    Payment,
    SupplierCategory,
    Supplier)

from .receipts import (
    Receipt,
    ReceiptItem,
    ReceiptPayment)

'''
SUPPLIER CATEGORY serializers
'''

class SupplierCategoryInsertSerializer(serializers.ModelSerializer):
    name = LocalizedTextSerializer(many=True, required=True)

    class Meta:
        model = SupplierCategory
        fields = (
            'name',
            'is_active')

    def create(self, data):
        return SupplierCategory.objects.create(
            supplier_cat_dict=data)

    def update(self, obj, data):
        return SupplierCategory.objects.update(
            supplier_cat=obj,
            supplier_cat_dict=data)


class SupplierCategoryDisplaySerializer(serializers.ModelSerializer):
    name = LocalizedTextSerializer(many=True)

    class Meta:
        model = SupplierCategory
        fields = ('id',
                  'name',
                  'is_active')


class SupplierCategoryDisplayShortSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()

    class Meta:
        model = SupplierCategory
        fields = ('id',
                  'name',
                  'is_active')

    def get_name(self, obj):
        return obj.get_name(
            self.context['request'].query_params.get('locale', None))

'''
SUPPLIER serializers
'''

class SupplierInsertSerializer(serializers.ModelSerializer):

    class Meta:
        model = Supplier
        fields = (
            'name',
            'is_active',
            'rbq_no',
            'address_line1',
            'address_line2',
            'postal_code',
            'phone_number',
            'city',
            'category',
            'supplier_gl_account',
            'supplier_gl_account_pay_over')

    def create(self, data):
        return Supplier.objects.create(supplier_dict=data)

    def update(self, obj, data):
        return Supplier.objects.update(supplier=obj, supplier_dict=data)

class SupplierDisplaySerializer(serializers.ModelSerializer):
    # category = SupplierCategoryDisplayShortSerializer()
    balance = serializers.SerializerMethodField()

    class Meta:
        model = Supplier
        fields = (
            'id',
            'name',
            'is_active',
            'rbq_no',
            'address_line1',
            'address_line2',
            'postal_code',
            'phone_number',
            'city',
            'balance',
            'category',
            'supplier_gl_account',
            'supplier_gl_account_pay_over',
            'created_by',
            'created_date',
            'modified_by',
            'modified_date')

    def get_balance(self, obj):
        return obj.balance.amount


class SupplierDisplayShortSerializer(serializers.ModelSerializer):
    category = SupplierCategoryDisplayShortSerializer()
    balance = serializers.SerializerMethodField()

    class Meta:
        model = Supplier
        fields = (
            'id',
            'name',
            'category',
            'balance',
            'is_active')

    def get_balance(self, obj):
        return obj.balance.amount


class SupplierDisplayMiniSerializer(serializers.ModelSerializer):
    category = SupplierCategoryDisplayShortSerializer()

    class Meta:
        model = Supplier
        fields = (
            'id',
            'name',
            'category',
            'is_active',)


'''
PAYMENT serializers
'''


class PaymentInsertSerializer(serializers.ModelSerializer):
    payment_method = serializers.PrimaryKeyRelatedField(
        queryset=PaymentMethod.objects.all(),
        write_only=True)

    class Meta:
        model = Payment
        fields = (
            'invoice',
            'amount',
            'payment_method',
            'date',
            'cheque_no')

    def create(self, data):
        return Payment.objects.create(payment_dict=data)

    def update(self):
        return None


class PaymentDisplaySerializer(serializers.ModelSerializer):
    payment_method = PaymentMethodDisplaySerializer(many=True)

    class Meta:
        model = Payment
        fields = (
            'id',
            'amount',
            'payment_method',
            'date',
            'cheque_no',
            'created_by',
            'created_date',
            'modified_by',
            'modified_date')


class PaymentDisplayShortSerializer(serializers.ModelSerializer):
    payment_method = PaymentMethodDisplaySerializer(many=True)
    class Meta:
        model = Payment
        fields = (
            'id',
            'amount',
            'payment_method',
            'date',
            'cheque_no')

'''
ITEM serializers
'''

class ItemSerializer(serializers.ModelSerializer):

    line_total = serializers.DecimalField(
                            max_digits=16,
                            decimal_places=2,
                            read_only=True)
    is_canceled = serializers.BooleanField(read_only=True)

    class Meta:
        model = InvoiceItem
        fields = ('id',
                  'is_canceled',
                  'description',
                  'quantity',
                  'price',
                  'line_total',
                  'gl_account',
                  'created_by',
                  'created_date',
                  'modified_by',
                  'modified_date')

    def __init__(self, *args, **kwargs):
        # Instantiate the superclass normally(modify to speed up)
        super(ItemSerializer, self).__init__(*args, **kwargs)

        if kwargs.get('context') is not None:
            #list
            if self.context['request'].method == 'GET' and\
                    self.context.get('pk_present') is False:

                self.fields.pop('created_by')
                self.fields.pop('created_date')
                self.fields.pop('modified_by')
                self.fields.pop('modified_date')

    def create(self, data):
        pass

    def update(self, obj, data):
        pass

'''
INVOICE serializers
'''

class InvoiceSerializer(serializers.ModelSerializer):
    items = ItemSerializer(
        many=True,
        required=False,
        source='invoiceitem_invoice')
    payments = PaymentDisplayShortSerializer(
        many=True,
        read_only=True)
    transaction = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = Invoice
        fields = (
            'id',
            'invoice_no',
            'status',
            'total',
            'balance',
            'supplier',
            'supplier_inv_ref',
            'date',
            'items',
            'payments',
            'note',
            'transaction',
            'modified_by',
            'created_by',
            'modified_date',
            'created_date')


    def __init__(self, *args, **kwargs):
        # Instantiate the superclass normally(modify to speed up)
        super(InvoiceSerializer, self).__init__(*args, **kwargs)
        #list
        if self.context['request'].method == 'GET' and\
                self.context.get('pk_present') is False:
            self.fields.pop('payments')
            self.fields.pop('transaction')
            self.fields.pop('note')
            self.fields.pop('created_by')
            self.fields.pop('created_date')
            self.fields.pop('modified_by')
            self.fields.pop('modified_date')

    def validate_status(self, value):
        if value not in ['DRAFT', 'NOT_PAID'] and\
                self.context['request'].method == 'POST':
            raise serializers.ValidationError(
                'Only Values allowed are DRAFT and NOT_PAID for status field')


    def create(self, data):
        data['created_by'] = self.context['request'].user
        data['created_date'] = timezone.now()
        data['modified_by'] = data['created_by']
        data['modified_date'] = data['created_date']
        for dict_item in data.get('invoiceitem_invoice', []):
            dict_item['created_by'] = data['created_by']
            dict_item['created_date'] = data['created_date']
            dict_item['modified_by'] = data['modified_by']
            dict_item['modified_date'] = data['modified_date']

        return Invoice.objects.create(**data)

    def update(self, obj, data):
        #validate invoice status
        data = self.update_validation(obj, data)
        # prepare audit
        data['modified_by'] = self.context['request'].user
        data['modified_date'] = timezone.now()
        for item_dict in data.get('invoiceitem_invoice', []):
            if item_dict.get('id', None) is None or\
                    item_dict.get('id', None) == 0:
                item_dict['invoice'] = obj
                item_dict['created_by'] = data['modified_by']
                item_dict['created_date'] = data['modified_date']
                item_dict['modified_by'] = data['modified_by']
                item_dict['modified_date'] = data['modified_date']
            else:
                item_dict['modified_by'] = data['modified_by']
                item_dict['modified_date'] = data['modified_date']

        if self.context['request'].method == 'PUT':
            raise Exception('UPDATE SERIALIZER STOP HERE')
        elif self.context['request'].method == 'PATCH':
            raise Exception('PATCH SERIALIZER STOP HERE')

    def update_validation(self, obj, data):
        # invalid status
        if obj.status in [
                Invoice.PAID,
                Invoice.PAR_PAY,
                Invoice.CANCELED]:
            raise serializers.ValidationError(
                'Cannot update with current status')
        # remove status if present
        data.pop('status', None)
        return data

'''
Receipt Serializer
'''

class ReceiptItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReceiptItem
        fields = '__all__'
        extra_kwargs = {
            'id': {'read_only': False, 'required': False},
            'receipt': {'read_only': True}
            }

class ReceiptPaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReceiptPayment
        fields = ('__all__')
        extra_kwargs = {
            'id': {'read_only': False, 'required': False},
            'receipt': {'read_only': True}
            }

class ReceiptSerializer(serializers.ModelSerializer):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if kwargs.get('context', None) is not None:
            if self.context['request'].method == 'GET' and\
                    self.context['pk_present'] is False:
                self.fields.pop('items')
                self.fields.pop('created_by')
                self.fields.pop('created_date')
                self.fields.pop('modified_by')
                self.fields.pop('modified_date')

    items = ReceiptItemSerializer(
        many=True,
        required=True,
        source='receiptitem')
    payment = ReceiptPaymentSerializer(
        many=False,
        required=True,
        source='receiptpayment')
    class Meta:
        model = Receipt
        fields = (
            'id',
            'date',
            'status',
            'accounting_document',
            'is_canceled',
            'note',
            'total',
            'created_by',
            'modified_by',
            'created_date',
            'modified_date',
            'items',
            'payment')
        
        extra_kwargs = {
            'created_by': {'read_only': True},
            'created_date': {'read_only': True},
            'modified_by': {'read_only': True},
            'modified_date': {'read_only': True},
            'accounting_document': {'read_only': True},
            'is_canceled': {'read_only': True},
            'total': {'read_only': True},
            'status': {'required': False}}

    def create(self, data):
        data['created_by'] = self.context['request'].user
        data['created_date'] = timezone.now()
        data['modified_by'] = data['created_by']
        data['modified_date'] = data['created_date']
        status_field = data.get('status', None)
        if status_field is None:
            data['status'] = Receipt.DRAFT
        elif status_field == Receipt.CANCELED:
            raise ValidationError('Cannot create invoices in CANCELED state')
        elif status_field not in [Receipt.DRAFT, Receipt.PAID]:
            raise ValidationError('Invalid status submitted')
        else:
            return Receipt.objects.create(**data)

    def update(self, obj, data):
        if obj.status != Receipt.DRAFT:
            raise ValidationError('Cannot update receipt, status must be DRAFT')
        else:
            data['modified_by'] = self.context['request'].user
            data['modified_date'] = timezone.now()
            for item_dict in data.get('receiptitem', []):
                if item_dict.get('id', None) is None:
                    item_dict['receipt'] = obj
            return Receipt.objects.update(obj, data)
