# # -*- coding: utf-8 -*-
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.response import Response
from rest_framework.views import APIView
from django.contrib.auth import get_user_model
from rest_framework import status, permissions
from rest_framework.parsers import MultiPartParser
from rest_framework import generics
from rest_framework.decorators import detail_route
from oauth2_provider.models import AccessToken
# Http404
from django.http import Http404
# Django shortcuts
from django.shortcuts import get_object_or_404
# rest framework viewsets
from rest_framework import viewsets, mixins
# filters
from rest_framework import filters
# transaction
from django.db import transaction, IntegrityError
# serializers
from .serializers import (
    InvoiceSerializer,
    ItemSerializer,
    PaymentInsertSerializer,
    PaymentDisplaySerializer,
    PaymentDisplayShortSerializer,
    ReceiptSerializer,
    SupplierCategoryDisplaySerializer,
    SupplierCategoryDisplayShortSerializer,
    SupplierCategoryInsertSerializer,
    SupplierDisplaySerializer,
    SupplierDisplayShortSerializer,
    SupplierInsertSerializer)

# coopms
from .models import(
                Invoice,
                InvoiceItem,
                Payment,
                Supplier,
                SupplierCategory)

from .receipts import Receipt


#class InvoiceViewSet(viewsets.ModelViewSet):
#    queryset = Invoice.objects.filter()
#    serializer_class = InvoiceSerializer


 #   @detail_route(methods=['POST'])
 #   def reverse_invoice(self, request, pk=None):
 #       try:
 #           invoice = Invoice.objects.get(id=pk)
 #           invoice.reverse_invoice()
 #           context = {
 #               'request': request,
 #               'pk_present': True,
 #               'detail_route_name': 'reverse_invoice',
 #               'detail_route': True}
 #       except ObjectDoesNotExist() as e:
 #           return Response({'detail':str(e)}, status.HTTP_404_NOT_FOUND)
 #       
 #       return Response({'detail': 'OK'}, status.HTTP_200_OK)
 #
 #   def get_serializer_context(self):
 #       if self.action == 'retrieve' or\
 #               self.action == 'update' or\
 #              self.action == 'partial_update' or\
 #               self.action == 'destroy':
 #           return {'request': self.request, 'pk_present': True}
 #       else:
 #           return {'request': self.request, 'pk_present': False}

  #  def destroy(self, request, pk):
   #     try:
   #         invoice = Invoice.objects.get(id=pk)
   #         Invoice.objects.delete(
   #             invoice=invoice,
   #             user=request.user)
   #         return Response(
   #             status=status.HTTP_204_NO_CONTENT)
   #     except Exception('Failed') as e:
   #         return Response(str(e), status.HTTP_400_BAD_REQUEST)


#class ItemViewSet(viewsets.ModelViewSet):
#    serializer_class = ItemSerializer
#    queryset = InvoiceItem.objects.filter()

 #   def get_serializer_context(self):
 #       if self.action == 'retrieve' or\
 #               self.action == 'update' or\
 #               self.action == 'partial_update' or\
 #               self.action == 'destroy':
 #           return {'request': self.request, 'pk_present': True}
 #       else:
 #           return {'request': self.request, 'pk_present': False}

#    def destroy(self, request, invoice_pk, pk):
#        try:
#            item = InvoiceItem.objects.get(
#                invoice__id=invoice_pk,
#                id=pk)
#            result = InvoiceItem.objects.delete(item)

 #           if result is True:
  #              return Response(
   #                 status=status.HTTP_204_NO_CONTENT)
    #        else:
     #           return Response(
      #              data={'detail': result},
      #              status=status.HTTP_400_BAD_REQUEST)
            
      #  except InvoiceItem.DoesNotExist:
       #     return Response(
        #        status=status.HTTP_404_NOT_FOUND)



#class PaymentViewSet(viewsets.ModelViewSet):
#    serializer_class = PaymentDisplaySerializer
#    queryset = Payment.objects.filter()

#    def create(self, request, invoice_pk):
#        try:
#            invoice = Invoice.objects.get(id=invoice_pk)
            
#            serializer = PaymentInsertSerializer(request.data)
#            if serializer.is_valid():
#                serializer.validated_data['by_user'] = request.user
#                serializer.validated_data['invoice'] = invoice
#                payment = serializer.create(data=serializer.validated_data)

#                return Response(
#                    data=PaymentDisplaySerializer(payment).data,
 #                   status=status.HTTP_201_CREATED)
  #          else:
  #              return Response(
  #                  data=serializer.errors,
  #                  status=status.HTTP_400_BAD_REQUEST)

#        except IntegrityError as e:
#            return Response(
#                data={'detail': str(e)},
#                status=status.HTTP_400_BAD_REQUEST)
#        except Invoice.DoesNotExist:
#            return Response(
#                data={'detail': 'root document not found'},
#                status=status.HTTP_404_NOT_FOUND)

 #   def list(self, request, invoice_pk):
  #      serializer = self.list_serialize_items(
   #         request, invoice_pk)

    #    return Response(
    #        data=serializer.data,
    #        status=status.HTTP_200_OK)

#    def destroy(self, request, pk):
#        return Response(
#            data={'detail': 'method not allowed'},
#            status=status.HTTP_405_METHOD_NOT_ALLOWED)

 #   def update(self, request, pk):
 #       return Response(
  #          data={'detail': 'method not allowed'},
  #          status=status.HTTP_405_METHOD_NOT_ALLOWED)

#    def partial_update(self, request, pk):
 #       return Response(
 #           data={'detail': 'method not allowed'},
 #           status=status.HTTP_405_METHOD_NOT_ALLOWED)

  #  def get_serializer_class(self):
   #     if self.action == 'list' or\
    #            self.action == 'retrieve':

     #       return PaymentDisplaySerializer
     #   else:
     #       return PaymentInsertSerializer

#    def list_serialize_payments(self, _type, request, _id):

#        queryset = self.filter_queryset(
#            Payment.objects.filter(invoice__id=_id))

#        page = self.paginate_queryset(queryset)

#        if page is not None:
 #           serializer = PaymentDisplayShortSerializer(
  #              page,
   #             many=True,
   #             context={'request': request})
    #        return self.get_paginated_response(serializer.data)

     #   serializer = PaymentDisplayShortSerializer(
     #       queryset,
     #       many=True,
     #       context={'request', request}
     #       )
      #  return serializer
      

class ReceiptViewSet(viewsets.ModelViewSet):
    """
    Receipt ViewSet
    """
    queryset = Receipt.objects.all()
    serializer_class = ReceiptSerializer

    def get_serializer_context(self):
        if self.action == 'retrieve' or\
                self.action == 'update' or\
                self.action == 'partial_update' or\
                self.action == 'destroy':
            return {'request': self.request, 'pk_present': True}
        else:
            return {'request': self.request, 'pk_present': False}

    @detail_route(methods=['POST'])
    def post_receipt(self, request, pk=None):
        try:
            receipt = Receipt.objects.get(id=pk)
            if receipt.status != Receipt.DRAFT:
                return Response(
                    {'detail': 'Receipt already posted'},
                    status.HTTP_400_BAD_REQUEST)
            receipt.modified_by = request.user
            receipt.modified_date = timezone.now()
            receipt.create_init_entries()
            receipt.save()
            return Response(
                {'detail': 'status changed'},
                status.HTTP_200_OK)
        except ObjectDoesNotExist as e:
            return Response(
                {'detail': str(e)},
                status.HTTP_404_NOT_FOUND)

    @detail_route(methods=['POST'])
    def reverse_receipt(self, request, pk=None):
        try:
            receipt = Receipt.objects.get(id=pk)
            if receipt.status == Receipt.CANCELED:
                return Response(
                    {'detail': 'Receipt already canceled'},
                    status.HTTP_400_BAD_REQUEST)

            elif receipt.status == Receipt.DRAFT:
                return Response(
                    {'detail': 'DRAFT status does not require reversal'},
                    status.HTTP_400_BAD_REQUEST)

            elif receipt.status == Receipt.PAID:
                receipt.modified_by = request.user
                receipt.modified_date = timezone.now()
                receipt.reverse_receipt()
                return Response(
                    {'detail': 'status changed'},
                    status.HTTP_200_OK)

            else:
                return Response(
                    {'detail': 'invalid status recorded contact software provider'},
                    status.HTTP_400_BAD_REQUEST)

        except ObjectDoesNotExist as e:
            return Response(
                {'detail': str(e)},
                status.HTTP_404_NOT_FOUND)
'''
{
  "note": "bla bla bla",
  "status": "DRAFT",
  "date": "2016-01-01",
  "items": [
    {"gl_account": 100, "description": "test test 1", "amount": 12.30}
  ],
  "payment": {"payment_method": 1, "amount":12.30}
}
'''

class SupplierCategoryViewSet(viewsets.ModelViewSet):
    """
    Supplier type
    """
    queryset = SupplierCategory.objects.all().order_by('id')
    serializer_class = SupplierCategoryDisplaySerializer

    def create(self, request):
        serializer = SupplierCategoryInsertSerializer(data=request.data)
        if serializer.is_valid():
            serializer.validated_data['by_user'] = request.user
            new_supplier_cat = serializer.create(data=serializer.validated_data)
            return Response(
                status=status.HTTP_201_CREATED,
                data=SupplierCategoryDisplaySerializer(new_supplier_cat).data)
        else:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=serializer.errors)

    def update(self, request, pk):
        try:
            supplier_category = SupplierCategory.objects.get(id=pk)
            serializer = SupplierCategoryInsertSerializer(data=request.data)
            if serializer.is_valid():
                serializer.validated_data['by_user'] = request.user
                new_supplier_cat = serializer.update(
                    obj=supplier_category,
                    data=serializer.validated_data)
                return Response(
                    status=status.HTTP_201_CREATED,
                    data=SupplierCategoryDisplaySerializer(new_supplier_cat).data)
            else:
                return Response(
                    data=serializer.errors,
                    status=status.HTTP_400_BAD_REQUEST)
        except SupplierCategory.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def destroy(self, request, pk):
        try:
            supplier_category = SupplierCategory.objects.get(id=pk)
            if SupplierCategory.objects.delete(supplier_category, request.user) is True:
                return Response(status=status.HTTP_204_NO_CONTENT)
            else:
                return Response(
                    status=status.HTTP_200_OK,
                    data={'detail':'SUPPLIERS_USING_CAT'})

        except SupplierCategory.DoesNotExist:
            return Response(
                status=status.HTTP_404_NOT_FOUND)


    def get_serializer_class(self):
        if self.action == 'retrieve':
            return SupplierCategoryDisplaySerializer
        elif self.action == 'list':
            return SupplierCategoryDisplayShortSerializer
        elif self.action == 'destroy':
            return SupplierCategoryDisplaySerializer
        elif self.action == 'create':
            return SupplierCategoryInsertSerializer
        elif self.action == 'update':
            return SupplierCategoryInsertSerializer
        else:
            return SupplierCategoryDisplaySerializer


class SupplierViewSet(viewsets.ModelViewSet):
    """
    Supplier
    """
    queryset = Supplier.objects.all()
    serializer_class = SupplierDisplaySerializer

    def create(self, request):
        serializer = SupplierInsertSerializer(data=request.data)
        if serializer.is_valid():
            serializer.validated_data['by_user'] = request.user
            new_supplier = serializer.create(data=serializer.validated_data)
            return Response(
                status=status.HTTP_201_CREATED,
                data=SupplierDisplaySerializer(new_supplier).data)
        else:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=serializer.errors)

    def update(self, request, pk):
        try:
            supplier = Supplier.objects.get(id=pk)
            serializer = SupplierInsertSerializer(data=request.data)
            if serializer.is_valid():
                serializer.validated_data['by_user'] = request.user
                new_supplier = serializer.update(
                    obj=supplier,
                    data=serializer.validated_data)
                return Response(
                    status=status.HTTP_200_OK,
                    data=SupplierDisplaySerializer(
                        new_supplier,
                        context={'request': request}).data)
            else:
                return Response(
                    status=status.HTTP_400_BAD_REQUEST,
                    data=serializer.errors)

        except Supplier.DoesNotExist:
            return Response(
                status=status.HTTP_404_NOT_FOUND)


    def get_serializer_class(self):
        if self.action == 'list':
            return SupplierDisplayShortSerializer
        elif self.action == 'retrieve':
            return SupplierDisplaySerializer
        elif self.action == 'create':
            return SupplierInsertSerializer
        elif self.action == 'update':
            return SupplierInsertSerializer
        else:
            return SupplierDisplaySerializer

