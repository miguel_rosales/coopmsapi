from django.conf.urls import url, include
# viewset
from rest_framework_nested import routers
from coopmsapi.payables.views import (
#                        InvoiceViewSet,
#                        ItemViewSet,
#                        PaymentViewSet,
                        ReceiptViewSet,
                        SupplierCategoryViewSet,
                        SupplierViewSet)

from coopmsapi.payables.invoice_views import (APInvoiceViewSet)
from coopmsapi.payables.payment_views import (APPaymentViewSet)


invoice_router = routers.SimpleRouter()
invoice_router.register(
                        r'invoices',
                        APInvoiceViewSet,
                        base_name='invoices')


payment_router = routers.SimpleRouter()
payment_router.register(
                        r'payments',
                        APPaymentViewSet,
                        base_name='payments')

supplier_router = routers.SimpleRouter()
supplier_router.register(
                        r'suppliers',
                        SupplierViewSet,
                        base_name='suppliers')

supplier_category_router = routers.SimpleRouter()
supplier_category_router.\
            register(
                    r'supplier_categories',
                    SupplierCategoryViewSet,
                    base_name='supplier_categories')

receipt_router = routers.SimpleRouter()
receipt_router.\
    register(
        r'receipts',
        ReceiptViewSet,
        base_name='receipts')

urlpatterns = [
    url(r'^', include(invoice_router.urls)),
    url(r'^', include(payment_router.urls)),
    url(r'^', include(receipt_router.urls)),
    url(r'^', include(supplier_router.urls)),
    url(r'^', include(supplier_category_router.urls)),
]
