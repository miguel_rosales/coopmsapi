from django.contrib import admin
from .models import Invoice, InvoiceItem, Payment, SupplierCategory, Supplier

admin.site.register(Invoice)
admin.site.register(InvoiceItem)
admin.site.register(Payment)
admin.site.register(SupplierCategory)
admin.site.register(Supplier)