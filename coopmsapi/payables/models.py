from collections import defaultdict, Counter
from django.utils import timezone
from django.db import models, IntegrityError
from django.db.models import Sum, Value as V
from django.db.models.functions import Coalesce
# django F model to compare to fields in the same record.
from coopmsapi.settings import AUTH_USER_MODEL
# from django.shortcuts import get_object_or_404
from coopmsapi.locale.models import LocalizedText
from coopmsapi.totalizers.models import Balance
from coopmsapi.accounting.models import (
                                GLAccount,
                                Transaction,
                                GLEntry,
                                Setting as AccSetting)
from coopmsapi.accounting.banking_models import PaymentMethod
from sequences import get_next_value


'''
SUPPLIER MANAGER
'''

class SupplierManager(models.Manager):

    def create(self, supplier_dict):

        acc_setting = AccSetting.objects.get()
        supplier_gl_account = supplier_dict.get('supplier_gl_account', None)
        supplier_gl_account_pay_over = supplier_dict.get('supplier_gl_account_pay_over', None)

        if supplier_gl_account is None:
            # verify
            supplier_gl_account = acc_setting.supplier_gl_account
        
        if supplier_gl_account_pay_over is None:
            # verify
            supplier_gl_account_pay_over = acc_setting.supplier_gl_account_pay_over


        new_balance = Balance.objects.create(balance_type='SUPPLIER')
        new_supplier = Supplier(
            name=supplier_dict['name'],
            rbq_no=supplier_dict.get('rbq_no', None),
            address_line1=supplier_dict.get('address_line1', None),
            address_line2=supplier_dict.get('address_line2', None),
            postal_code=supplier_dict.get('postal_code', None),
            phone_number=supplier_dict.get('phone_number', None),
            city=supplier_dict.get('city', None),
            balance=new_balance,
            category=supplier_dict.get('category'),
            supplier_gl_account=supplier_gl_account,
            supplier_gl_account_pay_over=supplier_gl_account_pay_over,
            created_by=supplier_dict['by_user'],
            modified_by=supplier_dict['by_user'])
        new_supplier.save()
        return new_supplier

    def update(self, supplier, supplier_dict):
        acc_setting = AccSetting.objects.get()
        supplier_gl_account = supplier_dict.get('supplier_gl_account', None)
        supplier_gl_account_pay_over = supplier_dict.get('supplier_gl_account_pay_over', None)

        if supplier_gl_account is None:
            # verify
            supplier_gl_account = acc_setting.supplier_gl_account
        
        if supplier_gl_account_pay_over is None:
            # verify
            supplier_gl_account_pay_over = acc_setting.supplier_gl_account_pay_over

        supplier.name = supplier_dict['name']
        supplier.rbq_no = supplier_dict.get('rbq_no', None)
        supplier.address_line1 = supplier_dict.get('address_line1', None)
        supplier.address_line2 = supplier_dict.get('address_line2', None)
        supplier.postal_code = supplier_dict.get('postal_code', None)
        supplier.phone_number = supplier_dict.get('phone_number', None)
        supplier.city = supplier_dict.get('city', None)
        supplier.category = supplier_dict.get('category', None)
        supplier.supplier_gl_account = supplier_gl_account
        supplier.supplier_gl_account_pay_over = supplier_gl_account_pay_over
        supplier.created_by = supplier_dict['by_user']
        supplier.modified_by = supplier_dict['by_user']

        supplier.save()

        return supplier

    def delete(self, supplier):
        if Invoice.objects.filter(supplier=supplier) == 0:
            supplier.delete()
        else:
            raise IntegrityError


'''
SUPPLIER CATEGORY MANAGER
'''

class SupplierCategoryManager(models.Manager):
    def create(self, supplier_cat_dict):
        new_supplier_cat = SupplierCategory(
            created_by=supplier_cat_dict['by_user'],
            modified_by=supplier_cat_dict['by_user'])
        new_supplier_cat.save()
        for localized_text in supplier_cat_dict['name']:
            new_text = LocalizedText.objects.create(
                locale=localized_text['locale'],
                text=localized_text['text'])
            new_supplier_cat.name.add(new_text)

        return new_supplier_cat

    def update(self, supplier_cat, supplier_cat_dict):
        supplier_cat.name.clear()
        for localized_text in supplier_cat_dict['name']:
            new_text = LocalizedText.objects.create(
                locale=localized_text['locale'],
                text=localized_text['text'])
            supplier_cat.name.add(new_text)

        supplier_cat.modified_by = supplier_cat_dict['by_user']
        supplier_cat.is_active = supplier_cat_dict.get(
            'is_active',
            supplier_cat.is_active)
        supplier_cat.save()
        return supplier_cat

    def delete(self, supplier_cat, user):
        if Supplier.objects.filter(category=supplier_cat).count() > 0:
            supplier_cat.is_active = False
            supplier_cat.modified_by = user
            supplier_cat.save()
            return True
        else:
            supplier_cat.delete()
            return True


'''
INVOICE MANAGER
'''


class InvoiceManager(models.Manager):

    def create(self, invoice_dict):
        invoice_header = self.create_invoice_header(
            invoice_header_dict=invoice_dict)

        for item_dict in invoice_dict['items']:
            item_dict['by_user'] = invoice_dict['by_user']
            InvoiceItem.objects.create(
                item_dict=item_dict,
                invoice=invoice_header,
                transaction=invoice_header.transaction)

        total = InvoiceItem.objects.filter(
            invoice=invoice_header)\
            .aggregate(total=Sum('line_total'))
        invoice_header.total = total['total']
        invoice_header.balance = total['total']
        invoice_header.save()

        return invoice_header

    def create_invoice_header(self, invoice_header_dict):
        if invoice_header_dict['status'] == Invoice.NOT_PAID:
            invoice_header_dict['transaction'] = Transaction(
                    transaction_date=invoice_header_dict['created_by'],
                    transaction_type='INV_AP',
                    linked_transaction=None,
                    created_by=invoice_header_dict['created_by'],
                    modified_by=invoice_header_dict['modified_by'])
            invoice_header_dict['transaction'].save()
        invoice_header_dict['gl_account_liability'] =\
            invoice_header_dict['supplier'].supplier_gl_account

        new_invoice_header = Invoice(**invoice_header_dict)

        # Assignation of invoice_no
        if new_invoice_header.status == Invoice.NOT_PAID:
            new_invoice_header.invoice_no = get_next_value(
                'payables_invoice_no',
                initial_value=10000)
        new_invoice_header.save()
        return new_invoice_header

    def update(self, invoice, invoice_dict):
        self.check_for_payments_or_credits(invoice=invoice)

        if invoice.status == 'DRAFT' and\
                invoice_dict['status'] == 'DRAFT':
            self.update_draft_to_draft(invoice, invoice_dict)
        elif invoice.status == 'DRAFT' and\
                invoice_dict['status'] == 'NOT_PAID':
            self.update_draft_to_not_paid(invoice,invoice_dict)
        elif invoice.status == 'NOT_PAID' and\
                invoice_dict['status'] == 'NOT_PAID':
            self.update_not_paid_to_not_paid(invoice,invoice_dict)
        return None

    def update_draft_to_draft(self, invoice, invoice_dict):
        invoice.status = invoice_dict.get('status', invoice.status)
        invoice.supplier = invoice_dict.get('supplier', invoice.supplier)
        invoice.gl_account_liability = invoice_dict.get(
            'supplier',
            invoice.supplier).supplier_gl_account
        invoice.supplier_inv_ref = invoice_dict.get(
            'supplier_inv_ref',
            invoice.supplier_inv_ref)
        invoice.note = invoice_dict.get('note', invoice.note)

        current_items = InvoiceItem.objects.filter(invoice=invoice)

        # variables
        new_items = []
        modified_items = []
        modified_items_dict = []
        to_delete_items = []

        # differentiate new_items, items that changed and items to delete
        new_items,\
        modified_items,\
        modified_items_dict,\
        to_delete_items = self.get_items(
                submitted_items=invoice_dict['items'],
                original_items=current_items)

        for item in new_items :
            InvoiceItem.objects.create(
                invoice=invoice,
                item_dict=item)

        for item in modified_items:
            for item_dict in modified_items_dict:
                if item.id == item_dict['id']:
                    InvoiceItem.objects.update(
                        item,
                        item_dict)

        for item in to_delete_items:
            InvoiceItem.objects.delete(item)

    def update_draft_to_not_paid(self, invoice, invoice_dict):
        invoice.status = invoice_dict.get('status', invoice.status)
        invoice.supplier = invoice_dict.get('supplier', invoice.supplier)
        invoice.gl_account_liability = invoice_dict.get(
            'supplier',
            invoice.supplier).supplier_gl_account
        invoice.supplier_inv_ref = invoice_dict.get(
            'supplier_inv_ref',
            invoice.supplier_inv_ref)
        invoice.note = invoice_dict.get('note', invoice.note)
        invoice.invoice_no = get_next_value(
                'payables_invoice_no',
                initial_value=10000)

        current_items = InvoiceItem.objects.filter(invoice=invoice)

        # differentiate new_items, items that changed and items to delete
        new_items,\
        modified_items,\
        modified_items_dict,
        to_delete_items = self.get_items(
                submitted_items=invoice_dict['items'],
                original_items=current_items)

        for item in new_items :
            print('for item in new_items :')
            #InvoiceItem.objects.create(
             #   invoice=invoice,
              #  item_dict=item)

        for item in modified_items:
            print('!!!for item in modified_items:')
            print(item.id)
            #for item_dict in modified_items_dict:
             #   if item.id == item_dict['id']
              #      InvoiceItem.objects.update(
               #         item,
                #        item_dict)

        for item in to_delete_items:
            print('!!!for item in to_delete_items:')
            print(item.id)
            #InvoiceItem.objects.delete(item)

    def update_not_paid_to_not_paid(self, invoice, invoice_dict):
        invoice.supplier = invoice_dict.get('supplier', invoice.supplier)
        invoice.gl_account_liability = invoice_dict.get(
            'supplier',
            invoice.supplier).supplier_gl_account
        invoice.supplier_inv_ref = invoice_dict.get(
            'supplier_inv_ref',
            invoice.supplier_inv_ref)
        invoice.note = invoice_dict.get('note', invoice.note)

        current_items = InvoiceItem.objects.filter(invoice=invoice)

        # differentiate new_items, items that changed and items to delete
        new_items,\
        modified_items,\
        modified_items_dict,
        to_delete_items = self.get_items(
                submitted_items=invoice_dict['items'],
                original_items=current_items)

        for item in new_items :
            print('!!!for item in new_items:')
            #InvoiceItem.objects.create(
             #   invoice=invoice,
              #  item_dict=item)

        for item in modified_items:
            print('!!!for item in modified_items:')
            #for item_dict in modified_items_dict:
             #   if item.id == item_dict['id']
              #      InvoiceItem.objects.update(
               #         item,
                #        item_dict)

        for item in to_delete_items:
            print('!!!for item in to_delete_items:')
            print(item.id)
            # InvoiceItem.objects.delete(item)

    def check_for_payments_or_credits(self, invoice):
        if invoice.status in ('PAR_PAY', 'PAID'):
            raise IntegrityError('delete payments first')

        if Payment.objects.filter(
            invoice=invoice,
            is_canceled=False).count() > 0:
            raise IntegrityError('delete payments first')

        elif Credit.objects.filter(
            invoice=invoice,
            is_canceled=False).count() > 0:
            raise IntegrityError('delete invoice credits first')

    def get_items(self, submitted_items, original_items):
        new_items = []
        to_delete_items = []
        modified_items = []
        modified_items_dict = []
        first_level_items = [] # items after removing to_delete items
        second_level_items = [] # second_level_items after removing updated

        for item in submitted_items:
            print('Here is the value of item.id')
            print(item)
            if item.id is None:
                new_items.append(item)
            else:
                first_level_items.append(item)
        
        for item in original_items:
            if self.flag_for_deletion(item, first_level_items) is True:
                to_delete_items.append(item)
            else:
                second_level_items.append(item)

        for item in second_level_items:
            modification_dict = self.flag_for_modification(
                item,
                first_level_items)

            if modification_dict is not None:
                modified_items.append(item)
                modified_items_dict.append(modification_dict)

        return new_items, modified_items, modified_items_dict, to_delete_items


    def flag_for_deletion(self, item, submitted_items):
        for current_item in submitted_items:
            if current_item.id == item.id and\
                    current_item.is_canceled == True and\
                    item.is_canceled == False:
                return True

            elif current_item.id == item.id and\
                    current_item.is_canceled == False and\
                    item.is_canceled == False:
                return False
        return True

    def flag_for_modification(self, item, original_items):
        for current_item in original_items:
            if current_item.description != item.description or\
                    current_item.gl_account != item.gl_account or\
                    current_item.quantity != item.quantity or\
                    current_item.price != item.price:
                return True
        return False

    def delete(self, invoice, user):
        if invoice.status == 'DRAFT':
            InvoiceItem.objects.filter(invoice=invoice).delete()
            invoice.delete()
            return True





'''
*** INVOICE ITEM MANAGER ***
Used in serializers only
'''


class InvoiceItemManager(models.Manager):

    def create(
            self,
            item_dict,
            invoice=None,
            update_inv=False,
            transaction=None):

        if invoice is None:
            invoice = item_dict['invoice']

        if invoice.status == 'PAR_PAY' or\
                invoice.status == 'PAID':
            raise IntegrityError('cannot add items when invoice has payments')

        item = InvoiceItem(
            invoice=invoice,
            is_canceled=False,
            description=item_dict['description'],
            gl_account=item_dict['gl_account'],
            quantity=item_dict['quantity'],
            price=item_dict['price'],
            line_total=item_dict['quantity']*item_dict['price'],
            created_by=item_dict['by_user'],
            modified_by=item_dict['by_user'])
        item.save()

        if update_inv is True:
            invoice.balance += item.line_total
            invoice.total += item.line_total
            invoice.modified_by = item_dict['by_user']
            invoice.save()

        if transaction is None and item.invoice.status == 'NOT_PAID':
            transaction = Transaction(
                transaction_date=invoice.date,
                transaction_type='MII_AP',
                linked_transaction=invoice.transaction,
                created_by=item_dict['user'],
                modified_by=item_dict['user'])
            transaction.save()

        if item.invoice.status == 'NOT_PAID':
            self.create_item_asset(
                        item=item,
                        transaction=transaction)
        return item

    def update(self, item, modified_item, transaction=None):

        if item.invoice.status == 'DRAFT':
            self.update_item(item, modified_item)
            self.update_invoice()
        elif item.invoice.status == 'NOT_PAID':
            if self.accounting_redistribution_required(
                        item=item,
                        modified_item=modified_item) is True:
                if transaction is None:
                    transaction = Transaction(
                        transaction_date=timezone.now(),
                        transaction_type='MII_AR',
                        linked_transaction=item.invoice.transaction,
                        created_by=modified_item['by_user'],
                        modified_by=modified_item['by_user'])
                self.reverse_item_asset(item, transaction)
                self.update_item(item, modified_item)
                self.create_item_asset(item, modified_item)
                return item
            else:
                return self.update_item(item, modified_item)

        elif item.invoice.status == 'PAID' or\
                item.invoice.status == 'PAR_PAY':
            raise IntegrityError('invoice paid, delete payments first')

    def update_item(self, item, modified_item):

        item.is_canceled = modified_item['is_canceled']
        item.name = self.get_item_name(modified_item['product'])
        item.product = modified_item['product']
        item.quantity = modified_item['quantity']
        item.price = modified_item['price']
        item.line_total = modified_item.price['quantity'] *\
            modified_item['price']
        item.modified_by = modified_item['user']

        item.save()

    def accounting_redistribution_required(self, item, modified_item):
        if item.is_canceled is True:
            raise IntegrityError('item is canceled')

        if (item.is_canceled is False and
                modified_item['is_canceled'] is True) or\
                (item.line_total != modified_item['price'] *
                    modified_item['quantity']):
            return True
        else:
            return False

    def reverse_item_asset(self, item, transaction):

        if item.invoice.internal_type == 'INTERNAL':
            settings = AccSetting.objects.get()
            second_gl_account = \
                settings.member_default_liability_gl_account
        elif item.invoice.internal_type == 'RENT':
            settings = AccSetting.objects.get()
            second_gl_account = settings.ar_rent_default_asset_gl_account
        elif item.invoice.internal_type == 'EXTERNAL':
            second_gl_account = item.invoice.internal_type
        else:
            raise Exception('internal_type not implemented yet')

        GLEntry.objects.doubleEntry(
            transaction=transaction,
            date=item.invoice.date,
            first_gl_account=item.gl_account,
            first_amount=-item.line_total,
            second_gl_account=second_gl_account,
            second_amount=-item.line_total)

    def create_item_asset(self, item, transaction):

        GLEntry.objects.doubleEntry(
            transaction=transaction,
            date=item.invoice.date,
            first_gl_account=item.gl_account,
            first_amount=item.line_total,
            second_gl_account=item.invoice.gl_account_liability,
            second_amount=-item.line_total)

    def delete(self, item, user, transaction=None):

        try:
            if item.invoice.status == 'DRAFT':
                item.delete()
                return True

            if item.is_canceled is True:
                return True

            if transaction is None:
                transaction = Transaction(
                                transaction_date=timezone.now(),
                                transaction_type='MII_AP',
                                linked_transaction=item.invoice.transaction,
                                created_by=user,
                                modified_by=user,
                                created_date=timezone.now(),
                                )
                transaction.save()

            item.is_canceled
            item.modified_by = user
            item.save(update_fields=['is_canceled', 'modified_by'])

            item.invoice.total -= item.price*item.quantity
            item.invoice.balance += item.price*item.quantity
            item.invoice.save(update_fields=['total', 'balance'])

            self.distributeGLInvoiceItem(
                supplier=item.invoice.supplier,
                gl_account=item.gl_account,
                transaction=transaction,
                amount=-item.price*item.quantity,
                date=item.invoice.date)
            return True
        except:
            return False

    def update(
            self,
            item,
            gl_account,
            description,
            quantity,
            price,
            user,
            trasaction=None):

        item.gl_account = gl_account
        item.description = description
        item.quantity = quantity
        item.price = price
        item.line_total = quantity*price
        item.modified_by = user
        item.save()

        if item.invoice.status == Invoice.DRAFT:
            return item

        if trasaction is None:
            transaction = Transaction(
                            transaction_date=timezone.now(),
                            transaction_type='MII_AP',
                            linked_transaction=item.invoice.transaction,
                            created_by=user,
                            modified_by=user,
                            created_date=timezone.now(),
                            )
            transaction.save()

        self.distributeGLInvoiceItem(
                                supplier=item.invoice.supplier,
                                gl_account=item.gl_account,
                                transaction=transaction,
                                amount=-item.line_total,
                                date=item.invoice.date)
        return item

    def distributeGLInvoiceItem(
                                self,
                                supplier,
                                gl_account,
                                transaction,
                                amount,
                                date=timezone.now()):

        # Three step 1)Sister_account, Supplier,
        # Settings default ap

        if gl_account.sister_account is not None:
            second_gl_account = gl_account.sister_account
        else:
            if supplier.gl_account_liability is not None:
                second_gl_account = supplier.gl_account_liability
            else:
                second_gl_account = Setting.objects.get().default_ap

        GLEntry.objects.doubleEntry(
                                    transaction=transaction,
                                    date=date,
                                    first_gl_account=gl_account,
                                    first_amount=amount,
                                    second_gl_account=second_gl_account,
                                    second_amount=-amount,
                                    )


'''
*** PAYMENT MANAGER ***
'''


class PaymentManager(models.Manager):

    def create(self,
               invoice,
               amount,
               date,
               payment_type,
               cheque_no,
               user):

        current_transaction = Transaction.\
                                objects.\
                                create(
                                    transaction_date=timezone.now(),
                                    transaction_type='PAY_AP',
                                    linked_transaction=invoice.trasaction,
                                    created_by=user,
                                    modified_by=user)

        current_payment = Payment(
                            invoice=invoice,
                            amount=amount,
                            transaction=current_transaction,
                            date=date,
                            payment_type=payment_type,
                            cheque_no=cheque_no,
                            modified_by=user,
                            created_by=user)

        current_payment.save()

        self.distributePaymentOnItems(current_payment)

        if (invoice.amount-invoice.balance) < amount:
            self.insertSupplierCredit

        self.adjustInvoiceAndSupplierCredit(current_payment)

        return current_payment

    def distributePaymentOnItems(self, payment):

        possible_payments = InvoiceItem.objects.\
            filter(invoice=payment.invoice).\
            values('id').\
            annotate(
                    to_pay=F('line_total') - Coalesce(Sum(
                                'paymentitemdistribution__amount'),
                                V(0))).order_by('-to_pay')

        amount_to_distribute = payment.amount

        for item_payment in possible_payments:
            max_item_payment_possible = item_payment.to_pay
            if max_item_payment_possible <= amount_to_distribute and\
                    amount_to_distribute > 0 and\
                    max_item_payment_possible > 0:
                new_payment_distribution = PaymentItemDistribution(
                                payment=payment,
                                item=item_payment,
                                amount=max_item_payment_possible
                                )
                # insert here gl_distribution
                amount_to_distribute -= max_item_payment_possible
            elif max_item_payment_possible <= amount_to_distribute and\
                    amount_to_distribute > 0 and\
                    max_item_payment_possible > 0:
                new_payment_distribution = PaymentItemDistribution(
                                payment=payment,
                                item=item_payment,
                                amount=amount_to_distribute
                                )
                # insert here gl_distribution
                amount_to_distribute = 0
            else:
                break

            new_payment_distribution.save()

            GLEntry.objects.doubleEntry(
             transaction=payment.transaction,
             date=payment.date,
             first_gl_account=payment.invoice.supplier.gl_account_liability,
             first_amount=new_payment_distribution.amount,
             second_gl_account=Setting.default_asset,
             second_amount=-new_payment_distribution.amount).save()

    def insertSupplierCredit(self, payment):
        amount_to_insert = payment.amount-payment.invoice.balance
        if payment.invoice.supplier.gl_account_pay_over is None:
            raise Exception('supplier gls must be set')
        if amount_to_insert <= 0:
            return
        GLEntry.objects.doubleEntry(
             transaction=payment.transaction,
             date=payment.date,
             first_gl_account=payment.invoice.supplier.gl_account_pay_over,
             first_amount=amount_to_insert,
             second_gl_account=payment.invoice.supplier.gl_account_liability,
             second_amount=-amount_to_insert).save()

    def adjustInvoiceAndSupplierCredit(self, payment):
        payment.invoice.balance -= payment.amount
        payment.invoice.status = 'PAID'
        payment.invoice.save(update_fields=['balance', 'status'])

    def delete(self, payment, user):
        if payment.is_canceled is False:
            pay_overs = GLEntry.objects.filter(
                linked_transaction=payment.transaction.linked_transaction,
                gl_account=payment.invoice.supplier.gl_account_pay_over)
            if pay_overs.count() > 0:
                raise Exception('delete the over payment first')
            payment.invoice.supplier.gl_account_liability
            self.reverse(
                    transaction_target=payment.transaction,
                    transaction=None,
                    user=user)

    def reverse(
                self,
                transaction_target,
                transaction,
                user):

        if transaction is None:

            Transaction.objects.reverseTransaction(
                            transaction_target=transaction_target,
                            transaction=None,
                            transaction_type='CPA_AP',
                            user=user)
        else:
            Transaction.objects.reverseTransaction(
                            transaction_target=transaction_target,
                            Transaction=transaction,
                            transaction_type=transaction.transaction_type,
                            user=user)


class CreditManager(models.Manager):

    def create(
            self,
            credit_type,
            invoice,
            gl_account,
            amount,
            description,
            date,
            user,
            ):

        new_credit = Credit(
                    credit_date=date,
                    credit_type=credit_type,
                    invoice=invoice,
                    amount=amount,
                    gl_account=gl_account,
                    description=description)
        new_credit.save()

        if credit_type is 'CM':

            current_transaction = Transaction(
                    transaction_date=date,
                    transaction_type='CRM_AP',
                    linked_transaction=invoice.transaction,
                    created_by=user,
                    modified_by=user,
                    created_date=timezone.now())
            current_transaction.save()

            GLEntry.doubleEntry(
                Transaction=current_transaction,
                date=date,
                first_gl_account='',
                first_amount='',
                second_gl_account='',
                second_amount='',)

        elif credit_type is 'CR':
            current_transaction = Transaction(
                    transaction_date=date,
                    transaction_type='CRR_AP',
                    linked_transaction=invoice.transaction,
                    created_by=user,
                    modified_by=user,
                    created_date=timezone.now())
            current_transaction.save()

            GLEntry.doubleEntry(
                Transaction=current_transaction,
                date=date,
                first_gl_account='',
                first_amount='',
                second_gl_account='',
                second_amount='',)
        else:
            raise Exception('invalid code')

    def delete(self, credit):
        pass

    def update(self, credit):
        raise Exception(
                'impossible to modify a credit memo, delete it instead'
                )


class Invoice(models.Model):
    DRAFT = 'DRAFT'
    NOT_PAID = 'NOT_PAID'
    PAR_PAY = 'PAR_PAY'
    PAID = 'PAID'
    CANCELED = 'CANCELED'
    STATUS_CHOICES = (
        (DRAFT, 'DRAFT'),
        (NOT_PAID, 'NOT_PAID'),
        (PAR_PAY, 'PAR_PAY'),
        (PAID, 'PAID'),
        (CANCELED, 'CANCELED')
    )
    invoice_no = models.IntegerField(null=True)
    status = models.CharField(
        max_length=16,
        choices=STATUS_CHOICES,
        default=DRAFT)
    total = models.DecimalField(max_digits=16, decimal_places=2, default=0)
    balance = models.DecimalField(max_digits=16, decimal_places=2, default=0)
    supplier = models.ForeignKey('Supplier', related_name='%(class)s_supplier')
    supplier_inv_ref = models.CharField(max_length=128)
    date = models.DateTimeField()
    note = models.CharField(max_length=2048, null=True)
    gl_account_liability = models.ForeignKey(
        GLAccount,
        related_name='%(class)s_gl_account_liability')
    transaction = models.OneToOneField(
                        Transaction,
                        related_name='%(class)s_transaction',
                        null=True,
                        blank=True)
    objects = InvoiceManager()
    # audit
    created_by = models.ForeignKey(
                        AUTH_USER_MODEL,
                        related_name='%(class)s_created_by')
    created_date = models.DateTimeField(editable=False)
    modified_by = models.ForeignKey(
                        AUTH_USER_MODEL,
                        related_name='%(class)s_modified_by')
    modified_date = models.DateTimeField()

    @staticmethod
    def get_list_accounting_value(dataset, group_by_key, sum_value_keys):
        dic = defaultdict(Counter)
        for item in dataset:
            key = item[group_by_key]
            vals = {k:item[k] for k in sum_value_keys}
            dic[key].update(vals)
        return dic

    def recalculate_total(self):
        self.total = self.invoiceitem_invoice.filter(
            is_canceled=False
            ).aggregate(Sum('line_total'))
        self.save()
        return self.total
    #invoice in status DRAFT OR NOT_PAID can be modified, nothing else
    # call after updating items
    def update_with_dict(self, invoice_dict):
        if self.status not in [Invoice.DRAFT, Invoice.NOT_PAID]:
            return None
        # data gathering
        supplier = invoice_dict.get('supplier', None)
        supplier_inv_ref = invoice_dict.get('supplier_inv_ref', '')
        note = invoice_dict.get('note', '')
        date = invoice_dict['date']
        # no impact on modification
        if supplier_inv_ref is not None:
            self.supplier_inv_ref = supplier_inv_ref
        if note is not None:
            self.note = note
        #impact on modification
        if supplier is not None:
            self.supplier = supplier
            gl_account_liability = supplier.supplier_gl_account

        #impact assesment and treatment
        # 4 possible transitions
        # NOT_PAID to NOT_PAID
        if self.status == Invoice.NOT_PAID and\
                status == Invoice.NOT_PAID:
            pass

        
        self.date
        self.gl_account_liability
        self.modified_by = invoice_dict['modified_by']
        self.modified_date = invoice_dict['modified_date']

    def get_total_liability(self):
        accounting_value = []

        for item in self.invoiceitem_invoice.\
                filter(is_canceled=False).\
                values('gl_account').\
                annotate(amount=Sum('line_total')):
            accounting_value.push({
                    'gl_account': item.gl_account,
                    'type': 'D',
                    'amount': item.amount
                })
        return accounting_value


    def __str__(self):
        return \
            str(self.id) +\
            ' ' + str(self.status) +\
            ' ' + str(self.balance) + \
            ' ' + str(self.supplier)


class InvoiceItem(models.Model):
    invoice = models.ForeignKey('Invoice', related_name='%(class)s_invoice')
    quantity = models.IntegerField(default=1)
    price = models.DecimalField(max_digits=8, decimal_places=2, default=0)
    line_total = models.DecimalField(max_digits=9, decimal_places=2, default=0)
    description = models.CharField(max_length=256)
    is_canceled = models.BooleanField(default=False)
    gl_account = models.ForeignKey(GLAccount, related_name='%(class)s_account')
    # audit
    created_by = models.ForeignKey(
                            AUTH_USER_MODEL,
                            related_name='%(class)s_created_by')
    created_date = models.DateTimeField(editable=False)
    modified_by = models.ForeignKey(
                            AUTH_USER_MODEL,
                            related_name='%(class)s_modified_by')
    modified_date = models.DateTimeField()
    objects = InvoiceItemManager()

    def save(self, *args, **kwargs):
        if not self.id:
            self.created_date = timezone.now()
        self.modified_date = timezone.now()
        return super(InvoiceItem, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.id) +\
            ' ' +\
            str(self.invoice) +\
            ' ' +\
            str(self.total) +\
            ' ' +\
            str(self.description) +\
            ' ' +\
            str(self.gl_account)

    # invoice status possible NOT_PAID and DRAFT
    def update_with_dict(self, item_dict):
        quantity = item_dict.get('quantity', 0)
        price = item_dict.get('price', 0)
        line_total = price*quantity
        description = item_dict.get('description', '')
        gl_account = item_dict['gl_account']

        if self.invoice.status == Invoice.NOT_PAID:
            gl_account_corrections = []
            if self.gl_account == gl_account:
                if line_total - self.line_total < 0:
                    gl_account_corrections.append({
                        'gl_account': self.gl_account,
                        'type': 'D',
                        'amount':  self.line_total - line_total
                    })
                elif line_total - self.line_total > 0:
                    gl_account_corrections.append({
                        'gl_account': self.gl_account,
                        'type': 'C',
                        'amount':  line_total - self.line_total
                    })
            else:
                gl_account_corrections.append({
                    'gl_account': self.gl_account,
                    'type': 'D',
                    'amount': self.line_total
                })
                gl_account_corrections.append({
                    'gl_account': gl_account,
                    'type': 'C',
                    'amount': line_total
                })

        self.quantity = quantity
        self.price = price
        self.line_total = line_total
        self.description = description
        self.gl_account = gl_account
        self.modified_by = item_dict['modified_by']
        self.modified_date = item_dict['modified_date']
        self.save()
        if self.invoice.status == Invoice.NOT_PAID:
            return gl_account_corrections
        else:
            return None

    def get_accounting_value(self):
        return {
                'gl_account': self.gl_account,
                'amount': self.line_total
            }


    def is_eq_dict(self, item_dict, check_all=False):

        if item_dict.get('id', None) is None:
            return False

        if check_all is False:
            field_quantity = item_dict['quantity']
            field_price = item_dict['price']
            field_gl_account = item_dict['gl_account']
            line_total = field_quantity * field_price
            if self.quantity == field_quantity and\
                    self.price == field_price and\
                    self.gl_account == field_gl_account:
                return True
            else:
                return False
        else:
            field_quantity = item_dict['quantity']
            field_price = item_dict['price']
            field_gl_account = item_dict['gl_account']
            field_description = item_dict['description']
            if self.quantity == field_quantity and\
                    self.price == field_price and\
                    self.description == field_description and\
                    self.gl_account == field_gl_account:
                return True
            else:
                return False

class Payment(models.Model):

    invoice = models.ForeignKey('Invoice', related_name='%(class)s_invoice_ap')
    is_canceled = models.BooleanField(default=False)
    note = models.CharField(max_length=2048, null=True)
    amount = models.DecimalField(max_digits=16, decimal_places=2)

    transaction = models.ForeignKey(
                            Transaction, related_name='%(class)s_transaction')
    date = models.DateTimeField()
    payment_method = models.ForeignKey(
        PaymentMethod,
        related_name='%(class)s_payment_type')
    cheque_no = models.CharField(max_length=32, null=True)
    proxy_user = models.ForeignKey(
                        AUTH_USER_MODEL,
                        related_name='%(class)s_proxy_user',
                        null=True)
    objects = PaymentManager()
    # audit
    created_by = models.ForeignKey(
                                    AUTH_USER_MODEL,
                                    related_name='%(class)s_created_by')
    created_date = models.DateTimeField(editable=False)
    modified_by = models.ForeignKey(
                        AUTH_USER_MODEL,
                        related_name='%(class)s_modified_by')
    modified_date = models.DateTimeField()

    def save(self, *args, **kwargs):
        if not self.id:
            self.created_date = timezone.now()
        self.modified_date = timezone.now()
        return super(Payment, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.id) + ' ' + str(self.invoice) + ' ' + str(self.amount)


class Credit(models.Model):
    CM = 'CM'
    CR = 'CR'
    CREDIT_TYPES = (
        (CM, 'CREDIT_MEMO'),
        (CR, 'CREDIT REFUND')
        )
    credit_date = models.DateTimeField()
    is_canceled = models.BooleanField(default=False)
    credit_type = models.CharField(max_length=8, choices=CREDIT_TYPES)
    invoice = models.ForeignKey('Invoice', related_name='%(class)s_invoice')
    amount = models.DecimalField(max_digits=8, decimal_places=2)
    gl_account = models.ForeignKey(GLAccount, related_name='%(class)s_account')
    description = models.CharField(max_length=512)
    # audit
    created_by = models.ForeignKey(
                                AUTH_USER_MODEL,
                                related_name='%(class)s_created_by')
    created_date = models.DateTimeField(editable=False)
    modified_by = models.ForeignKey(
                                AUTH_USER_MODEL,
                                related_name='%(class)s_modified_by')
    modified_date = models.DateTimeField()

    def save(self, *args, **kwargs):
        if not self.id:
            self.created_date = timezone.now()
        self.modified_date = timezone.now()
        return super(Credit, self).save(*args, **kwargs)

'''
SUPPLIER CATEGORY
'''


class SupplierCategory(models.Model):
    class Meta:
        verbose_name_plural = 'supplier categories'

    name = models.ManyToManyField(LocalizedText, related_name='%(class)s_name')
    is_active = models.BooleanField(default=True)
    # audit
    created_by = models.ForeignKey(
                            AUTH_USER_MODEL,
                            related_name='%(class)s_created_by')
    objects = SupplierCategoryManager()
    created_date = models.DateTimeField(editable=False)
    modified_by = models.ForeignKey(
                            AUTH_USER_MODEL,
                            related_name='%(class)s_modified_by')
    modified_date = models.DateTimeField()

    def get_name(self, locale):
        names = self.name.all()
        if names is None:
            return '<no translation>'
        for _name in names:
            if _name.locale == locale and\
                    _name.text.strip():
                return _name.text
        if locale == 'en':
            return '<no translation>'
        elif locale == 'fr':
            return '<pas de traduction>'
        elif locale == 'es':
            return '<no hay draduccion>'
        else:
            return '<no translation>'

    def save(self, *args, **kwargs):
        if not self.id:
            self.created_date = timezone.now()
        self.modified_date = timezone.now()
        return super(SupplierCategory, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.id)

'''
SUPPLIER
'''


class Supplier(models.Model):
    is_active = models.BooleanField(default=True)
    name = models.CharField(max_length=128)
    rbq_no = models.CharField(max_length=32, null=True)
    address_line1 = models.CharField(max_length=256, null=True)
    address_line2 = models.CharField(max_length=256, null=True)
    postal_code = models.CharField(max_length=6, null=True)
    phone_number = models.CharField(max_length=16, null=True)
    city = models.CharField(max_length=128, null=True)
    balance = models.OneToOneField(Balance, related_name='%(class)s_balance_ap')
    category = models.ForeignKey(
                        'SupplierCategory',
                        related_name='%(class)s_category')
    supplier_gl_account = models.ForeignKey(
                                GLAccount,
                                related_name='%(class)s_gl_account_liability')
    supplier_gl_account_pay_over = models.ForeignKey(
                                GLAccount,
                                related_name='%(class)s_gl_account_pay_over')
    objects = SupplierManager()
    # audit
    created_by = models.ForeignKey(
                                AUTH_USER_MODEL,
                                related_name='%(class)s_created_by')
    created_date = models.DateTimeField(editable=False)
    modified_by = models.ForeignKey(
                                AUTH_USER_MODEL,
                                related_name='%(class)s_modified_by')
    modified_date = models.DateTimeField()

    def save(self, *args, **kwargs):
        if not self.id:
            self.created_date = timezone.now()
        self.modified_date = timezone.now()
        return super(Supplier, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.id) + ' ' + self.name
