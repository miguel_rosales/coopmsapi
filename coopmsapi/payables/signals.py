from django.dispatch import receiver
from django.core.signals import post_save
from .models import InvoiceDetail


@receiver(post_save, sender=InvoiceDetail)
def ensure_profile_exists(sender, **kwargs):
    if kwargs.get('created', False):
        print('SIGNAL WORKED!!')
