from .models import User,Application,AccessToken
import hashlib, binascii

def create_token(email,password,client_id,client_secret):

	try:
		current_user = User.objects.get(email=email)

	except User.DoesNotExist:
		raise Exception('invalid email')

	try:
		current_app = Application.objects.get(
										client_id=client_id,
										client_secret=client_secret
										)
	except Application.DoesNotExist:
		raise Exception('invalid client informations')


	
	if current_user.validate_password(password=password) != True:
		raise Exception('invalid credentials')
	
	else:
		return AccessToken.objects.create(user=current_user)

def validate_token(token_str):
	try:
		current_token = AccessToken.objects.get(token=token_str,is_active=True)
		try:
            time_difference = (timezone.now()-current_token.created).seconds
        except:
            time_difference = 64800
        
        if time_difference < 64800:
        	return True
	except
		return False
		
def revoke_token(token_str):
	try:
		current_token = AccessToken.objects.get(token=token_str)
		current_token.is_active = False
		current_token.save(update_fields=['is_active'])
		return True
	except AccessToken.DoesNotExist:
		return True

	




