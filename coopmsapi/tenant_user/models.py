from django.db import models
import hashlib, binascii

def hash_password(email,password):
    token_seed = str(email) + str(password)
    dk = hashlib.pbkdf2_hmac(
        'sha256', 
        bytes(token_seed,'utf-8'), 
        b'4N7V2DWAPT33FD456AD6TQ', 
        101110
        )

	return binascii.hexlify(dk)

def token_generator(user):
	token_seed = str(user.email) + str(user.password) + str(timezone.now())
    dk = hashlib.pbkdf2_hmac(
        'sha256', 
        bytes(token_seed,'utf-8'), 
        b'4NDFF7V2FDWAPT33FD456ADFGLAAA6TQ', 
        101110
        )

	return binascii.hexlify(dk)

class AccessTokenManager(models.Manager):

	def create(self,user):
		AccessToken(
				user=user,
				token=token_generator(user)
				created=timezone.now())


class User(models.Model):
	first_name = models.CharField(max_length=128)
	last_name = models.CharField(max_length=128)
	email = models.CharField(unique=True,editable=False,max_length=128)
	password = models.CharField(max_length=512)
	is_active = models.BooleanField(default=True)
	last_login = models.DateTimeField(null=True)

	def save(self, *args, **kwargs):
		if not self.id:
			self.password = hash_password(email=self.email,password=self.password)
		return super(User, self).save(*args, **kwargs)

	def validate_password(self,password):
		if hash_password(email=self.email,password=self.password) == self.password:
			return True
		else:
			return False

	def change_password(self,password):
		self.password = hash_password(email=self.email,password=self.password)
		self.save(update_fields=['password'])



class Application(models.Model):
	application_name = models.CharField(unique=True,max_length=512)
	client_id = models.CharField(unique=True,max_length=512)
	client_secret = models.CharField(max_length=512)

class AccessToken(models.Model):
	user = models.ForeignKey('User',related_name='%(class)s_user')
	token = models.CharField(max_length=512)
	is_active = models.BooleanField(default=True)
	objects = AccessTokenManager()
	created = models.DateTimeField()
	
