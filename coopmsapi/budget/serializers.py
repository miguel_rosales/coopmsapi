from django.utils import timezone
from rest_framework import serializers
from coopmsapi.accounting.models import GLAccount
from coopmsapi.users.serializers import UserDisplayMiniSerializer
from coopmsapi.accounting.serializers import GLAccountDisplayShortSerializer
from .models import Budget, BudgetItem


"""
BUDGET ITEM serializer
"""

class BudgetItemSerializer(serializers.ModelSerializer):

    gl_account = serializers.PrimaryKeyRelatedField(
        queryset=GLAccount.objects.all(),
        many=False)
    amount = serializers.DecimalField(max_digits=8, decimal_places=2)
    classification = serializers.SerializerMethodField()
    date = serializers.CharField(required=False, allow_blank=True)

    class Meta:
        model = BudgetItem
        fields = (
            'id',
            'amount',
            'gl_account',
            'classification',
            'description',
            'date')

        extra_kwargs = {
            'id': {'read_only': False, 'required': False}
        }

    def get_classification(self, obj):
        return obj.gl_account.get_classification()

    def create(self, data):
        if data.get('created_by') is None:
            data['created_by'] = self.context['request'].user
        if data.get('created_date') is None:
            data['created_date'] = timezone.now()
        if data.get('modified_by') is None:
            data['modified_by'] = data['created_by']
        if data.get('modified_date') is None:
            data['modified_date'] = data['created_date']
        return BudgetItem.objects.create(budget_item_dict=data)

    def update(self, obj, data):
        return BudgetItem.objects.update(
            budget_item=obj,
            budget_item_dict=data)

'''
BUDGET serializer
'''

class BudgetSerializer(serializers.ModelSerializer):

    items = BudgetItemSerializer(
        many=True,
        source='budgetitem_budget',
        required=False)

    class Meta:
        model = Budget
        fields = (
            'id',
            'name',
            'year',
            'date_start',
            'items',
            'date_end',
            'created_by',
            'created_date',
            'modified_by',
            'modified_date')

        extra_kwargs = {
                'created_by': {'read_only': True},
                'created_date': {'read_only': True},
                'modified_by': {'read_only': True},
                'modified_date': {'read_only': True}}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if kwargs.get('context', None) is not None:
            if self.context['request'].method == 'GET' and\
                    self.context['pk_present'] is False:
                self.fields.pop('items')

    def validate(self, data):
        if data['date_end'] < data['date_start']:
            raise serializers.ValidationError(
                'date_end field must be greater than date_start.')
        return data

    def create(self, data):
        data['created_by'] = self.context['request'].user
        data['created_date'] = timezone.now()
        data['modified_by'] = data['created_by']
        data['modified_date'] = data['created_date']
        for dict_item in data.get('budgetitem_budget', []):
            dict_item['created_by'] = data['created_by']
            dict_item['created_date'] = data['created_date']
            dict_item['modified_by'] = data['modified_by']
            dict_item['modified_date'] = data['modified_date']
        return Budget.objects.create(budget_dict=data)

    def update(self, obj, data):
        if self.context['request'].method == 'PUT':
            data['modified_by'] = self.context.get('request').user
            data['modified_date'] = timezone.now()
            return Budget.objects.update(budget=obj, budget_dict=data)

        elif self.context['request'].method == 'PATCH':
            data['modified_by'] = self.context.get('request').user
            data['modified_date'] = timezone.now()
            for dict_item in data.get('budgetitem_budget'):
                if dict_item.get('id', None) is None or dict_item.get('id', None) == 0:
                    dict_item['budget'] = obj
                    dict_item['created_by'] = self.context.get('request').user
                    dict_item['created_date'] = timezone.now()
                    dict_item['modified_by'] = self.context.get('request').user
                    dict_item['modified_date'] = timezone.now()
                    if dict_item.get('id', None) == 0:
                        dict_item['id'] = None
                else:
                    dict_item['modified_by'] = self.context.get('request').user
                    dict_item['modified_date'] = timezone.now()
            return Budget.objects.partial_update(obj, data)
        else:
            raise Exception(
                'HTTP METHOD NOT SUPPORTED: ' + self.context['request'].method)
