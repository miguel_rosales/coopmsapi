# -*- coding: utf-8 -*-
from rest_framework.response import Response
from rest_framework import status
from rest_framework import viewsets
# filters
from rest_framework import filters
# transaction
from django.db import transaction, IntegrityError
from .models import Budget, BudgetItem
from .serializers import (
    BudgetSerializer,
    BudgetItemSerializer)


"""
BUDGET viewset
"""


class BudgetViewSet(viewsets.ModelViewSet):
    serializer_class = BudgetSerializer
    queryset = Budget.objects.filter()

    def get_serializer_context(self):
        if self.action == 'retrieve' or\
                self.action == 'update' or\
                self.action == 'partial_update' or\
                self.action == 'destroy':
            return {'request': self.request, 'pk_present': True}
        else:
            return {'request': self.request, 'pk_present': False}
"""
BUDGET ITEM viewset
"""


class BudgetItemViewSet(viewsets.ModelViewSet):
    serializer_class = BudgetItemSerializer
    queryset = BudgetItem.objects.filter()

    
    def list(self, request, budget_pk):
        items = BudgetItem.objects.filter(budget_id=budget_pk)
        serializer = BudgetItemSerializer(items, many=True)
        response_dict = {}
        response_dict['count'] = items.count()
        response_dict['next'] = None
        response_dict['previous'] = None
        response_dict['results'] = serializer.data
        return Response(response_dict, status.HTTP_200_OK)

    def get_serializer_context(self):
        if self.action == 'retrieve' or\
                self.action == 'update' or\
                self.action == 'partial_update' or\
                self.action == 'destroy':
            return {'request': self.request, 'pk_present': True}
        else:
            return {'request': self.request, 'pk_present': False}
