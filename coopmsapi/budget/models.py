from django.utils import timezone
from django.db import models
from coopmsapi.settings import AUTH_USER_MODEL
from coopmsapi.accounting.models import GLAccount


class BudgetManager(models.Manager):

    def create(self, budget_dict):

        budget = Budget(
            name=budget_dict['name'],
            year=budget_dict['year'],
            date_start=budget_dict['date_start'],
            date_end=budget_dict['date_end'],
            created_by=budget_dict['created_by'],
            modified_by=budget_dict['created_by'])

        budget.save()
        budget_dict['budget'] = budget

        for budget_item_dict in budget_dict.get('budgetitem_budget', []):
            budget_item_dict['budget'] = budget
            BudgetItem.objects.create(
                budget_item_dict=budget_item_dict)

        return budget

    def update(self, budget, budget_dict):
        budget.update_with_dict(budget_dict)
        return budget

    def partial_update(self, budget, budget_dict):
        # check items are distinct later
        dict_items = budget_dict.get('budgetitem_budget', None)
        # delete
        obj_budget_items = budget.budgetitem_budget.all()
        for obj_budget_item in obj_budget_items:
            if obj_budget_item.is_obj_in_list(dict_items) is False:
                obj_budget_item.delete()

        # update        
        if dict_items is not None:
            for dict_item in dict_items:
                if dict_item.get('id', None) is not None:
                    obj_budget_items = budget.budgetitem_budget.all()
                    for obj_budget_item in obj_budget_items:
                        if obj_budget_item.id == dict_item['id']:
                            obj_budget_item.update_with_dict(dict_item)
        # create
        for dict_item in dict_items:
            if dict_item.get('id', None) is None:
                new_budget_item = BudgetItem(**dict_item)
                new_budget_item.save()

        budget.update_with_dict(budget_dict)

        return budget


    def delete(self, budget):
        try:
            BudgetItem.objects.filter(budget=budget).delete()
            budget.delete()
            return True
        except Exception('Delete Failed'):
            return False


class BudgetItemManager(models.Manager):

    def create(self, budget_item_dict):

        budget_item = BudgetItem(
            budget=budget_item_dict['budget'],
            amount=budget_item_dict['amount'],
            date=budget_item_dict.get('date', None),
            description=budget_item_dict['description'],
            gl_account=budget_item_dict['gl_account'],
            created_by=budget_item_dict['created_by'],
            created_date=budget_item_dict['created_date'],
            modified_by=budget_item_dict['modified_by'],
            modified_date=budget_item_dict['modified_date'])

        budget_item.save()
        return budget_item

    def update(self, budget_item, budget_item_dict):
        budget_item.update_with_dict(budget_item_dict)
        return budget_item

    def delete(self, budget_item):
        try:
            budget_item.delete()
            return True
        except:
            return False


class Budget(models.Model):
    name = models.CharField(max_length=128)
    year = models.IntegerField()
    date_start = models.DateField()
    date_end = models.DateField()

    objects = BudgetManager()

    created_by = models.ForeignKey(
                    AUTH_USER_MODEL,
                    related_name='%(class)s_created_by')
    created_date = models.DateTimeField(editable=False)
    modified_by = models.ForeignKey(
                    AUTH_USER_MODEL,
                    related_name='%(class)s_modified_by')
    modified_date = models.DateTimeField()

    def save(self, *args, **kwargs):
        if not self.id:
            self.created_date = timezone.now()
        self.modified_date = timezone.now()
        return super(Budget, self).save(*args, **kwargs)

    def update_with_dict(self, budget_dict):
        field_name = budget_dict.get('name')
        if field_name is not None:
            self.name = field_name
        field_year = budget_dict.get('year')
        if field_year is not None:
            self.year = field_year
        field_date_start = budget_dict.get('date_start')
        if field_date_start is not None:
            self.date_start = field_date_start
        field_date_end = budget_dict.get('date_end')
        if field_date_end is not None:
            self.date_end = field_date_end
        self.modified_by = budget_dict['modified_by']
        self.modified_date = budget_dict['modified_date']
        self.save()
        return True


class BudgetItem(models.Model):
    budget = models.ForeignKey(
                    'Budget',
                    related_name='%(class)s_budget',
                    on_delete=models.CASCADE)
    amount = models.DecimalField(
                    max_digits=16,
                    decimal_places=2)
    description = models.CharField(max_length=256)
    gl_account = models.ForeignKey(
                    GLAccount)
    date = models.CharField(default='', blank=True, max_length=128)
    objects = BudgetItemManager()

    created_by = models.ForeignKey(
                    AUTH_USER_MODEL,
                    related_name='%(class)s_created_by')
    created_date = models.DateTimeField(editable=False)
    modified_by = models.ForeignKey(
                    AUTH_USER_MODEL,
                    related_name='%(class)s_modified_by')
    modified_date = models.DateTimeField()

    def save(self, *args, **kwargs):
        if not self.id:
            self.created_date = timezone.now()
        self.modified_date = timezone.now()
        return super(BudgetItem, self).save(*args, **kwargs)

    def is_equal_to_dict(self, budget_item_dict):
        if self.id == budget_item_dict.get('id') and\
                self.budget == budget_item_dict.get('budget') and\
                self.description == budget_item_dict.get('description') and\
                self.gl_account == budget_item_dict.get('gl_account') and\
                self.date == budget_item_dict.get('date'):
            return True
        return False

    def update_with_dict(self, budget_item_dict):

        field_description = budget_item_dict.get('description', None)
        if field_description is not None:
            self.description = field_description
        field_gl_account = budget_item_dict.get('gl_account', None)
        if field_gl_account is not None:
            self.gl_account = budget_item_dict.get('gl_account', None)
        field_date = budget_item_dict.get('date', None)
        if field_date is not None:
            self.date = field_date
        field_amount = budget_item_dict.get('amount', None)
        if field_amount is not None:
            self.amount= field_amount
        self.modified_by = budget_item_dict['modified_by']
        self.modified_date = budget_item_dict['modified_date']
        self.save()
        return True

    def is_obj_in_list(self, list_budget_item):
        for budget_item_dict in list_budget_item:
            if self.id == budget_item_dict.get('id', None):
                return True
        return False
