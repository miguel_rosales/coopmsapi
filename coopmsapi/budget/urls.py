from django.conf.urls import url, include
from rest_framework_nested import routers
from .views import (
    BudgetViewSet,
    BudgetItemViewSet)


budget_router = routers.SimpleRouter()
budget_router.register(r'budgets', BudgetViewSet, base_name='budget')

budget_item_router = routers.NestedSimpleRouter(
    budget_router,
    r'budgets',
    lookup='budget')

budget_item_router.\
    register(r'items', BudgetItemViewSet, base_name='budget_item')

urlpatterns = [
    url(r'^', include(budget_router.urls)),
    url(r'^', include(budget_item_router.urls)),
]
