from django.utils import timezone
from solo.models import SingletonModel
from coopmsapi.settings import AUTH_USER_MODEL
from dateutil.relativedelta import relativedelta
from django.db import models, IntegrityError
from coopmsapi.locale.models import LocalizedText


class CoopManager(models.Manager):

    def create(
            self,
            tenant_id,
            name,
            civil_number,
            street,
            postal_code,
            city,
            user):
        current_coop = Coop(
            tenant_id=tenant_id,
            name=name,
            civil_number=civil_number,
            street=street,
            postal_code=postal_code,
            city=city,
            created_by=user,
            modified_by=user)
        current_coop.save()


class BuildingManager(models.Manager):
    def create(self, building_dict):
        new_building = Building(**building_dict)
        new_building.save()
        return new_building

    def update(self, building, building_dict):
        building.contains_many = building_dict.get('contains_many', True)
        building.civil_number = building_dict['civil_number']
        building.street = building_dict['street']
        building.postal_code = building_dict.get('postal_code', None)
        building.modified_by = building_dict['modified_by']
        building.modified_date = building_dict['modified_date']
        building.save()
        return building

    def delete(self, building, user):
        if Unit.objects.filter(building=building).count() > 0:
            raise IntegrityError(
                'There are units associated with this building, ' +
                'cannot delete')
        else:
            building.delete()
            return True

        return False


class UnitTypeManager(models.Manager):
    def create(self, unit_type_dict):
        unit_type = UnitType(
            nbr_bedrooms=unit_type_dict.get('nbr_bedrooms', None),
            nbr_bathrooms=unit_type_dict.get('nbr_bathrooms', None),
            floor=unit_type_dict.get('floor', None),
            created_by=unit_type_dict['created_by'],
            created_date=unit_type_dict['created_date'],
            modified_by=unit_type_dict['modified_by'],
            modified_date=unit_type_dict['modified_date'])
        unit_type.save()

        unit_type.name.clear()
        for localized_text in unit_type_dict.get('name'):
            new_name = LocalizedText.objects.create(
                localized_text['locale'],
                localized_text['text'])
            unit_type.name.add(new_name)
        unit_type.description.clear()
        for localized_text in unit_type_dict.get('description'):
            new_name = LocalizedText.objects.create(
                localized_text['locale'],
                localized_text['text'])
            unit_type.description.add(new_name)

        return unit_type

    def update(self, unit_type, unit_type_dict):
        unit_type.nbr_bedrooms = unit_type_dict.get('nbr_bedrooms', None)
        unit_type.nbr_bathrooms = unit_type_dict.get('nbr_bathrooms', None)
        unit_type.floor = unit_type_dict.get('floor', None)
        unit_type.modified_by = unit_type_dict['modified_by']
        unit_type.modified_date = unit_type_dict['modified_date']
        unit_type.save()
        
        unit_type.name.clear()
        for localized_text in unit_type_dict['name']:
            new_name = LocalizedText.objects.create(
                localized_text['locale'],
                localized_text['text'])
            unit_type.name.add(new_name)
        unit_type.description.clear()
        for localized_text in unit_type_dict['description']:
            new_description = LocalizedText.objects.create(
                localized_text['locale'],
                localized_text['text'])
            unit_type.description.add(new_description)

        return unit_type

    def delete(self, unit_type, user):
        if Unit.objects.filter(unit_type=unit_type).count() > 0:
            raise IntegrityError(
                'There are units associated with this unit_type, ' +
                'cannot delete')
        else:
            unit_type.delete()
            return True
        return False

class UnitTypePriceManager(models.Manager):

    def create(self, unit_type_price_dict):
        current_price = unit_type_price_dict['unit_type'].get_current_price()
        return self.set_contender_price(current_price, unit_type_price_dict)

    def update(self, obj, unit_type_price_dict):
        if unit_type_price_dict['is_expired'] and obj.is_expired is False:
            obj.is_expired = True
            obj.is_active = False
            obj.modified_by = unit_type_price_dict['modified_by']
            obj.modified_date = unit_type_price_dict['modified_date']
            obj.save()
        return obj

    def delete(self, obj, user):
        obj.is_expired = True
        obj.is_active = False
        obj.modified_by = user
        obj.modified_date = timezone.now()
        obj.save()
        return obj

    def set_contender_price(self, current_price, unit_type_price_dict):
        #needs to be included in a transaction block
        if current_price is None:
            if unit_type_price_dict['start_date'] <= timezone.now().date():
                unit_type_price_dict['is_active'] = True
                unit_type_price_dict['is_expired'] = False
            else:
                unit_type_price_dict['is_active'] = False
                unit_type_price_dict['is_expired'] = True           
            
            unit_type_price = UnitTypePrice(**unit_type_price_dict)
            unit_type_price.save()
            return unit_type_price
        else:
            current_price.end_date = timezone.now().date()
            current_price.is_expired = True
            current_price.modified_by = unit_type_price_dict['modified_by']
            current_price.modified_date = unit_type_price_dict['modified_date']
            current_price.save()
            if unit_type_price_dict['start_date'] <= timezone.now().date() and\
                    unit_type_price_dict.get(
                        'end_date',
                        timezone.now().date() + relativedelta(years=1)) > timezone.now().date():
                unit_type_price_dict['is_active'] = True
                unit_type_price_dict['is_expired'] = False
            else:
                unit_type_price_dict['is_active'] = False
                unit_type_price_dict['is_expired'] = True
            unit_type_price = UnitTypePrice(**unit_type_price_dict)
            unit_type_price.save()
            return unit_type_price


class UnitManager(models.Manager):
    def create(self, unit_dict):
        new_unit = Unit(**unit_dict)
        new_unit.save()
        return new_unit

    def update(self, unit, unit_dict):
        unit.unit_type = unit_dict['unit_type']
        unit.building = unit_dict['building']
        unit.unit_door_code = unit_dict['unit_door_code']
        unit.user = unit_dict.get('user', None)
        unit.modified_by = unit_dict['modified_by']
        unit.modified_date = unit_dict['modified_date']
        unit.save()
        return unit

    def delete(self, unit, user):
        unit.delete()
        return True

class RenteeRenterRelationManager(models.Manager):

    def create(self, rrr_dict):
        current_renter = rrr_dict['unit'].get_current_renter()
        obj = self.set_contender_renter(current_renter, rrr_dict)
        return obj 

    def set_contender_renter(self, current_renter, rrr_dict):
        if current_renter is None:
            if rrr_dict['start_date'] <= timezone.now().date():
                rrr_dict['is_active'] = True
                rrr_dict['is_expired'] = False
            else:
                rrr_dict['is_active'] = False
                rrr_dict['is_expired'] = True           
            
            rrr_obj = RenteeRenterRelation(**rrr_dict)
            rrr_obj.save()
            return rrr_obj
        else:
            current_renter.end_date = timezone.now().date()
            current_renter.is_expired = True
            current_renter.modified_by = rrr_dict['modified_by']
            current_renter.modified_date = rrr_dict['modified_date']
            current_renter.save()
            if rrr_dict['start_date'] <= timezone.now().date() and\
                    rrr_dict.get(
                        'end_date',
                        timezone.now().date() + relativedelta(years=1)) > timezone.now().date():
                rrr_dict['is_active'] = True
                rrr_dict['is_expired'] = False
            else:
                rrr_dict['is_active'] = False
                rrr_dict['is_expired'] = True
            rrr_obj = RenteeRenterRelation(**rrr_dict)
            rrr_obj.save()
            return rrr_obj

    def update(self, obj, rrr_dict):
        if rrr_dict['is_expired'] and obj.is_expired is False:
            obj.is_expired = True
            obj.is_active = False
            obj.modified_by = rrr_dict['modified_by']
            obj.modified_date = rrr_dict['modified_date']
            obj.save()
        return obj

    def delete(self, obj, user):
        obj.is_expired = True
        obj.is_active = False
        obj.modified_by = user
        obj.modified_date = timezone.now()
        obj.save()
        return obj


class Coop(SingletonModel):
    name = models.CharField(max_length=128)
    civil_number = models.CharField(max_length=32)
    street = models.CharField(max_length=128)
    city = models.CharField(max_length=128)
    postal_code = models.CharField(max_length=8)
    tenant_id = models.IntegerField(editable=False)
    objects = CoopManager()
    created_by = models.ForeignKey(
        AUTH_USER_MODEL,
        related_name='%(class)s_created_by')
    created_date = models.DateTimeField(editable=False)
    modified_by = models.ForeignKey(
        AUTH_USER_MODEL,
        related_name='%(class)s_modified_by')
    modified_date = models.DateTimeField()

    def save(self, *args, **kwargs):
        if not self.id:
            self.created_date = timezone.now()
        self.modified_date = timezone.now()
        return super(Coop, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.id) + ' ' + self.name


class Building(models.Model):
    contains_many = models.BooleanField(default=True)
    civil_number = models.CharField(max_length=32)
    street = models.CharField(max_length=128)
    postal_code = models.CharField(max_length=8, null=True)
    objects = BuildingManager()
    # audit
    created_by = models.ForeignKey(
        AUTH_USER_MODEL,
        related_name='%(class)s_created_by')
    created_date = models.DateTimeField(editable=False)
    modified_by = models.ForeignKey(
        AUTH_USER_MODEL,
        related_name='%(class)s_modified_by')
    modified_date = models.DateTimeField()

    def save(self, *args, **kwargs):
        if not self.id:
            self.created_date = timezone.now()
        self.modified_date = timezone.now()
        return super(Building, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.id) + ' ' + self.name

class UnitType(models.Model):
    name = models.ManyToManyField(
        LocalizedText,
        related_name='%(class)s_name')
    description = models.ManyToManyField(
        LocalizedText,
        related_name='%(class)s_description')
    nbr_bedrooms = models.IntegerField(null=True)
    nbr_bathrooms = models.IntegerField(null=True)
    floor = models.IntegerField(null=True)
    objects = UnitTypeManager()
    # audit
    created_by = models.ForeignKey(
        AUTH_USER_MODEL,
        related_name='%(class)s_created_by')
    created_date = models.DateTimeField(editable=False)
    modified_by = models.ForeignKey(
        AUTH_USER_MODEL,
        related_name='%(class)s_modified_by')
    modified_date = models.DateTimeField()

    def get_current_price(self):
        try:
            current_price = self.unittypeprice.filter(
                is_active=True,
                is_expired=False).first()
            return current_price
        except Exception as e:
            return None

    def save(self, *args, **kwargs):
        if not self.id:
            self.created_date = timezone.now()
        self.modified_date = timezone.now()
        return super(UnitType, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.id) + ' ' + self.name


class Unit(models.Model):
    unit_type = models.ForeignKey(
        'UnitType',
        related_name='%(class)s_unit_type')
    building = models.ForeignKey(
        Building,
        related_name='%(class)s_building')
    unit_door_code = models.CharField(max_length=16)
    objects = UnitManager()
    # audit
    created_by = models.ForeignKey(
        AUTH_USER_MODEL,
        related_name='%(class)s_created_by')
    created_date = models.DateTimeField(editable=False)
    modified_by = models.ForeignKey(
        AUTH_USER_MODEL,
        related_name='%(class)s_modified_by')
    modified_date = models.DateTimeField()

    def get_current_renter(self):
        try:
            return self.renteerenterrelation.filter(
                is_active=True, is_expired=False).first()
        except Exception as e:
            return None

    def get_current_renter_start_date(self):
        try:
            return (self.renteerenterrelation.filter(
                is_active=True, is_expired=False).first()).start_date
        except Exception as e:
            print(e)
            return None

    def get_current_renter_end_date(self):
        try:
            return (self.renteerenterrelation.filter(
                is_active=True, is_expired=False).first()).end_date
        except Exception as e:
            return None

    def get_current_price(self):
        return self.unit_type.get_current_price()

    def save(self, *args, **kwargs):
        if not self.id:
            self.created_date = timezone.now()
        self.modified_date = timezone.now()
        return super(Unit, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.id) + ' ' + self.unit_type + ' ' + self.unit_door_code

class UnitTypePrice(models.Model):
    price = models.DecimalField(max_digits=16, decimal_places=2, default=0)
    unit_type = models.ForeignKey('UnitType', related_name='%(class)s')
    start_date = models.DateField()
    end_date = models.DateField(null=True)
    is_active = models.BooleanField(default=False)
    is_expired = models.BooleanField(default=False)
    objects = UnitTypePriceManager()
    # audit
    created_by = models.ForeignKey(
        AUTH_USER_MODEL,
        related_name='%(class)s_created_by')
    created_date = models.DateTimeField(editable=False)
    modified_by = models.ForeignKey(
        AUTH_USER_MODEL,
        related_name='%(class)s_modified_by')
    modified_date = models.DateTimeField()


class RenteeRenterRelation(models.Model):
    is_active = models.BooleanField(default=False)
    is_expired = models.BooleanField(default=False)
    unit = models.ForeignKey('Unit', related_name='%(class)s')
    user = models.ForeignKey(
        AUTH_USER_MODEL,
        related_name='%(class)s')
    start_date = models.DateField(null=False)
    end_date = models.DateField(null=True)
    objects = RenteeRenterRelationManager()
    # audit
    created_by = models.ForeignKey(
        AUTH_USER_MODEL,
        related_name='%(class)s_created_by')
    created_date = models.DateTimeField(editable=False)
    modified_by = models.ForeignKey(
        AUTH_USER_MODEL,
        related_name='%(class)s_modified_by')
    modified_date = models.DateTimeField()

    def get_fullname(self):
        return self.user.get_fullname()
    