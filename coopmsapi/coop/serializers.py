from django.utils import timezone
from rest_framework import serializers
from .models import Coop, Building, UnitType, Unit, UnitTypePrice, RenteeRenterRelation
from coopmsapi.locale.serializers import LocalizedTextSerializer


'''
COOP serializers
'''
class CoopInsertSerializer(serializers.ModelSerializer):
    name = serializers.CharField(max_length=128, required=False)
    modified_by = serializers.PrimaryKeyRelatedField(read_only=True)
    created_by = serializers.PrimaryKeyRelatedField(read_only=True)
    modified_date = serializers.DateTimeField(read_only=True)
    tenant_id = serializers.IntegerField(read_only=True)

    class Meta:
        model = Coop
        fields = '__all__'

class CoopSerializer(serializers.ModelSerializer):
    modified_by = serializers.PrimaryKeyRelatedField(read_only=True)
    created_by = serializers.PrimaryKeyRelatedField(read_only=True)
    modified_date = serializers.DateTimeField(read_only=True)
    tenant_id = serializers.IntegerField(read_only=True)

    class Meta:
        model = Coop
        fields = '__all__'

"""
BUILDING serializer
"""

class BuildingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Building
        fields = (
            'id',
            'contains_many',
            'civil_number',
            'street',
            'postal_code',
            'created_by',
            'created_date',
            'modified_by',
            'modified_date')

        extra_kwargs = {
                'postal_code': {'required': False},
                'created_by': {'read_only': True},
                'created_date': {'read_only': True},
                'modified_by': {'read_only': True},
                'modified_date': {'read_only': True}}

    def create(self, data):
        data['created_by'] = self.context.get('request').user
        data['created_date'] = timezone.now()
        data['modified_by'] = data['created_by']
        data['modified_date'] = data['created_date']
        new_building = Building.objects.create(data)
        return new_building

    def update(self, obj, data):
        data['modified_by'] = self.context.get('request').user
        data['modified_date'] = timezone.now()
        return Building.objects.update(obj, data)


class BuildingInsertSerializer(serializers.ModelSerializer):
    class Meta:
        model = Building
        fields = (
            'contains_many',
            'civil_number',
            'street',
            'postal_code')

    def create(self, data):
        pass

    def update(self, obj, data):
        pass


class BuildingDisplaySerializer(serializers.ModelSerializer):

    class Meta:
        model = Building
        fields = (
            'id',
            'contains_many',
            'civil_number',
            'street',
            'postal_code'
            'created_by',
            'created_date',
            'modified_by',
            'modified_date')


class BuildingDisplayShortSerializer(serializers.ModelSerializer):
    class Meta:
        model = Building
        fields = (
            'id',
            'contains_many',
            'civil_number',
            'street',
            'postal_code')

"""
UNIT TYPE serializers
"""


class UnitTypeSerializer(serializers.ModelSerializer):
    name = LocalizedTextSerializer(many=True)
    description = LocalizedTextSerializer(many=True)

    class Meta:
        model = UnitType
        fields = (
            'id',
            'name',
            'description',
            'nbr_bedrooms',
            'nbr_bathrooms',
            'floor',
            'created_by',
            'created_date',
            'modified_by',
            'modified_date')

        extra_kwargs = {
                'nbr_bathrooms': {'required': False},
                'nbr_bedrooms': {'required': False},
                'floor': {'required': False},
                'created_by': {'read_only': True},
                'created_date': {'read_only': True},
                'modified_by': {'read_only': True},
                'modified_date': {'read_only': True}}

    def create(self, data):
        data['created_by'] = self.context['request'].user
        data['created_date'] = timezone.now()
        data['modified_by'] = data['created_by']
        data['modified_date'] = data['created_date']

        new_unit_type = UnitType.objects.create(data)
        return new_unit_type

    def update(self, obj, data):
        data['modified_by'] = self.context.get('request').user
        data['modified_date'] = timezone.now()
        modified_unit_type = UnitType.objects.update(obj, data)
        return modified_unit_type

"""
UNIT serializers
"""

class UnitSerializer(serializers.ModelSerializer):

    current_user = serializers.SerializerMethodField()
    current_user_fullname = serializers.SerializerMethodField()
    current_user_start_date = serializers.SerializerMethodField()
    current_user_end_date = serializers.SerializerMethodField()
    current_price = serializers.SerializerMethodField()
    current_price_val = serializers.SerializerMethodField()

    class Meta:
        model = Unit
        fields = (
            'id',
            'unit_type',
            'building',
            'unit_door_code',
            'current_user',
            'current_user_fullname',
            'current_user_start_date',
            'current_user_end_date',
            'current_price',
            'current_price_val',
            'created_by',
            'created_date',
            'modified_by',
            'modified_date')

        extra_kwargs = {
                'created_by': {'read_only': True},
                'created_date': {'read_only': True},
                'modified_by': {'read_only': True},
                'modified_date': {'read_only': True}}

    def get_current_user(self, obj):
        current_user = obj.get_current_renter()
        if current_user is not None:
            return current_user.id
        else:
            return None

    def get_current_user_fullname(self, obj):
        current_user = obj.get_current_renter()
        if current_user is not None:
            return current_user.get_fullname()
        else:
            return None

    def get_current_user_start_date(self, obj):
        return obj.get_current_renter_start_date()

    def get_current_user_end_date(self, obj):
        return obj.get_current_renter_end_date()

    def get_current_price(self, obj):
        current_price = obj.get_current_price()
        if current_price is not None:
            return current_price.id
        else:
            return None

    def get_current_price_val(self, obj):
        current_price_val = obj.get_current_price()
        if current_price_val is not None:
            return current_price_val.price
        else:
            return None
        
    def create(self, data):
        data['created_by'] = self.context['request'].user
        data['created_date'] = timezone.now()
        data['modified_by'] = data['created_by']
        data['modified_date'] = data['created_date']
        new_unit = Unit.objects.create(data)
        return new_unit

    def update(self, obj, data):
        data['modified_by'] = self.context.get('request').user
        data['modified_date'] = timezone.now()
        modified_unit = Unit.objects.update(obj, data)
        return modified_unit

'''
UNIT TYPE PRICE serializer
'''

class UnitTypePriceSerializer(serializers.ModelSerializer):

    class Meta:
        model = UnitTypePrice
        fields = '__all__'
        extra_kwargs = {
                'end_date': {'required': False},
                'created_by': {'read_only': True},
                'created_date': {'read_only': True},
                'modified_by': {'read_only': True},
                'modified_date': {'read_only': True}}

    def create(self, data):
        data['created_by'] = self.context['request'].user
        data['created_date'] = timezone.now()
        data['modified_by'] = data['created_by']
        data['modified_date'] = data['created_date']
        new_unit = UnitTypePrice.objects.create(data)
        return new_unit

    def update(self, obj, data):
        data['modified_by'] = self.context.get('request').user
        data['modified_date'] = timezone.now()
        modified_unit = UnitTypePrice.objects.update(obj, data)
        return modified_unit

'''
RenteeRenterRelationship serializer
'''

class RenteeRenterRelationSerializer(serializers.ModelSerializer):

    class Meta:
        fields = '__all__'
        model = RenteeRenterRelation
        extra_kwargs = {
                'end_date': {'required': False},
                'created_by': {'read_only': True},
                'created_date': {'read_only': True},
                'modified_by': {'read_only': True},
                'modified_date': {'read_only': True}}

    def create(self, data):
        data['created_by'] = self.context['request'].user
        data['created_date'] = timezone.now()
        data['modified_by'] = data['created_by']
        data['modified_date'] = data['created_date']
        new_unit = RenteeRenterRelation.objects.create(data)
        return new_unit

    def update(self, obj, data):
        data['modified_by'] = self.context.get('request').user
        data['modified_date'] = timezone.now()
        modified_unit = RenteeRenterRelation.objects.update(obj, data)
        return modified_unit
        