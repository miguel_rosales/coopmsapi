from django.conf.urls import url, include
from rest_framework_nested import routers
from .views import CoopDetailView, CoopView
from .views import (
    BuildingViewSet,
    RenteeRenterRelationViewSet,
    UnitTypeViewSet,
    UnitViewSet,
    UnitTypePriceViewSet)

unit_type_router = routers.SimpleRouter()
unit_type_router.register(r'unit_types', UnitTypeViewSet, base_name='unit_type')

unit_router = routers.SimpleRouter()
unit_router.register(r'units', UnitViewSet, base_name='unit')

building_router = routers.SimpleRouter()
building_router.register(r'buildings', BuildingViewSet, base_name='building')

unit_type_price = routers.SimpleRouter()
unit_type_price.register(r'unit_type_prices', UnitTypePriceViewSet, base_name='unit_type_price')

rrr_router = routers.SimpleRouter()
rrr_router.register(r'rr_relations', RenteeRenterRelationViewSet, base_name='')


urlpatterns = [
    url(r'^', include(building_router.urls)),
    url(r'^(?P<pk>[0-9]+)/$', CoopDetailView.as_view(), name="coop"),
    url(r'^$', CoopView.as_view(), name="coops"),
    url(r'^', include(rrr_router.urls)),
    url(r'^', include(unit_type_price.urls)),
    url(r'^', include(unit_type_router.urls)),
    url(r'^', include(unit_router.urls))
]