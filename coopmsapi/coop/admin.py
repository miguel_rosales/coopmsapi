from django.contrib import admin
from .models import Coop, Building, UnitType, Unit

admin.site.register(Coop)
admin.site.register(Building)
admin.site.register(UnitType)
admin.site.register(Unit)