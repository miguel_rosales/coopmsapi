## -*- coding: utf-8 -*-
from django.utils import timezone
from rest_framework.response import Response
from rest_framework.views import APIView
from django.contrib.auth import get_user_model
from rest_framework import status, permissions, generics, viewsets
from rest_framework.parsers import MultiPartParser
from oauth2_provider.models import AccessToken
#filters
from rest_framework import filters
#transaction
from django.db import transaction, IntegrityError
from .models import Coop, Unit, UnitType, Building, UnitTypePrice, RenteeRenterRelation
from .serializers import (
    BuildingSerializer,
    CoopSerializer,
    UnitSerializer,
    UnitTypeSerializer,
    RenteeRenterRelationSerializer,
    UnitTypePriceSerializer)

'''
COOP VIEWS
'''
class CoopDetailView(generics.RetrieveUpdateDestroyAPIView):
    """
    Cooperative detail visualization of the data that model contains.
    """
    lookup_field = 'pk'
    queryset = Coop.objects.all()
    serializer_class = CoopSerializer


class CoopView(generics.ListCreateAPIView):
    """
    Cooperative visualization of the data that model contains.
    """
    queryset = Coop.objects.all()
    serializer_class = CoopSerializer

    def perform_create(self, serializer):
        serializer.save(modified_by=self.request.user,
                        created_by=self.request.user)


'''
UNIT views
'''
class UnitViewSet(viewsets.ModelViewSet):
    queryset = Unit.objects.all()
    serializer_class = UnitSerializer

'''
UNIT TYPE views
'''

class UnitTypeViewSet(viewsets.ModelViewSet):
    queryset = UnitType.objects.all()
    serializer_class = UnitTypeSerializer


'''
BUILDING views
'''
class BuildingViewSet(viewsets.ModelViewSet):
    queryset = Building.objects.all()
    serializer_class = BuildingSerializer

'''
UNIT TYPE PRICE views
'''
class UnitTypePriceViewSet(viewsets.ModelViewSet):
    queryset = UnitTypePrice.objects.all()
    serializer_class = UnitTypePriceSerializer

'''
Rentee Renter Relation views
'''
class RenteeRenterRelationViewSet(viewsets.ModelViewSet):
    queryset = RenteeRenterRelation.objects.all()
    serializer_class = RenteeRenterRelationSerializer
