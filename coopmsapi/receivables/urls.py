from django.conf.urls import url, include
# viewset
from rest_framework_nested import routers
from coopmsapi.receivables.views import (
        RentViewSet,
        ItemViewSet,
        InternalInvoiceViewSet,
        ExternalInvoiceViewSet,
        ProductViewSet,
        ProductCategoryViewSet,
        ClientViewSet,
        PaymentViewset,
        RentPaymentViewset)

'''
PRODUCT ROUTES
'''

product_category_router = routers.SimpleRouter()
product_category_router.register(
    r'product_categories',
    ProductCategoryViewSet,
    base_name='product_category')

product_router = routers.SimpleRouter()
product_router.register(
    r'products',
    ProductViewSet,
    base_name='product')

'''
CLIENT ROUTES
'''
client_router = routers.SimpleRouter()
client_router.register(
    r'clients',
    ClientViewSet,
    base_name='client')

'''
RENT ROUTES
'''

rent_router = routers.SimpleRouter()
rent_router.register(r'rents', RentViewSet, base_name='rent')

# item
rent_item_router = routers.NestedSimpleRouter(
    rent_router,
    r'rents',
    lookup='rent')

rent_item_router.register(
    r'items',
    ItemViewSet,
    base_name='rent_items')

# payment
rent_payment_router = routers.NestedSimpleRouter(
    rent_router,
    r'rents',
    lookup='rent')

rent_payment_router.register(
    r'payments',
    RentPaymentViewset,
    base_name='rent_payments')

'''
INTERNAL INVOICE ROUTES
'''

internal_invoice_router = routers.SimpleRouter()
internal_invoice_router.register(
    r'internal_invoices',
    InternalInvoiceViewSet,
    base_name='internal_invoice')

# item
internal_invoice_item_router = routers.NestedSimpleRouter(
    internal_invoice_router,
    r'internal_invoices',
    lookup='internal_invoice')
internal_invoice_item_router.register(
    r'items',
    ItemViewSet,
    base_name='internal_invoice_items')

# payment
internal_invoice_payment_router = routers.NestedSimpleRouter(
    internal_invoice_router,
    r'internal_invoices',
    lookup='internal_invoice')

internal_invoice_payment_router.register(
    r'payments',
    PaymentViewset,
    base_name='internal_invoice_payment')

'''
EXTERNAL INVOICE ROUTES
'''

external_invoice_router = routers.SimpleRouter()
external_invoice_router.register(
    r'external_invoices',
    ExternalInvoiceViewSet,
    base_name='external_invoice')

# items
external_invoice_item_router = routers.NestedSimpleRouter(
    external_invoice_router,
    r'external_invoices',
    lookup='external_invoice')

external_invoice_item_router.register(
    r'items',
    ItemViewSet,
    base_name='external_invoice_item')

# payments
external_invoice_payment_router = routers.NestedSimpleRouter(
    external_invoice_router,
    r'external_invoices',
    lookup='external_invoice')

internal_invoice_payment_router.register(
    r'payments',
    PaymentViewset,
    base_name='internal_invoice_payment')

urlpatterns = [
    url(r'^', include(product_category_router.urls)),
    url(r'^', include(product_router.urls)),
    url(r'^', include(client_router.urls)),
    url(r'^', include(rent_router.urls)),
    url(r'^', include(rent_item_router.urls)),
    url(r'^', include(rent_payment_router.urls)),
    url(r'^', include(internal_invoice_router.urls)),
    url(r'^', include(internal_invoice_item_router.urls)),
    url(r'^', include(external_invoice_router.urls)),
    url(r'^', include(external_invoice_item_router.urls)),
]
