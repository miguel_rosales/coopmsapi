# -*- coding: utf-8 -*-
from django.utils import timezone
from rest_framework.response import Response
from rest_framework.views import APIView
from django.contrib.auth import get_user_model
from rest_framework import status, permissions
from rest_framework.parsers import MultiPartParser
from rest_framework import generics
# Http404
from django.http import Http404
# status codes
from rest_framework import status
# Django shortcuts
from django.shortcuts import get_object_or_404
# rest framework viewsets
from rest_framework import viewsets, mixins
# filters
from rest_framework import filters
# transaction
from django.db import transaction, IntegrityError
# serializers
from .serializers import (
    RentInsertSerializer,
    RentUpdateSerializer,
    RentDisplaySerializer,
    RentDisplayShortSerializer,
    InternalInvoiceInsertSerializer,
    InternalInvoiceDisplaySerializer,
    ExternalInvoiceInsertSerializer,
    ExternalInvoiceDisplaySerializer,
    ItemInsertSerializer,
    ItemDisplaySerializer,
    ItemDisplayShortSerializer,
    ClientInsertSerializer,
    ClientDisplaySerializer,
    ProductCategoryInsertSerializer,
    ProductCategoryDisplaySerializer,
    ProductInsertSerializer,
    ProductDisplaySerializer,
    ProductDisplayShortSerializer,
    PaymentDisplayShortSerializer,
    PaymentDisplaySerializer,
    PaymentInsertSerializer,
    RentPaymentInsertSerializer,
    RentPaymentInsertSerializer,
    RentPaymentDisplaySerializer,
    RentPaymentDisplayShortSerializer,
    RentPaymentInsertSerializer)
# models
from .models import (
    Product,
    ProductCategory,
    Client,
    Invoice,
    InvoiceItem,
    Payment)


'''
PRODUCT CATEGORY views
'''


class ProductCategoryViewSet(viewsets.ModelViewSet):
    serializer_class = ProductCategoryDisplaySerializer
    queryset = ProductCategory.objects.all()

    def create(self, request):
        serializer = ProductCategoryInsertSerializer(data=request.data)

        if serializer.is_valid():
            serializer.validated_data['by_user'] = request.user
            prod_cat = serializer.create(data=serializer.validated_data)

            return Response(
                data=ProductCategoryDisplaySerializer(prod_cat).data,
                status=status.HTTP_201_CREATED)
        else:
            return Response(
                data={'detail': serializer.errors},
                status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk):

        try:
            prod_cat = ProductCategory.objects.get(id=pk)
            ProductCategory.objects.delete(prod_cat=prod_cat)
            return Response(status=status.HTTP_204_NO_CONTENT)
        except:
            return Response(status=status.HTTP_204_NO_CONTENT)

    def update(self, request, pk):
        try:
            prod_cat = ProductCategory.objects.get(id=pk)

            serializer = ProductCategoryInsertSerializer(data=request.data)

            if serializer.is_valid():
                serializer.validated_data['by_user'] = request.user
                prod_cat = serializer.update(
                    obj=prod_cat,
                    data=serializer.validated_data)
            else:
                return Response(
                    data={'detail': serializer.errors},
                    status=status.HTTP_400_BAD_REQUEST
                    )

            return Response(
                data=ProductCategoryDisplaySerializer(prod_cat).data,
                status=status.HTTP_200_OK)

        except ProductCategory.DoesNotExist:
            return Response(
                data={'detail': 'product category does not exist'},
                status=status.HTTP_404_NOT_FOUND)
        except Exception as e:
            return Response(
                {'detail': 'invalid data'},
                status.HTTP_400_BAD_REQUEST
                )

    def partial_update(self, request, pk):
        """
        METHOD NOT ALLOWED
        """
        return Response(
            data={'detail': 'method not allowed'},
            status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def get_serializer_class(self):
        if self.action == 'list' or\
                self.action == 'retrieve':

            return ProductCategoryDisplaySerializer
        else:
            return ProductCategoryInsertSerializer


'''
PRODUCT VIEWS
'''


class ProductViewSet(viewsets.ModelViewSet):
    serializer_class = ProductDisplaySerializer
    queryset = Product.objects.all()

    def create(self, request):
        serializer = ProductInsertSerializer(data=request.data)

        if serializer.is_valid():
            serializer.validated_data['by_user'] = request.user
            prod = serializer.create(data=serializer.validated_data)

            return Response(
                data=ProductDisplaySerializer(prod).data,
                status=status.HTTP_201_CREATED)
        else:
            return Response(
                data={'detail': serializer.errors},
                status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk):
        try:
            prod = Product.objects.get(id=pk)
            Product.objects.delete(prod=prod, user=request.user)
            return Response(status=status.HTTP_204_NO_CONTENT)
        except:
            return Response(status=status.HTTP_204_NO_CONTENT)

    def update(self, request, pk):
        try:
            prod_target = Product.objects.get(id=pk)
            serializer = ProductInsertSerializer(data=request.data)

            if serializer.is_valid():
                serializer.validated_data['by_user'] = request.user
                prod = serializer.update(
                    obj=prod_target,
                    data=serializer.validated_data)

                return Response(
                    data=ProductDisplaySerializer(prod).data,
                    status=status.HTTP_200_OK)
            else:
                return Response(
                    data={'detail': serializer.errors},
                    status=status.HTTP_400_BAD_REQUEST)

        except Product.DoesNotExist:
            return Response(
                data={'detail': 'product not found'},
                status=status.HTTP_404_NOT_FOUND)

    def partial_update(self, request, pk):
        """
        METHOD NOT IMPLEMENTED
        """
        return Response(
            data={'detail': 'method not allowed'},
            status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def get_serializer_class(self):
        if self.action == 'list':
            return ProductDisplayShortSerializer
        elif self.action == 'retrieve':
            return ProductDisplaySerializer
        else:
            return ProductInsertSerializer


'''
CLIENT VIEWS
'''


class ClientViewSet(viewsets.ModelViewSet):
    serializer_class = ClientDisplaySerializer
    queryset = Client.objects.all()

    def create(self, request):
        serializer = ClientInsertSerializer(data=request.data)

        if serializer.is_valid():
            serializer.validated_data['by_user'] = request.user
            serializer.create(data=serializer.validated_data)
            return Response(
                data={},
                status=status.HTTP_201_CREATED)
        else:
            return Response(
                data=serializer.errors,
                status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk):
        try:
            client = Client.objects.get(id=pk)
            Client.objects.delete(client=client)
            return Response(status=status.HTTP_204_NO_CONTENT)
        except:
            return Response(status=status.HTTP_204_NO_CONTENT)

    def update(self, request, pk):
        try:
            client = Client.objects.get(id=pk)
            serializer = ClientInsertSerializer(request.data)
            if serializer.is_valid():
                Client.objects.update(
                    client=client,
                    client_dict=client)
                return Response(
                    data={},
                    status=status.HTTP_200_OK)
            else:
                return Response(
                    data={'detail': 'invalid data submitted'},
                    status=status.HTTP_400_BAD_REQUEST)

        except Client.DoesNotExist:
            return Response(
                data={'detail': 'client not found'},
                status=status.HTTP_404_NOT_FOUND
                )

        return Response(
            data={'detail': 'method not allowed'},
            status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def partial_update(self, request, pk):
        """
        METHOD NOT IMPLEMENTED
        """
        return Response(
            data={'detail': 'method not allowed'},
            status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def get_serializer_class(self):
        if self.action == 'list' or\
                self.action == 'retrieve':

            return ClientDisplaySerializer
        else:
            return ClientInsertSerializer


'''
RENT VIEWS
'''


class RentViewSet(viewsets.ModelViewSet):
    serializer_class = RentDisplaySerializer
    queryset = Invoice.objects.filter(internal_type='RENT')

    def create(self, request):
        print('RENT INSERT SERIALIZER')
        serializer = RentInsertSerializer(data=request.data)
        if serializer.is_valid():
            serializer.validated_data['internal_type'] = 'RENT'
            serializer.validated_data['by_user'] = request.user
            rent = serializer.create(data=serializer.validated_data)
            return Response(
                data=RentDisplaySerializer(rent).data,
                status=status.HTTP_201_CREATED)
        else:
            return Response(
                data=serializer.errors,
                status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk):
        try:
            invoice = Invoice.objects.get(id=pk)
            Invoice.objects.delete(
                invoice=invoice,
                user=request.user)
            return Response(
                status=status.HTTP_204_NO_CONTENT)

        except Invoice.DoesNotExist:
            return Response(
                status=status.HTTP_404_NOT_FOUND)

    def update(self, request, pk):
        try:
            invoice = Invoice.objects.get(id=pk)

            if invoice.status == 'PAR_PAY' or\
                    invoice.status == 'PAID':
                return Response(
                    data={
                        'detail':
                        'cannot modify an invoice with payments,' +
                        ' remove payments first'
                        },
                    status=status.HTTP_400_BAD_REQUEST)
            elif invoice.status == 'CANCELED':
                return Response(
                    data={'detail': 'invoice is canceled, cannot modify it'},
                    status=status.HTTP_400_BAD_REQUEST)
            else:
                serializer = RentUpdateSerializer(data=request.data)
                if serializer.is_valid():
                    serializer.validated_data['by_user'] = request.user
                    new_invoice = serializer.update(
                        obj=invoice,
                        data=serializer.validated_data)

                    return Response(
                        data=RentDisplaySerializer(new_invoice)
                        .data,
                        status=status.HTTP_200_OK)
                else:
                    return Response(
                        data=serializer.errors,
                        status=status.HTTP_400_BAD_REQUEST)

        except Invoice.DoesNotExist:
            return Response(
                status=status.HTTP_404_NOT_FOUND)
        except IntegrityError as e:
            return Response(
                data={'detail': str(e)},
                status=status.HTTP_400_BAD_REQUEST)
        except ValueError as e:
            return Response(
                data={'detail': str(e)},
                status=status.HTTP_400_BAD_REQUEST)

    def partial_update(self, request, pk):
        """
        METHOD NOT IMPLEMENTED
        """
        return Response(
            data={'detail': 'method not allowed'},
            status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def get_serializer_class(self):
        if self.action == 'list':
            return RentDisplayShortSerializer

        elif self.action == 'retrieve':
            return RentDisplaySerializer

        else:
            return RentInsertSerializer
"""
INTERNAL INVOICE VIEWS
"""


class InternalInvoiceViewSet(viewsets.ModelViewSet):
    serializer_class = InternalInvoiceDisplaySerializer
    queryset = Invoice.objects.filter(internal_type='INTERNAL')

    def create(self, request):
        serializer = InternalInvoiceInsertSerializer(data=request.data)
        if serializer.is_valid():
            serializer.validated_data['internal_type'] = 'INTERNAL'
            serializer.validated_data['by_user'] = request.user
            rent = serializer.create(data=serializer.validated_data)
            return Response(
                data=InternalInvoiceDisplaySerializer(rent).data,
                status=status.HTTP_201_CREATED)
        else:
            return Response(
                data=serializer.errors,
                status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk):
        try:
            invoice = Invoice.objects.get(id=pk)
            Invoice.objects.delete(
                invoice=invoice,
                user=request.user)
            return Response(
                status=status.HTTP_204_NO_CONTENT)

        except Invoice.DoesNotExist:
            return Response(
                status=status.HTTP_404_NOT_FOUND)

    def update(self, request, pk):
        try:
            invoice = Invoice.objects.get(id=pk)

            if invoice.status == 'PAR_PAY' or\
                    invoice.status == 'PAID':
                return Response(
                    data={
                        'detail':
                        'cannot modify an invoice with payments,' +
                        ' remove payments first'
                        },
                    status=status.HTTP_400_BAD_REQUEST)
            elif invoice.status == 'CANCELED':
                return Response(
                    data={'detail': 'invoice is canceled, cannot modify it'},
                    status=status.HTTP_400_BAD_REQUEST)
            else:
                serializer = InternalInvoiceInsertSerializer(data=request.data)
                if serializer.is_valid():
                    serializer.validated_data['by_user'] = request.user
                    new_invoice = serializer.update(
                        obj=invoice,
                        data=serializer.validated_data)

                    return Response(
                        data=InternalInvoiceDisplaySerializer(new_invoice)
                        .data,
                        status=status.HTTP_200_OK)
                else:
                    return Response(
                        data=serializer.errors,
                        status=status.HTTP_400_BAD_REQUEST)

        except Invoice.DoesNotExist:
            return Response(
                status=status.HTTP_404_NOT_FOUND)
        except IntegrityError as e:
            return Response(
                data={'detail': str(e)},
                status=status.HTTP_400_BAD_REQUEST)
        except ValueError as e:
            return Response(
                data={'detail': str(e)},
                status=status.HTTP_400_BAD_REQUEST)

    def partial_update(self, request, pk):
        """
        METHOD NOT IMPLEMENTED
        """
        return Response(
            data={'detail': 'method not allowed'},
            status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def get_serializer_class(self):
        if self.action == 'list' or\
                self.action == 'retrieve':

            return InternalInvoiceDisplaySerializer
        else:
            return InternalInvoiceInsertSerializer

"""
EXTERNAL INVOICES
"""


class ExternalInvoiceViewSet(viewsets.ModelViewSet):
    serializer_class = ExternalInvoiceDisplaySerializer
    queryset = Invoice.objects.filter(internal_type='EXTERNAL')

    def create(self, request):
        serializer = InternalInvoiceInsertSerializer(data=request.data)
        if serializer.is_valid():
            serializer.validated_data['internal_type'] = 'EXTERNAL'
            serializer.validated_data['by_user'] = request.user
            rent = serializer.create(data=serializer.validated_data)
            return Response(
                data=InternalInvoiceDisplaySerializer(rent).data,
                status=status.HTTP_201_CREATED)
        else:
            return Response(
                data=serializer.errors,
                status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk):
        try:
            invoice = Invoice.objects.get(id=pk)
            Invoice.objects.delete(
                invoice=invoice,
                user=request.user)
            return Response(
                status=status.HTTP_204_NO_CONTENT)

        except Invoice.DoesNotExist:
            return Response(
                status=status.HTTP_404_NOT_FOUND)

    def update(self, request, pk):
        try:
            invoice = Invoice.objects.get(id=pk)

            if invoice.status == 'PAR_PAY' or\
                    invoice.status == 'PAID':
                return Response(
                    data={
                        'detail':
                        'cannot modify an invoice with payments,' +
                        ' remove payments first'
                        },
                    status=status.HTTP_400_BAD_REQUEST)
            elif invoice.status == 'CANCELED':
                return Response(
                    data={'detail': 'invoice is canceled, cannot modify it'},
                    status=status.HTTP_400_BAD_REQUEST)
            else:
                serializer = InternalInvoiceInsertSerializer(data=request.data)
                if serializer.is_valid():
                    serializer.validated_data['by_user'] = request.user
                    new_invoice = serializer.update(
                        obj=invoice,
                        data=serializer.validated_data)

                    return Response(
                        data=InternalInvoiceDisplaySerializer(new_invoice)
                        .data,
                        status=status.HTTP_200_OK)
                else:
                    return Response(
                        data=serializer.errors,
                        status=status.HTTP_400_BAD_REQUEST)

        except Invoice.DoesNotExist:
            return Response(
                status=status.HTTP_404_NOT_FOUND)
        except IntegrityError as e:
            return Response(
                data={'detail': str(e)},
                status=status.HTTP_400_BAD_REQUEST)
        except ValueError as e:
            return Response(
                data={'detail': str(e)},
                status=status.HTTP_400_BAD_REQUEST)

    def partial_update(self, request, pk):
        """
        METHOD NOT IMPLEMENTED
        """
        return Response(
            data={'detail': 'method not allowed'},
            status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def get_serializer_class(self):
        if self.action == 'list' or\
                self.action == 'retrieve':

            return ExternalInvoiceDisplaySerializer
        else:
            return ExternalInvoiceInsertSerializer

'''
ITEM viewset
'''


class ItemViewSet(viewsets.ModelViewSet):
    serializer_class = ItemDisplaySerializer
    queryset = InvoiceItem.objects.filter()

    def create(
            self,
            request,
            rent_pk=None,
            internal_invoice_pk=None,
            external_invoice_pk=None):
        try:
            if rent_pk is not None:
                inv_pk = rent_pk
            elif internal_invoice_pk is not None:
                inv_pk = internal_invoice_pk
            elif external_invoice_pk is not None:
                inv_pk = external_invoice_pk
            else:
                return Response(
                    data={'detail': 'invalid request'},
                    status=status.HTTP_400_BAD_REQUEST)

            invoice = Invoice.objects.get(id=inv_pk)

            serializer = ItemInsertSerializer(data=request.data)
            if serializer.is_valid():
                serializer.validated_data['invoice'] = invoice
                serializer.validated_data['by_user'] = request.user
                item = serializer.create(data=serializer.validated_data)

                return Response(
                    data=ItemDisplaySerializer(item).data,
                    status=status.HTTP_201_CREATED)

            else:
                return Response(
                    data=serializer.errors,
                    status=status.HTTP_400_BAD_REQUEST)

        except Invoice.DoesNotExist:
            return Response(
                data={'detail': 'root document does not exist'},
                status=status.HTTP_404_NOT_FOUND)

        except IntegrityError as e:
            return Response(
                data={'detail': str(e)},
                status=status.HTTP_400_BAD_REQUEST)

    def list(
            self,
            request,
            rent_pk=None,
            internal_invoice_pk=None,
            external_invoice_pk=None):
        if rent_pk is not None:
            serializer = self.list_serialize_items(
                request, rent_pk)
        elif internal_invoice_pk is not None:
            serializer = self.list_serialize_items(
                request, internal_invoice_pk)
        elif external_invoice_pk is not None:
            serializer = self.list_serialize_items(
                request, external_invoice_pk)
        else:
            return Response(
                status=status.HTTP_400_BAD_REQUEST)

        return Response(
            data=serializer.data,
            status=status.HTTP_200_OK)

    def destroy(self, request, pk):
        return Response(
            data={'detail': 'method not allowed'},
            status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def update(self, request, pk):
        return Response(
            data={'detail': 'method not allowed'},
            status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def partial_update(self, request, pk):
        """
        METHOD NOT IMPLEMENTED
        """
        return Response(
            data={'detail': 'method not allowed'},
            status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def get_serializer_class(self):
        if self.action == 'list' or\
                self.action == 'retrieve':

            return ItemDisplaySerializer
        else:
            return ItemInsertSerializer

    def list_serialize_items(self, request, _id):
        queryset = self.filter_queryset(
            InvoiceItem.objects.filter(invoice__id=_id)
        )

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = ItemDisplayShortSerializer(
                page,
                many=True,
                context={'request': request})
            return self.get_paginated_response(serializer.data)

        serializer = ItemDisplaySerializer(
            queryset,
            many=True,
            context={'request', request}
            )
        return serializer

'''
PAYMENT viewset
'''


class PaymentViewset(viewsets.ModelViewSet):
    serializer_class = PaymentDisplaySerializer
    queryset = Payment.objects.filter()

    def create(
            self,
            request,
            rent_pk=None,
            internal_invoice_pk=None,
            external_invoice_pk=None):
        try:
            if internal_invoice_pk is not None:
                invoice = Invoice.objects.get(id=internal_invoice_pk)
            elif external_invoice_pk is not None:
                invoice = Invoice.objects.get(id=external_invoice_pk)
            elif rent_pk is not None:
                invoice = Invoice.objects.get(id=rent_pk)
            else:
                return Response(
                    status=status.HTTP_400_BAD_REQUEST)
            serializer = PaymentInsertSerializer(request.data)
            if serializer.is_valid():
                serializer.validated_data['by_user'] = request.user
                serializer.validated_data['invoice'] = invoice

                payment = serializer.create(data=serializer.validated_data)

                return Response(
                    data=PaymentDisplaySerializer(payment).data,
                    status=status.HTTP_201_CREATED)

            else:
                return Response(
                    data=serializer.errors,
                    status=status.HTTP_400_BAD_REQUEST)

        except IntegrityError as e:
            return Response(
                data={'detail': str(e)},
                status=status.HTTP_400_BAD_REQUEST)
        except Invoice.DoesNotExist:
            return Response(
                data={'detail': 'root document not found'},
                status=status.HTTP_404_NOT_FOUND)

    def list(
            self,
            request,
            rent_pk=None,
            internal_invoice_pk=None,
            external_invoice_pk=None):
        if rent_pk is not None:
            serializer = self.list_serialize_items(
                request, rent_pk)
        elif internal_invoice_pk is not None:
            serializer = self.list_serialize_items(
                request, internal_invoice_pk)
        elif external_invoice_pk is not None:
            serializer = self.list_serialize_items(
                request, external_invoice_pk)
        else:
            return Response(
                status=status.HTTP_400_BAD_REQUEST)

        return Response(
            data=serializer.data,
            status=status.HTTP_200_OK)

    def destroy(self, request, pk):
        return Response(
            data={'detail': 'method not allowed'},
            status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def update(self, request, pk):
        return Response(
            data={'detail': 'method not allowed'},
            status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def partial_update(self, request, pk):
        """
        METHOD NOT IMPLEMENTED
        """
        return Response(
            data={'detail': 'method not allowed'},
            status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def get_serializer_class(self):
        if self.action == 'list' or\
                self.action == 'retrieve':

            return ItemDisplaySerializer
        else:
            return ItemInsertSerializer

    def list_serialize_payments(self, _type, request, _id):
        if _type == 'rent':
            queryset = self.filter_queryset(
                Payment.objects.filter(invoice__id=_id)
            )

            page = self.paginate_queryset(queryset)
            if page is not None:
                serializer = RentPaymentDisplayShortSerializer(
                    page,
                    many=True,
                    context={'request': request})
                return self.get_paginated_response(serializer.data)

            serializer = RentPaymentDisplayShortSerializer(
                queryset,
                many=True,
                context={'request', request}
                )
            return serializer
        else:
            queryset = self.filter_queryset(
                Payment.objects.filter(invoice__id=_id)
            )

            page = self.paginate_queryset(queryset)
            if page is not None:
                serializer = PaymentDisplayShortSerializer(
                    page,
                    many=True,
                    context={'request': request})
                return self.get_paginated_response(serializer.data)

            serializer = PaymentDisplayShortSerializer(
                queryset,
                many=True,
                context={'request', request}
                )
            return serializer

'''
RENT PAYMENT viewset
'''


class RentPaymentViewset(viewsets.ModelViewSet):
    serializer_class = RentPaymentDisplaySerializer
    queryset = Payment.objects.filter()

    def create(
            self,
            request,
            rent_pk=None,
            internal_invoice_pk=None,
            external_invoice_pk=None):
        try:
            if rent_pk is not None:
                invoice = Invoice.objects.get(id=rent_pk)
            elif internal_invoice_pk is not None:
                invoice = Invoice.objects.get(id=internal_invoice_pk)
            elif external_invoice_pk is not None:
                invoice = Invoice.objects.get(id=external_invoice_pk)
            else:
                return Response(
                    status=status.HTTP_400_BAD_REQUEST)
            serializer = RentPaymentInsertSerializer(data=request.data)
            if serializer.is_valid():
                serializer.validated_data['by_user'] = request.user
                serializer.validated_data['invoice'] = invoice

                new_payment = serializer.create(serializer.validated_data)
                
                return Response(
                    data={PaymentDisplaySerializer(new_payment).data},
                    status=status.HTTP_201_CREATED
                    )
            else:
                return Response(
                    data=serializer.errors,
                    status=status.HTTP_400_BAD_REQUEST
                    )

        except IntegrityError as e:
            return Response(
                data={'detail': str(e)},
                status=status.HTTP_400_BAD_REQUEST)
        except Invoice.DoesNotExist:
            return Response(
                data={'detail': 'root document not found'},
                status=status.HTTP_404_NOT_FOUND)

    def list(
            self,
            request,
            rent_pk=None):
        if rent_pk is not None:
            serializer = self.list_serialize_payments(
                request, rent_pk)
        else:
            return Response(
                status=status.HTTP_400_BAD_REQUEST)

        return Response(
            data=serializer.data,
            status=status.HTTP_200_OK)

    def destroy(self, request, pk):
        return Response(
            data={'detail': 'method not allowed'},
            status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def update(self, request, pk):
        # update notes
        return Response(
            data={'detail': 'method not allowed'},
            status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def partial_update(self, request, pk):
        """
        METHOD NOT IMPLEMENTED
        """
        return Response(
            data={'detail': 'method not allowed'},
            status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def get_serializer_class(self):
        if self.action == 'list' or\
                self.action == 'retrieve':

            return ItemDisplaySerializer
        else:
            return ItemInsertSerializer

    def list_serialize_payments(self, request, _id):

        queryset = self.filter_queryset(
            Payment.objects.filter(invoice__id=_id))

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = RentPaymentDisplayShortSerializer(
                page,
                many=True,
                context={'request': request})
            return self.get_paginated_response(serializer.data)

        serializer = RentPaymentDisplayShortSerializer(
            queryset,
            many=True,
            context={'request', request}
            )
        return serializer
       