from django.db import IntegrityError
from django.contrib.auth import get_user_model
from rest_framework import serializers
from coopmsapi.totalizers.serializers import BalanceDisplayShortSerializer
from coopmsapi.locale.serializers import LocalizedTextSerializer
from coopmsapi.users.serializers import UserDisplayMiniSerializer
from coopmsapi.accounting.models import GLAccount
from coopmsapi.accounting.banking_models import BankAccount
from coopmsapi.accounting.serializers import (
    GLAccountDisplayShortSerializer)
from coopmsapi.accounting.banking_serializers import(
    BankAccountSerializer
    )
from .models import (
        Invoice,
        InvoiceItem,
        Client,
        Product,
        ProductCategory,
        Payment)


"""
PRODUCT CATEGORY serializer
"""


class ProductCategoryInsertSerializer(
        serializers.ModelSerializer):
    name = LocalizedTextSerializer(many=True)
    is_active = serializers.BooleanField(required=False)

    class Meta:
        model = ProductCategory
        fields = (
            'internal_type',
            'is_active',
            'name')

    def create(self, data):
        return ProductCategory.objects.create(
            prod_cat_dict=data)

    def update(self, obj, data):
        return ProductCategory.objects.update(
            prod_cat=obj,
            prod_cat_dict=data)


class ProductCategoryDisplaySerializer(
        serializers.ModelSerializer):
    name = LocalizedTextSerializer(many=True)
    is_active = serializers.BooleanField(required=False)
    created_by = UserDisplayMiniSerializer(read_only=True)
    created_date = serializers.DateTimeField(read_only=True)
    modified_by = UserDisplayMiniSerializer(read_only=True)
    modified_date = serializers.DateTimeField(read_only=True)

    class Meta:
        model = ProductCategory
        fields = (
            'id',
            'internal_type',
            'is_active',
            'name',
            'created_by',
            'created_date',
            'modified_by',
            'modified_date')


class ProductCategoryDisplayShortSerializer(
        serializers.ModelSerializer):
    name = LocalizedTextSerializer(many=True)

    class Meta:
        model = ProductCategory
        fields = (
            'id',
            'name',
            'is_active',
            )

"""
Product serializers
"""


class ProductInsertSerializer(serializers.ModelSerializer):
    name = LocalizedTextSerializer(many=True)
    description = LocalizedTextSerializer(many=True)
    product_gl_account = serializers.PrimaryKeyRelatedField(
        queryset=GLAccount.objects.all(),
        write_only=True,
        many=False)
    category = serializers.PrimaryKeyRelatedField(
        queryset=ProductCategory.objects.all(),
        write_only=True,
        many=False)
    active_price = serializers.DecimalField(max_digits=8, decimal_places=2)

    class Meta:
        model = Product
        fields = (
            'id',
            'category',
            'is_taxable',
            'is_active',
            'overridable',
            'name',
            'description',
            'product_gl_account',
            'active_price')

    def create(self, data):
        return Product.objects.create(prod_dict=data)

    def update(self, obj, data):
        return Product.objects.update(prod=obj, prod_dict=data)


class ProductDisplaySerializer(serializers.ModelSerializer):
    name = LocalizedTextSerializer(many=True)
    description = LocalizedTextSerializer(many=True)
    created_by = UserDisplayMiniSerializer(read_only=True)
    created_date = serializers.DateTimeField(read_only=True)
    modified_by = UserDisplayMiniSerializer(read_only=True)
    modified_date = serializers.DateTimeField(read_only=True)

    class Meta:
        model = Product
        fields = (
            'id',
            'category',
            'is_active',
            'overridable',
            'name',
            'description',
            'product_gl_account',
            'active_price',
            'created_by',
            'created_date',
            'modified_by',
            'modified_date',
            )


class ProductDisplayShortSerializer(serializers.ModelSerializer):
    name = LocalizedTextSerializer(many=True)

    class Meta:
        model = Product
        fields = (
            'id',
            'category',
            'is_active',
            'overridable',
            'name',
            'product_gl_account',
            'active_price')


"""
Client serializer
"""


class ClientInsertSerializer(serializers.ModelSerializer):

    client_gl_account = serializers.PrimaryKeyRelatedField(
        queryset=GLAccount.objects.all(),
        write_only=True,
        required=False)

    client_gl_account_pay_over = serializers.PrimaryKeyRelatedField(
        queryset=GLAccount.objects.all(),
        write_only=True,
        required=False)

    class Meta:
        model = Client
        fields = (
            'id',
            'name',
            'primary_phone',
            'address_line_1',
            'address_line_2',
            'city',
            'province',
            'client_gl_account',
            'client_gl_account_pay_over')

    def create(self, data):
        Client.objects.create(
            client_dict=data)

    def update(self, client, data):
        Client.objects.update(
            client=client,
            client_dict=data)


class ClientDisplaySerializer(serializers.ModelSerializer):

    balance = BalanceDisplayShortSerializer()
    client_gl_account = GLAccountDisplayShortSerializer(read_only=True)
    client_gl_account_pay_over = GLAccountDisplayShortSerializer(
        read_only=True)
    created_by = serializers.PrimaryKeyRelatedField(read_only=True)
    created_date = serializers.DateTimeField(read_only=True)
    modified_by = serializers.PrimaryKeyRelatedField(read_only=True)
    modified_date = serializers.DateTimeField(read_only=True)

    class Meta:
        model = Client
        fields = (
            'id',
            'name',
            'primary_phone',
            'address_line_1',
            'address_line_2',
            'city',
            'province',
            'client_gl_account',
            'client_gl_account_pay_over',
            'balance',
            'created_by',
            'created_date',
            'modified_by',
            'modified_date')


class ClientDisplayShortSerializer(serializers.ModelSerializer):

    balance = BalanceDisplayShortSerializer()

    class Meta:
        model = Client
        fields = (
            'id',
            'name',
            'balance')


"""
Item serializer
"""


class ItemInsertSerializer(serializers.ModelSerializer):
    product = serializers.PrimaryKeyRelatedField(
        queryset=Product.objects.all())
    price = serializers.DecimalField(
        max_digits=8,
        decimal_places=2,
        required=False)
    is_canceled = serializers.BooleanField(required=False)

    class Meta:
        model = InvoiceItem
        fields = (
            'id',
            'product',
            'quantity',
            'price',
            'is_canceled')

    def create(self, data):
        return InvoiceItem.objects.create(item_dict=data, update_inv=True)

    def update(self, obj, data):
        InvoiceItem.objects.update(invoice_item=obj, invoice_item_dict=data)


class ItemDisplaySerializer(serializers.ModelSerializer):
    invoice = serializers.PrimaryKeyRelatedField(
        required=False,
        queryset=Invoice.objects.all())
    name = serializers.CharField(max_length=1024)
    gl_account = serializers.PrimaryKeyRelatedField(read_only=True)
    product = serializers.PrimaryKeyRelatedField(
        queryset=Product.objects.all())
    line_total = serializers.DecimalField(
        max_digits=16,
        decimal_places=2,
        read_only=True)
    is_canceled = serializers.BooleanField(required=False)
    created_by = serializers.PrimaryKeyRelatedField(read_only=True)
    created_date = serializers.DateTimeField(read_only=True)
    modified_by = serializers.PrimaryKeyRelatedField(read_only=True)
    modified_date = serializers.DateTimeField(read_only=True)

    class Meta:
        model = InvoiceItem
        fields = (
            'id',
            'invoice',
            'name',
            'gl_account',
            'product',
            'quantity',
            'price',
            'line_total',
            'is_canceled',
            'created_by',
            'created_date',
            'modified_by',
            'modified_date'
            )


class ItemDisplayShortSerializer(serializers.ModelSerializer):
    name = serializers.CharField(max_length=1024)
    gl_account = serializers.PrimaryKeyRelatedField(read_only=True)
    product = serializers.PrimaryKeyRelatedField(
        queryset=Product.objects.all())
    line_total = serializers.DecimalField(
        max_digits=16,
        decimal_places=2,
        read_only=True)
    is_canceled = serializers.BooleanField(read_only=True)

    class Meta:
        model = InvoiceItem
        fields = (
            'id',
            'name',
            'gl_account',
            'product',
            'quantity',
            'price',
            'line_total',
            'is_canceled')

"""
PAYMENT serializer
"""


class PaymentInsertSerializer(serializers.ModelSerializer):
    bank_account = serializers.PrimaryKeyRelatedField(
        queryset=BankAccount.objects.filter(is_inactive=False),
        required=True)

    class Meta:
        model = Payment
        fields = (
            'date',
            'bank_account',
            'amount')


class PaymentDisplaySerializer(serializers.ModelSerializer):
    created_by = serializers.PrimaryKeyRelatedField(read_only=True)
    created_date = serializers.DateTimeField(read_only=True)
    modified_by = serializers.PrimaryKeyRelatedField(read_only=True)
    modified_date = serializers.DateTimeField(read_only=True)
    bank_account = BankAccountSerializer(
        read_only=True,
        many=False)

    class Meta:
        model = Payment
        fields = (
            'id',
            'date',
            'bank_account',
            'amount',
            'created_by',
            'created_date',
            'modified_by',
            'modified_date')


class PaymentDisplayShortSerializer(serializers.ModelSerializer):
    bank_account = serializers.PrimaryKeyRelatedField(
        queryset=BankAccount.objects.filter(is_inactive=False),
        required=True)

    class Meta:
        model = Payment
        fields = (
            'date',
            'amount')


"""
RENT PAYMENT serializer
"""


class RentPaymentInsertSerializer(serializers.ModelSerializer):
    subsidy = serializers.CharField(max_length=8, required=True)
    bank_account = serializers.PrimaryKeyRelatedField(
        queryset=BankAccount.objects.filter(is_inactive=True),
        required=True)
    notes = serializers.CharField(max_length=2048, required=False)

    class Meta:
        model = Payment
        fields = (
            'date',
            'subsidy',
            'bank_account',
            'amount',
            'notes')

    def create(self, data):
        return Payment.objects.create(payment_dict=data)

    def update(self):
        raise IntegrityError('cannot update payment')


class RentPaymentUpdateSerializer(serializers.ModelSerializer):
    notes = serializers.CharField(max_length=2048, required=False)
    class Meta:
        model = Payment
        fields = (
            'notes')

    def create(self):
        raise IntegrityError('cannot update payment')

    def update(self, obj, data):
        return Payment.objects.update(
            payment=obj,
            payment_dict=data)


class RentPaymentDisplaySerializer(serializers.ModelSerializer):
    created_by = serializers.PrimaryKeyRelatedField(read_only=True)
    created_date = serializers.DateTimeField(read_only=True)
    modified_by = serializers.PrimaryKeyRelatedField(read_only=True)
    modified_date = serializers.DateTimeField(read_only=True)
    bank_account = BankAccountSerializer(
        read_only=True,
        many=False)

    class Meta:
        model = Payment
        fields = (
            'id',
            'date',
            'subsidy',
            'bank_account',
            'amount',
            'created_by',
            'created_date',
            'modified_by',
            'modified_date')


class RentPaymentDisplayShortSerializer(serializers.ModelSerializer):
    bank_account = serializers.PrimaryKeyRelatedField(
        queryset=BankAccount.objects.filter(
            is_inactive=False),
            required=True)

    class Meta:
        model = Payment
        fields = (
            'date',
            'amount',
            'bank_account',
            'subsidy',)

"""
Rent serializer
"""


class RentInsertSerializer(serializers.ModelSerializer):
    date = serializers.DateTimeField()
    user = serializers.PrimaryKeyRelatedField(
        queryset=get_user_model().objects.all(),
        required=True)
    items = ItemInsertSerializer(many=True)
    status = serializers.CharField(max_length=16, required=False)
    notes = serializers.CharField(max_length=2048, required=False)

    class Meta:
        model = Invoice
        fields = (
            'date',
            'user',
            'status',
            'items',
            'notes')

    def create(self, data):
        return Invoice.objects.create(invoice_dict=data)

    def update(self):
        raise IntegrityError(
            'Implementation error, use RentUpdateSerializer to update')

class RentUpdateSerializer(serializers.ModelSerializer):
    date = serializers.DateTimeField()
    user = serializers.PrimaryKeyRelatedField(
        queryset=get_user_model().objects.all(),
        required=True)
    notes = serializers.CharField(max_length=2048, required=False)
    status = serializers.CharField(max_length=16, required=False)

    class Meta:
        model = Invoice
        fields = (
            'date',
            'user',
            'status',
            'notes')

    # def create(self):
    #    raise IntegrityError(
    #        'Implementation error, use RentInsertSerializer to create')

    def update(self, obj, data):
        return Invoice.objects.update(invoice=obj, invoice_dict=data)


class RentDisplaySerializer(serializers.ModelSerializer):
    date = serializers.DateTimeField()
    asset_gl_account = serializers.PrimaryKeyRelatedField(read_only=True)
    transaction = serializers.PrimaryKeyRelatedField(read_only=True)
    user = UserDisplayMiniSerializer(read_only=True)
    amount = serializers.DecimalField(max_digits=16, decimal_places=2)
    items = ItemDisplayShortSerializer(
        many=True,
        source='invoiceitem_invoice_ar')
    payments = RentPaymentDisplayShortSerializer(
        read_only=True,
        source='payment_invoice_ar',
        many=True)
    created_by = serializers.PrimaryKeyRelatedField(read_only=True)
    created_date = serializers.DateTimeField(read_only=True)
    modified_by = serializers.PrimaryKeyRelatedField(read_only=True)
    modified_date = serializers.DateTimeField(read_only=True)

    class Meta:
        model = Invoice
        fields = (
            'id',
            'rent_no',
            'date',
            'asset_gl_account',
            'status',
            'transaction',
            'user',
            'amount',
            'balance',
            'items',
            'payments',
            'created_by',
            'created_date',
            'modified_by',
            'modified_date')


class RentDisplayShortSerializer(serializers.ModelSerializer):
    date = serializers.DateTimeField()
    user = UserDisplayMiniSerializer(read_only=True)
    amount = serializers.DecimalField(max_digits=16, decimal_places=2)

    class Meta:
        model = Invoice
        fields = (
            'id',
            'rent_no',
            'status',
            'date',
            'user',
            'amount',
            'balance')
"""
Intenal Invoice serializer
"""


class InternalInvoiceInsertSerializer(serializers.ModelSerializer):
    date = serializers.DateTimeField()
    user = serializers.PrimaryKeyRelatedField(
        queryset=get_user_model().objects.all(),
        required=True)
    items = ItemInsertSerializer(many=True)

    class Meta:
        model = Invoice
        fields = (
            'date',
            'user',
            'items')

    def create(self, data):
        return Invoice.objects.create(invoice_dict=data)

    def update(self, obj, data):
        return Invoice.objects.update(invoice=obj, invoice_dict=data)


class InternalInvoiceDisplaySerializer(serializers.ModelSerializer):
    date = serializers.DateTimeField()
    asset_gl_account = serializers.PrimaryKeyRelatedField(read_only=True)
    transaction = serializers.PrimaryKeyRelatedField(read_only=True)
    user = UserDisplayMiniSerializer(read_only=True)
    amount = serializers.DecimalField(max_digits=16, decimal_places=2)
    items = ItemDisplaySerializer(
        many=True,
        source='invoiceitem_invoice_ar')
    created_by = serializers.PrimaryKeyRelatedField(read_only=True)
    created_date = serializers.DateTimeField(read_only=True)
    modified_by = serializers.PrimaryKeyRelatedField(read_only=True)
    modified_date = serializers.DateTimeField(read_only=True)

    class Meta:
        model = Invoice
        fields = (
            'id',
            'internal_invoice_no',
            'date',
            'asset_gl_account',
            'status',
            'transaction',
            'user',
            'amount',
            'balance',
            'items',
            'created_by',
            'created_date',
            'modified_by',
            'modified_date')

"""
External invoice serializer
"""


class ExternalInvoiceInsertSerializer(serializers.ModelSerializer):
    date = serializers.DateTimeField()
    client = serializers.PrimaryKeyRelatedField(
        queryset=Client.objects.all(),
        required=True)
    items = ItemInsertSerializer(many=True)

    class Meta:
        model = Invoice
        fields = (
            'date',
            'client',
            'items')

    def create(self, data):
        return Invoice.objects.create(invoice_dict=data)

    def update(self, obj, data):
        return Invoice.objects.update(invoice=obj, invoice_dict=data)


class ExternalInvoiceDisplaySerializer(serializers.ModelSerializer):
    date = serializers.DateTimeField()
    asset_gl_account = serializers.PrimaryKeyRelatedField(read_only=True)
    transaction = serializers.PrimaryKeyRelatedField(read_only=True)
    client = ClientDisplayShortSerializer(read_only=True)
    amount = serializers.DecimalField(max_digits=16, decimal_places=2)
    items = ItemDisplaySerializer(
        many=True,
        source='invoiceitem_invoice_ar')
    created_by = serializers.PrimaryKeyRelatedField(read_only=True)
    created_date = serializers.DateTimeField(read_only=True)
    modified_by = serializers.PrimaryKeyRelatedField(read_only=True)
    modified_date = serializers.DateTimeField(read_only=True)

    class Meta:
        model = Invoice
        fields = (
            'id',
            'external_invoice_no',
            'date',
            'asset_gl_account',
            'status',
            'transaction',
            'client',
            'amount',
            'balance',
            'items',
            'created_by',
            'created_date',
            'modified_by',
            'modified_date')


class ExternalInvoiceDisplayShortSerializer(serializers.ModelSerializer):
    class Meta:
        model = Invoice
        fields = (
            'id',
            'date',
            'external_invoice_no',
            'amount',
            'balance')
