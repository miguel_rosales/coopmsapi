from django.db.models.signals import post_save
from django.dispatch import receiver
from accounting.models import Transaction, GLAccount, GLEntry
from .models import InvoiceItem


@receiver(post_save, sender=InvoiceItem)
def distribute_gl_accounts_invoice_item(sender, **kwargs):
    invoice_item = kwargs.get('instance')
    if kwargs.get('created', True):
        invoice_item = kwargs.get('instance')
        print('InvoiceItem is created')
        my_invoice = Invoice(invoice_date=timezone.now(),internal_type='INTERNAL',user=get_user_model().objects.get(id=2),amount=300,balance=333)

    else:
        print('invoice item was modified')

        test1 = invoice_item.__original_line_total
        test2 = invoice_item.line_total
        test3 = invoice_item.__original_is_canceled
        import pdb; pdb.set_trace()  # breakpoint bd16da3a //

        invoice_item.__original_line_total = invoice_item.line_total
        invoice_item.__original_is_canceled = invoice_item.is_canceled

@receiver(post_save, sender=Invoice)
def update_deemer_balance(sender, **kwargs):
    pass
