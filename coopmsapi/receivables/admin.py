from django.contrib import admin
from solo.admin import SingletonModelAdmin
from .models import (
    Product,
    ProductCategory,
    Setting)
admin.site.register(Product)
admin.site.register(ProductCategory)
admin.site.register(Setting, SingletonModelAdmin)
