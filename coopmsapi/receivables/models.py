from django.utils import timezone
from django.db.models import Sum
from coopmsapi.settings import AUTH_USER_MODEL
from django.db import models, IntegrityError
from solo.models import SingletonModel
from coopmsapi.locale.models import Locale, LocalizedText
from coopmsapi.totalizers.models import Balance
# ACCOUNTING
from coopmsapi.accounting.models import (
                        GLAccount,
                        GLEntry,
                        Transaction,
                        Setting as AccSetting)

from coopmsapi.accounting.banking_models import BankAccount

"""
PRODUCT CATEGORY MANAGER
"""


class ProductCategoryManager(models.Manager):

    def create(self, prod_cat_dict):
        new_prod_cat = ProductCategory(
                internal_type=prod_cat_dict['internal_type'],
                is_active=prod_cat_dict['is_active'],
                created_by=prod_cat_dict['by_user'],
                modified_by=prod_cat_dict['by_user'])

        new_prod_cat.save()

        for localized_text in prod_cat_dict['name']:
            new_text = LocalizedText.objects.create(
                locale=localized_text['locale'],
                text=localized_text['text'])
            new_prod_cat.name.add(new_text)

        return new_prod_cat

    def update(self, prod_cat, prod_cat_dict):
        prod_cat.internal_type = prod_cat_dict['internal_type']
        prod_cat.is_active = prod_cat_dict['is_active']
        prod_cat.modified_by = prod_cat_dict['by_user']
        prod_cat.save()

        prod_cat.name.clear()

        for localized_text in prod_cat_dict['name']:
            new_text = LocalizedText.objects.create(
                locale=localized_text['locale'],
                text=localized_text['text'])
            prod_cat.name.add(new_text)

        return prod_cat

    def delete(self, prod_cat, user):
        if Product.objects.filter(category=prod_cat).count() == 0:
            prod_cat.name.clear()
            prod_cat.delete()
            return True
        else:
            return False

"""
PRODUCT MANAGER
"""


class ProductManager(models.Manager):

    def create(self, prod_dict):
        prod = Product(
            category=prod_dict['category'],
            overridable=prod_dict['overridable'],
            is_active=prod_dict['is_active'],
            is_taxable=prod_dict['is_taxable'],
            product_gl_account=prod_dict['product_gl_account'],
            active_price=prod_dict['active_price'],
            created_by=prod_dict['by_user'],
            created_date=timezone.now(),
            modified_by=prod_dict['by_user'],
            modified_date=timezone.now())

        prod.save()
        for localized_text in prod_dict['name']:
            new_text = LocalizedText.objects.create(
                locale=localized_text['locale'],
                text=localized_text['text'])
            prod.name.add(new_text)

        for localized_text in prod_dict['description']:
            new_text = LocalizedText.objects.create(
                locale=localized_text['locale'],
                text=localized_text['text'])
            prod.description.add(new_text)

        return prod

    def update(self, prod, prod_dict):
        prod.category = prod_dict['category']
        prod.overridable = prod_dict['overridable']
        prod.is_active = prod_dict['is_active']
        prod.is_taxable = prod_dict['is_taxable']
        prod.product_gl_account = prod_dict['product_gl_account']
        prod.active_price = prod_dict['active_price']
        prod.modified_by = prod_dict['by_user']
        prod.modified_date = timezone.now()

        prod.save()

        prod.name.clear()

        for localized_text in prod_dict['name']:
            new_text = LocalizedText.objects.create(
                locale=localized_text['locale'],
                text=localized_text['text'])
            prod.name.add(new_text)

        prod.description.clear()

        for localized_text in prod_dict['description']:
            new_text = LocalizedText.objects.create(
                locale=localized_text['locale'],
                text=localized_text['text'])
            prod.description.add(new_text)

        return prod

    def delete(self, prod, user):
        if InvoiceItem.objects.filter(product=prod).count() == 0:
            prod.name.clear()
            prod.description.clear()
            prod.delete()
            return True
        else:
            return False


"""
CLIENT MANAGER
"""


class ClientManager(models.Manager):

    def create(self, client_dict):
        balance = Balance.objects.create('CLIENT')
        client_gl_account = client_dict.get('client_gl_account')
        client_gl_account_pay_over = client_dict.get(
            'client_gl_account_pay_over')

        acc_settings = AccSetting.objects.get()

        if client_gl_account is None:
            client_gl_account = \
                acc_settings.ar_client_default_asset_gl_account

        if client_gl_account_pay_over is None:
            client_gl_account_pay_over = \
                acc_settings.ar_client_default_pay_over_gl_account

        new_client = Client(
            name=client_dict['name'],
            primary_phone=client_dict.get('primary_phone'),
            address_line_1=client_dict.get('address_line_1'),
            address_line_2=client_dict.get('address_line_2'),
            city=client_dict['city'],
            province=client_dict['province'],
            balance=balance,
            client_gl_account=client_gl_account,
            client_gl_account_pay_over=client_gl_account_pay_over,
            created_by=client_dict['by_user'],
            modified_by=client_dict['by_user'])
        new_client.save()

        return new_client

    def update(self, client, client_dict):

        client_gl_account = client_dict.get('client_gl_account')
        client_gl_account_pay_over = client_dict.get(
            'client_gl_account_pay_over')

        acc_settings = AccSetting.objects.get()

        if client_gl_account is None:
            client_gl_account = \
                acc_settings.ar_client_default_asset_gl_account

        if client_gl_account_pay_over is None:
            client_gl_account_pay_over = \
                acc_settings.ar_client_default_pay_over_gl_account

        client.name = client_dict['name']
        client.primary_phone = client_dict['primary_phone']
        client.address_line_1 = client_dict.get('address_line_1')
        client.address_line_2 = client_dict.get('address_line_2')
        client.city = client_dict['city']
        client.province = client_dict['province']
        client.client_gl_account = client_gl_account
        client.client_gl_account_pay_over = client_gl_account_pay_over
        client.modified_by = client_dict['by_user']

        client.save()

    def delete(self, client):
        if Invoice.objects.filter(client=client).count() == 0:
            client.delete()
            return True

        else:
            return False
"""
INVOICE MANAGER
"""


class InvoiceManager(models.Manager):

    def create(self, invoice_dict):
        if (invoice_dict['internal_type'] == 'INTERNAL' or
                invoice_dict['internal_type'] == 'RENT') and\
                invoice_dict.get('user') is None:
            raise IntegrityError('user missing for INTERNAL or EXTERNAL TYPE')
        if invoice_dict['internal_type'] == 'EXTERNAL' and\
                invoice_dict.get('client') is None:
            raise IntegrityError('client missing for EXTERNAL type')

        invoice_header = self.create_invoice_header(
            invoice_header_dict=invoice_dict)
        # must include invoice_header
        for item_dict in invoice_dict['items']:
            item_dict['by_user'] = invoice_dict['by_user']
            InvoiceItem.objects.create(
                item_dict=item_dict,
                invoice=invoice_header,
                transaction=invoice_header.transaction)

        total = InvoiceItem.objects.filter(
            invoice=invoice_header)\
            .aggregate(total=Sum('line_total'))
        invoice_header.amount = total['total']
        invoice_header.balance = total['total']
        invoice_header.save()
        return invoice_header

    def create_invoice_header(self, invoice_header_dict):
        acc_setting = AccSetting.objects.get()
        if invoice_header_dict['internal_type'] == 'INTERNAL':
            transaction_type = 'INI_AR'
            asset_gl_account = acc_setting.ar_member_default_asset_gl_account
        elif invoice_header_dict['internal_type'] == 'EXTERNAL':
            transaction_type = 'INX_AR'
            asset_gl_account = acc_setting.ar_client_default_asset_gl_account
        elif invoice_header_dict['internal_type'] == 'RENT':
            transaction_type = 'INR_AR'
            asset_gl_account = acc_setting.ar_rent_default_asset_gl_account
        elif invoice_header_dict['internal_type'] == 'GOV':
            raise IntegrityError('Internal type not supported')

        current_transaction = Transaction(
                        transaction_date=timezone.now(),
                        transaction_type=transaction_type,
                        linked_transaction=None,
                        created_by=invoice_header_dict['by_user'],
                        modified_by=invoice_header_dict['by_user'])
        current_transaction.save()

        new_invoice_header = Invoice(
                        date=invoice_header_dict['date'],
                        internal_type=invoice_header_dict['internal_type'],
                        asset_gl_account=asset_gl_account,
                        transaction=current_transaction,
                        user=invoice_header_dict.get('user'),
                        client=invoice_header_dict.get('client'),
                        amount=0,
                        balance=0,
                        created_by=invoice_header_dict['by_user'],
                        modified_by=invoice_header_dict['by_user'],
                        )
        new_invoice_header.save()

        return new_invoice_header

    def update(self, invoice, invoice_dict):
        # DRAFT --> NOT_PAID
        if invoice.status == 'DRAFT' and\
                invoice_dict['status'] == 'NOT_PAID':

            return self.update_draft_to_not_paid(
                invoice=invoice,
                invoice_dict=invoice_dict)
        # DRAFT --> DRAFT
        elif invoice.status == 'DRAFT' and \
                invoice_dict['status'] == 'DRAFT':

            return self.update_draft_to_draft(
                invoice=invoice,
                invoice_dict=invoice_dict)

        # NOT_PAID --> NOT_PAID
        elif invoice.status == 'NOT_PAID' and\
                invoice_dict['status'] == 'NOT_PAID':

            return self.update_not_paid_to_not_paid(
                invoice=invoice,
                invoice_dict=invoice_dict)

        # NOT_PAID --> CANCELED
        elif invoice.status == 'NOT_PAID' and\
                invoice_dict['status'] == 'CANCELED':
            raise IntegrityError('delete invoice instead of chaning status')
        # DRAFT --> INVALID STATUS
        elif (invoice.status == 'DRAFT' and
                invoice_dict['status'] == 'NOT_PAID') or\
            (invoice.status == 'DRAFT' and
                invoice_dict['status'] == 'PAR_PAY') or\
            (invoice.status == 'DRAFT' and
                invoice_dict['status'] == 'CANCELED'):
            raise IntegrityError('invalid status submitted')
        # CANCELED UPDATE
        elif invoice.status == 'CANCELED':
            raise IntegrityError('cannot modify a canceled invoice')
        # INVOICE_CONTAINING PAYMENTS --> *
        elif invoice.status == 'PAR_PAY' or\
                invoice.status == 'PAID':
            raise IntegrityError('cannot modify an invoice containing payments')
        # ALL OTHER
        else:
            raise IntegrityError('invalid status')

    def update_draft_to_not_paid(self, invoice, invoice_dict):
        invoice.status = invoice_dict['status']
        invoice.date = invoice_dict['date']
        invoice.modified_by = invoice_dict['by_user']

        if invoice.internal_type in ('INTERNAL', 'RENT'):
            invoice.user = invoice_dict['user']
            invoice.user.balance.amount += invoice.balance
            invoice.user.balance.save()
        elif invoice.internal_type == 'EXTERNAL':
            invoice.client = invoice_dict['client']
            invoice.client.balance.amount += invoice.balance
            invoice.client.balance.save()

        invoice.save()
        list_of_items = InvoiceItem.objects.filter(
            invoice=invoice,
            is_canceled=False).exclude(line_total__lte=0)
        # change to create fresh new transaction invoice.transaction not fresh
        for item in list_of_items:
            InvoiceItem.objects.create_item_asset(
                item=item,
                transaction=invoice.transaction)
        return invoice

    def update_draft_to_draft(self, invoice, invoice_dict):
        invoice.date = invoice_dict['date']
        invoice.modified_by = invoice_dict['by_user']

        if invoice.internal_type in ('INTERNAL', 'RENT'):
            invoice.user = invoice_dict['user']
        elif invoice.internal_type == 'EXTERNAL':
            invoice.client = invoice_dict['client']
        invoice.save()
        return invoice

    def update_not_paid_to_not_paid(self, invoice, invoice_dict):
        if invoice.date.date() == invoice_dict['date'].date():
            if invoice.internal_type in ('RENT', 'INTERNAL'):
                if invoice.user != invoice_dict['user']:
                    invoice.user.balance.amount -= invoice.balance
                    invoice.user = invoice_dict['user']
                    invoice.user.balance.amount += invoice.balance
                    invoice.user.balance.save()
                else:
                    return invoice
            elif invoice.internal_type == 'EXTERNAL':
                if invoice.client != invoice_dict['client']:
                    invoice.client.balance.amount -= invoice.balance
                    invoice.client = invoice_dict['client']
                    invoice.client.balance.amount += invoice.balance
                    invoice.client.balance.save()
                else:
                    return invoice
            else:
                raise ValueError('invalid internal_type')
        else:
            invoice.date = invoice_dict['date']
            list_of_invoice_items = InvoiceItem.objects.filter(
                invoice=invoice,
                is_canceled=False)

            transaction = Transaction(
                transaction_date=invoice.date,
                transaction_type='MII_AR',
                linked_transaction=invoice.transaction,
                created_by=invoice_dict['user'],
                modified_by=invoice_dict['user'])
            transaction.save()

            for item in list_of_invoice_items:
                InvoiceItem.objects.reverse_item_asset(
                    item=item,
                    transaction=transaction)
                InvoiceItem.objects.create_item_asset(
                    item=item,
                    transaction=transaction)

            if invoice.internal_type in ('RENT', 'INTERNAL'):
                if invoice.user != invoice_dict['user']:
                    invoice.user.balance.amount -= invoice.balance
                    invoice.user = invoice_dict['user']
                    invoice.user.balance.amount += invoice.balance
                    invoice.user.balance.save()
            elif invoice.internal_type == 'EXTERNAL':
                if invoice.client != invoice_dict['client']:
                    invoice.client.balance.amount -= invoice.balance
                    invoice.client = invoice_dict['client']
                    invoice.client.balance.amount += invoice.balance
                    invoice.client.balance.save()
            else:
                raise ValueError('invalid internal_type')
        invoice.save()
        return invoice

    def delete(self, invoice, user):
        if invoice.status == 'DRAFT':
            InvoiceItem.objects.filter(invoice=invoice).delete()
            invoice.delete()
            return True

        elif invoice.status == 'NOT_PAID':
            # reverse gl accounts for invoice
            list_of_invoice_items = InvoiceItem.objects.filter(
                invoice=invoice,
                is_canceled=False)

            transaction = Transaction(
                transaction_date=invoice.date,
                transaction_type='CEI_AR',
                linked_transaction=invoice.transaction,
                created_by=user,
                modified_by=user)
            transaction.save()

            for item in list_of_invoice_items:
                InvoiceItem.objects.reverse_item_asset(
                    item=item,
                    transaction=transaction)
            invoice.status = 'CANCELED'
            invoice.modified_by = user
            invoice.save()
            return True

        elif invoice.status in ('PAY_PAY', 'NOT_PAID'):
            return True

        elif invoice.status == 'CANCELED':
            # already deleted
            return True

"""
INVOICE ITEM MANAGER CLASS
"""


class InvoiceItemManager(models.Manager):

    def create(
            self,
            item_dict,
            invoice=None,
            update_inv=False,
            transaction=None):

        if invoice is None:
            invoice = item_dict['invoice']

        if invoice.status == 'PAR_PAY' or\
                invoice.status == 'PAID':
            raise IntegrityError('cannot add items when invoice has payments')

        if item_dict['product'].overridable is True and\
                item_dict.get('price') is not None:
            price = item_dict.get('price')
        else:
            price = item_dict['product'].active_price

        item = InvoiceItem(
            invoice=invoice,
            is_canceled=False,
            name=self.get_item_name(item_dict['product']),
            product=item_dict['product'],
            quantity=item_dict['quantity'],
            price=price,
            line_total=item_dict['quantity']*price,
            gl_account=item_dict['product'].product_gl_account,
            created_by=item_dict['by_user'],
            modified_by=item_dict['by_user'],)
        item.save()

        if update_inv is True:
            invoice.balance += item.line_total
            invoice.amount += item.line_total
            invoice.modified_by = item_dict['by_user']
            invoice.save()

        if transaction is None and item.invoice.status == 'NOT_PAID':
            transaction = Transaction(
                transaction_date=invoice.date,
                transaction_type='MII_AR',
                linked_transaction=invoice.transaction,
                created_by=item_dict['user'],
                modified_by=item_dict['user']
                )
            transaction.save()

        if item.invoice.status == 'NOT_PAID':
            self.create_item_asset(
                        item=item,
                        transaction=transaction)

        return item

    def update(self, item, modified_item, transaction=None):

        if item.invoice.status == 'DRAFT':
            self.update_item(item, modified_item)
            self.update_invoice()
        elif item.invoice.status == 'NOT_PAID':
            if self.accounting_redistribution_required(
                        item=item,
                        modified_item=modified_item) is True:
                if transaction is None:
                    transaction = Transaction(
                        transaction_date=timezone.now(),
                        transaction_type='MII_AR',
                        linked_transaction=item.invoice.transaction,
                        created_by=modified_item['by_user'],
                        modified_by=modified_item['by_user'])
                self.reverse_item_asset(item, transaction)
                self.update_item(item, modified_item)
                self.create_item_asset(item, modified_item)
                return item
            else:
                return self.update_item(item, modified_item)

        elif item.invoice.status == 'PAID' or\
                item.invoice.status == 'PAR_PAY':
            raise IntegrityError('invoice paid, delete payments first')

    def delete(self, item, user, transaction=None):

        if item.invoice.status in ('PAID', 'PAR_PAY'):
            raise IntegrityError('delete payments first')

        elif item.invoice.status == 'DRAFT':
            item.invoice.balance -= item.line_total
            item.invoice.amount -= item.line_total
            item.invoice.modified_by = user
            item.invoice.save()

            item.delete()

            return True

        elif item.invoice.status == 'NOT_PAID':

            if transaction is None:
                transaction = Transaction(
                    transaction_date=timezone.now(),
                    transaction_type='MII_AR',
                    linked_transaction=item.invoice.transaction,
                    created_by=user,
                    modified_by=user)

            self.reverse_item_asset(item, transaction)
            item.is_canceled = True
            if item.invoice.internal_type in ('RENT', 'INTERNAL'):
                item.invoice.user.balance.amount -= item.line_total
            elif item.invoice.internal_type == 'EXTERNAL':
                item.invoice.client.balance.amount -= item.line_total
            else:
                raise ValueError('internal_type is not supported')

            item.invoice.user.balance.save()

            item.invoice.modified_by = user
            item.invoice.amount -= item.line_total
            item.invoice.balance -= item.line_total
            item.invoice.modified_by = user

            item.save()
            item.invoice.save()
            return True

    def update_item(self, item, modified_item):

        item.is_canceled = modified_item['is_canceled']
        item.name = self.get_item_name(modified_item['product'])
        item.product = modified_item['product']
        item.quantity = modified_item['quantity']
        item.price = modified_item['price']
        item.line_total = modified_item.price['quantity'] *\
            modified_item['price']
        item.modified_by = modified_item['user']

        item.save()

    def get_item_name(self, product):
        ar_settings = Setting.objects.get()
        locale = ar_settings.inv_default_locale
        name_list = product.name.all()
        for name in name_list:
            if name.locale == locale.locale:
                return name.text
        return 'NO_NAME <CHECK LANG>'

    def accounting_redistribution_required(self, item, modified_item):
        if item.is_canceled is True:
            raise ValueError('item is canceled')

        if (item.is_canceled is False and
                modified_item['is_canceled'] is True) or\
                (item.line_total != modified_item['price'] *
                    modified_item['quantity']):
            return True
        else:
            return False

    def create_item_asset(self, item, transaction):

        ar_settings = AccSetting.objects.get()

        if item.invoice.internal_type == 'INTERNAL':
            ar_settings = AccSetting.objects.get()
            second_gl_account = \
                ar_settings.member_default_liability_gl_account

        elif item.invoice.internal_type == 'RENT':
            ar_settings = AccSetting.objects.get()
            second_gl_account = \
                ar_settings.ar_rent_default_asset_gl_account

        elif item.invoice.internal_type == 'EXTERNAL':
            second_gl_account = \
                item.invoice.client.gl_account_ar

        else:
            raise Exception('internal_type not implemented yet')

        GLEntry.objects.doubleEntry(
            transaction=transaction,
            date=item.invoice.date,
            first_gl_account=item.gl_account,
            first_amount=item.line_total,
            second_gl_account=second_gl_account,
            second_amount=item.line_total)

    def reverse_item_asset(self, item, transaction):

        if item.invoice.internal_type == 'INTERNAL':
            settings = AccSetting.objects.get()
            second_gl_account = \
                settings.member_default_liability_gl_account
        elif item.invoice.internal_type == 'RENT':
            settings = AccSetting.objects.get()
            second_gl_account = settings.ar_rent_default_asset_gl_account
        elif item.invoice.internal_type == 'EXTERNAL':
            second_gl_account = item.invoice.internal_type
        else:
            raise Exception('internal_type not implemented yet')

        GLEntry.objects.doubleEntry(
            transaction=transaction,
            date=item.invoice.date,
            first_gl_account=item.gl_account,
            first_amount=-item.line_total,
            second_gl_account=second_gl_account,
            second_amount=-item.line_total)




"""
PAYMENT ITEM MANAGER CLASS
"""


class PaymentManager(models.Manager):

    def create(self, payment_dict):
        if payment_dict['amount'] < 0:
            raise IntegrityError('payment must be greater than 0')

        payment = self.create_payment(payment_dict)

        # exact payment or smaller
        if payment.invoice.balance <= payment.amount:
            GLEntry.objects.doubleEntry(
                transaction=payment.transaction,
                date=payment.date,
                first_gl_account=payment.invoice.asset_gl_account,
                first_amount=-payment.amount,
                second_gl_account=payment.bank_account.gl_account,
                second_amount=payment.amount)

            payment.invoice.balance -= payment.amount
            payment.invoice.modified_by = payment_dict['by_user']
            payment.invoice.save()
        # over payment
        else:
            GLEntry.objects.doubleEntry(
                transaction=payment.transaction,
                date=payment.date,
                first_gl_account=payment.invoice.asset_gl_account,
                first_amount=-payment.invoice.balance,
                second_gl_account=payment.bank_account.gl_account,
                second_amount=payment.invoice.balance)

            if payment.invoice.internal_type == 'RENT' or\
                    payment.invoice.internal_type == 'INTERNAL':
                ar_settings = AccSetting.objects.get()
                first_gl_account = ar_settings.\
                    ar_member_default_pay_over_gl_account
                # enter information about the money

            elif payment.invoice.internal_type == 'EXTERNAL':
                first_gl_account = payment\
                    .invoice\
                    .client.ar_client_default_pay_over_gl_account

            GLEntry.objects.doubleEntry(
                transaction=payment.transaction,
                date=payment.date,
                first_gl_account=first_gl_account,
                first_amount=-payment.amount+payment.invoice.balance,
                second_gl_account=payment.bank_account.gl_account,
                second_amount=payment.amount-payment.invoice.balance)

            if payment.invoice.balance > 0:
                payment.invoice.balance = 0
                payment.invoice.modified_by = payment_dict['by_user']
                payment.invoice.save()

    def create_payment(self, payment_dict):
        transaction = Transaction(
            transaction_date=payment_dict['invoice'].date,
            transaction_type='PAY_AR',
            linked_transaction=payment_dict['invoice'].transaction,
            created_by=payment_dict['by_user'],
            modified_by=payment_dict['by_user']
            )
        transaction.save()

        payment = Payment(
            amount=payment_dict['amount'],
            date=payment_dict['date'],
            invoice=payment_dict['invoice'],
            bank_account=payment_dict['bank_account'],
            transaction=transaction,
            created_by=payment_dict['by_user'],
            modified_by=payment_dict['by_user'])
        payment.save()

        return payment

    def delete(self, payment, user):
        self.reverse_payment(payment=payment, user=user)
        payment.is_canceled = True
        payment.save()
        return True

    def reverse_payment(self, payment, user, transaction=None, amount=None):
        total_payment = Payment.objects.filter(
            invoice=payment.invoice,
            is_canceled=False).aggregate(Sum)
        if transaction is None:
            transaction = Transaction(
                transaction_date=timezone.now(),
                transaction_type='',
                linked_transaction=payment.invoice.transaction,
                created_by=user,
                modified_by=user)
            transaction.save()
        # no amount in client/user credit
        if total_payment <= payment.invoice.amount:
            payment.invoice.balance -= payment.amount

            GLEntry.objects.doubleEntry(
                transaction=transaction,
                date=timezone.now(),
                first_gl_account=payment.bank_account.gl_account,
                first_amount=-payment.amount,
                second_gl_account=payment.invoice.asset_gl_account,
                second_amount=-payment.amount)
        # amount to credit
        else:
            total_credit_amount = total_payment - payment.invoice.amount

            if payment.invoice.internal_type in ('INTERNAL', 'RENT'):
                # payment.amount is smaller or eq. than credit
                if payment.amount <= total_credit_amount:

                    GLEntry.objects.doubleEntry(
                        transaction=transaction,
                        date=timezone.now(),
                        first_gl_account=AccSetting.
                        ar_member_default_pay_over_gl_account,
                        first_amount=-payment.amount,
                        second_gl_account=payment.invoice.asset_gl_account,
                        second_amount=payment.amount)

                # payment.amount is greated than credit
                else:
                    # reverse_payment_user_over_payment
                    self.reverse_payment_user_over_payment(
                        payment, transaction, total_credit_amount)

            elif payment.invoice.internal_type == 'EXTERNAL':
                if payment.amount <= total_credit_amount:

                    GLEntry.objects.doubleEntry(
                        transaction=transaction,
                        date=timezone.now(),
                        first_gl_account=AccSetting.
                        ar_client_default_pay_over_gl_account,
                        first_amount=-payment.amount,
                        second_gl_account=payment.invoice.asset_gl_account,
                        second_amount=payment.amount)

                # payment.amount is greated than credit
                else:
                    self.reverse_payment_client_over_payment(
                        payment, transaction, total_credit_amount)

    def reverse_payment_user_over_payment(self, payment, transaction, tca):
        reminder = payment.amount - tca
        GLEntry.objects.doubleEntry(
            transaction=transaction,
            date=timezone.now(),
            first_gl_account=AccSetting.
            ar_member_default_pay_over_gl_account,
            first_amount=-tca,
            second_gl_account=payment.invoice.asset_gl_account,
            second_amount=tca)

        GLEntry.objects.doubleEntry(
            transaction=transaction,
            date=timezone.now(),
            first_gl_account=payment.bank_account.gl_account,
            first_amount=-reminder,
            second_gl_account=payment.invoice.asset_gl_account,
            second_amount=reminder)

    def reverse_payment_client_over_payment(self, payment, transaction, tca):
        reminder = payment.amount - tca

        GLEntry.objects.doubleEntry(
            transaction=transaction,
            date=timezone.now(),
            first_gl_account=AccSetting.
            ar_client_default_pay_over_gl_account,
            first_amount=-tca,
            second_gl_account=payment.invoice.asset_gl_account,
            second_amount=tca)

        GLEntry.objects.doubleEntry(
            transaction=transaction,
            date=timezone.now(),
            first_gl_account=payment.bank_account.gl_account,
            first_amount=-reminder,
            second_gl_account=payment.invoice.asset_gl_account,
            second_amount=reminder)


class Client(models.Model):
    AB = 'AB'
    BC = 'BC'
    MB = 'MB'
    NB = 'NB'
    NL = 'NL'
    NT = 'NT'
    NS = 'NS'
    NU = 'NU'
    ON = 'ON'
    PE = 'PE'
    QC = 'QC'
    SK = 'SK'
    YT = 'YT'

    PROVINCES = (
        (AB, 'AB'),
        (BC, 'BC'),
        (MB, 'MB'),
        (NB, 'NB'),
        (NL, 'NL'),
        (NT, 'NT'),
        (NS, 'NS'),
        (NU, 'NU'),
        (ON, 'ON'),
        (PE, 'PE'),
        (QC, 'QC'),
        (SK, 'SK'),
        (YT, 'YT'),
        )
    name = models.CharField(max_length=256)
    primary_phone = models.CharField(max_length=16, null=True)
    address_line_1 = models.CharField(max_length=128, null=True)
    address_line_2 = models.CharField(max_length=128, null=True)
    city = models.CharField(max_length=64)
    province = models.CharField(
                choices=PROVINCES,
                max_length=2)

    client_gl_account = models.ForeignKey(
                        GLAccount,
                        related_name='%(class)s_client_gl_account')
    client_gl_account_pay_over = models.ForeignKey(
                        GLAccount,
                        related_name='%(class)s_client_gl_account_pay_over')
    balance = models.ForeignKey(Balance, related_name='%(class)s_balance_ar')
    created_by = models.ForeignKey(
                    AUTH_USER_MODEL,
                    related_name='%(class)s_created_by_ar')
    created_date = models.DateTimeField(editable=False)
    modified_by = models.ForeignKey(
                    AUTH_USER_MODEL,
                    related_name='%(class)s_modified_by_ar')
    modified_date = models.DateTimeField()

    objects = ClientManager()

    def save(self, *args, **kwargs):
        if not self.id:
            self.created_date = timezone.now()
        self.modified_date = timezone.now()
        return super(Client, self).save(*args, **kwargs)


class ProductCategory(models.Model):
    RENT = 'RENT'
    INTERNAL = 'INTERNAL'
    EXTERNAL = 'EXTERNAL'
    GOV = 'GOV'
    INTERNAL_TYPES = (
        (RENT, 'RENT'),
        (INTERNAL, 'INTERNAL'),
        (EXTERNAL, 'EXTERNAL'),
        (GOV, 'GOV'),
    )

    internal_type = models.CharField(
                    choices=INTERNAL_TYPES,
                    max_length=16)
    is_active = models.BooleanField(default=True)

    # translated
    name = models.ManyToManyField(LocalizedText, '%(class)s_name')

    objects = ProductCategoryManager()
    created_by = models.ForeignKey(
                    AUTH_USER_MODEL,
                    related_name='%(class)s_created_by_ar')
    created_date = models.DateTimeField(editable=False)
    modified_by = models.ForeignKey(
                    AUTH_USER_MODEL,
                    related_name='%(class)s_modified_by_ar')
    modified_date = models.DateTimeField()

    def save(self, *args, **kwargs):
        if not self.id:
            self.created_date = timezone.now()
        self.modified_date = timezone.now()
        return super(ProductCategory, self).save(*args, **kwargs)


class Product(models.Model):
    category = models.ForeignKey(
                ProductCategory,
                related_name='%(class)s_category')
    overridable = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_taxable = models.BooleanField(default=True)

    # translated
    name = models.ManyToManyField(LocalizedText, '%(class)s_name')
    # translated
    description = models.ManyToManyField(
        LocalizedText,
        '%(class)s_description')

    product_gl_account = models.ForeignKey(
                    GLAccount,
                    related_name='%(class)s_product_gl_account')
    active_price = models.DecimalField(max_digits=8, decimal_places=2)

    objects = ProductManager()

    created_by = models.ForeignKey(
                    AUTH_USER_MODEL,
                    related_name='%(class)s_created_by_ar')
    created_date = models.DateTimeField(editable=False)
    modified_by = models.ForeignKey(
                    AUTH_USER_MODEL,
                    related_name='%(class)s_modified_by_ar')
    modified_date = models.DateTimeField()

    def save(self, *args, **kwargs):
        if not self.id:
            self.created_date = timezone.now()
        self.modified_date = timezone.now()
        return super(Product, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.id) + ' ' + self.name


class Invoice(models.Model):
    RENT = 'RENT'
    INTERNAL = 'INTERNAL'
    EXTERNAL = 'EXTERNAL'
    GOV = 'GOV'

    INTERNAL_TYPES = (
        (RENT, 'RENT'),
        (INTERNAL, 'INTERNAL'),
        (EXTERNAL, 'EXTERNAL'),
        (GOV, 'GOV')
    )

    DRAFT = 'DRAFT'
    NOT_PAID = 'NOT_PAID'
    PAID = 'PAID'
    PAR_PAY = 'PAR_PAY'
    CANCELED = 'CANCELED'

    STATUS = (
        (DRAFT, 'DRAFT'),
        (NOT_PAID, 'NOT_PAID'),
        (PAID, 'PAID'),
        (PAR_PAY, 'PARTIAL PAYMENT'),
        (CANCELED, 'CANCELED')
    )

    date = models.DateTimeField()

    internal_invoice_no = models.IntegerField(null=True)

    rent_no = models.IntegerField(null=True)

    external_invoice_no = models.IntegerField(null=True)

    status = models.CharField(max_length=16, choices=STATUS, default='DRAFT')

    internal_type = models.CharField(
                    max_length=16,
                    choices=INTERNAL_TYPES,
                    default='RENT')

    asset_gl_account = models.ForeignKey(
                    GLAccount,
                    related_name='%(class)s_asset_gl_account')

    transaction = models.ForeignKey(
                    Transaction,
                    related_name='%(class)s_transaction_ar',
                    null=True)
    user = models.ForeignKey(
                    AUTH_USER_MODEL,
                    related_name='%(class)s_user_ar',
                    null=True)
    client = models.ForeignKey(
                    Client,
                    related_name='%(class)s_client_ar',
                    null=True)

    amount = models.DecimalField(
                    max_digits=16,
                    decimal_places=2,
                    default=0)
    balance = models.DecimalField(
                    max_digits=16,
                    decimal_places=2,
                    default=0)
    notes = models. CharField(max_length=2048, null=True)
    objects = InvoiceManager()

    created_by = models.ForeignKey(
                    AUTH_USER_MODEL,
                    related_name='%(class)s_created_by_ar')
    created_date = models.DateTimeField(editable=False)
    modified_by = models.ForeignKey(
                    AUTH_USER_MODEL,
                    related_name='%(class)s_modified_by_ar')
    modified_date = models.DateTimeField()

    def save(self, *args, **kwargs):
        if not self.id:
            self.created_date = timezone.now()
        self.modified_date = timezone.now()
        return super(Invoice, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.id) + ' ' + str(self.amount) + ' ' + str(self.balance)


class InvoiceItem(models.Model):

    invoice = models.ForeignKey(
                    Invoice,
                    related_name='%(class)s_invoice_ar')

    name = models.CharField(max_length=1024)
    gl_account = models.ForeignKey(
                    GLAccount,
                    related_name='%(class)s_gl_account_ar', null=True)
    product = models.ForeignKey(
                    'Product',
                    related_name='%(class)s_gl_account_ar', null=True)
    quantity = models.IntegerField()

    price = models.DecimalField(
                    max_digits=8,
                    decimal_places=2)

    line_total = models.DecimalField(
                    max_digits=8,
                    decimal_places=2)
    is_canceled = models.BooleanField(default=True)

    objects = InvoiceItemManager()

    # audit
    created_by = models.ForeignKey(
                    AUTH_USER_MODEL,
                    related_name='%(class)s_created_by_ar')
    created_date = models.DateTimeField(editable=False)
    modified_by = models.ForeignKey(
                    AUTH_USER_MODEL,
                    related_name='%(class)s_modified_by_ar')
    modified_date = models.DateTimeField()

    def save(self, *args, **kwargs):
        if not self.id:
            self.created_date = timezone.now()
        self.modified_date = timezone.now()

        super(InvoiceItem, self).save(
                            *args,
                            **kwargs)

    def __str__(self):
        return str(self.id) + ' ' + self.name


class Payment(models.Model):

    NONE = 'NONE'
    SCHL = 'SCHL'
    SHQ = 'SHQ'

    SUBSIDIES = (
        (NONE, 'NONE'),
        (NONE, 'SCHL'),
        (NONE, 'SHQ')
        )

    subsidy = models.CharField(max_length=8, null=True, choices=SUBSIDIES)

    payment_no = models.IntegerField(null=True)

    amount = models.DecimalField(max_digits=16, decimal_places=2)

    date = models.DateTimeField()

    invoice = models.ForeignKey('Invoice', related_name='%(class)s_invoice_ar')

    bank_account = models.ForeignKey(
        BankAccount,
        related_name='%(class)s_bank',
        null=True)

    transaction = models.ForeignKey(
                Transaction,
                related_name='%(class)s_transaction_ar')
    notes = models.CharField(max_length=2048, null=True)

    objects = PaymentManager()

    created_by = models.ForeignKey(
                AUTH_USER_MODEL,
                related_name='%(class)s_created_by_ar')
    created_date = models.DateTimeField(editable=False)
    modified_by = models.ForeignKey(
                AUTH_USER_MODEL,
                related_name='%(class)s_modified_by_ar')
    modified_date = models.DateTimeField()

    def save(self, *args, **kwargs):
        if not self.id:
            self.created_date = timezone.now()
        self.modified_date = timezone.now()

        super(Payment, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.id) + ' ' + self.name


class Setting(SingletonModel):
    NONE = 'NONE'
    SCHL = 'SCHL'
    SHQ = 'SHQ'

    SUBSIDIES = (
        (NONE, 'NONE'),
        (NONE, 'SCHL'),
        (NONE, 'SHQ')
        )
    inv_default_subsidy = models.CharField(
        max_length=8,
        choices=SUBSIDIES,
        default='NONE'
        )
    inv_default_locale = models.ForeignKey(
                Locale,
                related_name='%(class)s_inv_default_locale',
                null=True
                )
