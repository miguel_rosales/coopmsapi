"""coopmsapi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Import the include() function: from django.conf.urls import url, include
    3. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from rest_framework_swagger.views import get_swagger_view
schema_view = get_swagger_view(title='COOPMS API')

urlpatterns = [
    url(r'^auth/', include('oauth2_provider.urls',
        namespace='oauth2_provider')),
    url(r'^admin/', admin.site.urls),
    url(r'^docs/', schema_view),
    url(r'^accounting/', view=include('coopmsapi.accounting.urls',
        namespace='accounting')),
    url(r'^', view=include('coopmsapi.budget.urls',
        namespace='budget')),
    url(r'^coop/', view=include('coopmsapi.coop.urls',
        namespace='coop')),
    url(r'^payables/', view=include('coopmsapi.payables.urls',
        namespace='payables')),
    url(r'^users_module/', view=include('coopmsapi.users.urls',
        namespace='users')),
    url(r'^receivables/', view=include('coopmsapi.receivables.urls',
        namespace='receivables')),
    url(r'', view=include('coopmsapi.clients.urls',
        namespace='clients')),
    url(r'^reports', view=include('coopmsapi.reports.urls',
        namespace='reports')),
    url(r'^form_data/', view=include('coopmsapi.form_data.urls',
        namespace='form_data')),
    #url(r'^subsidies/',
        #view=include('coopmsapi.subsidies.urls', namespace='subsidies')),
    #url(r'^taxes/', view=include('coopmsapi.taxes.urls', namespace='taxes')),
]
