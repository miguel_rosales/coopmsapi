from coopmsapi.accounting.models import GLAccount
from coopmsapi.payables.models import Supplier, SupplierCategory
from coopmsapi.accounting.serializers import GLAccountDisplayMiniSerializer
from coopmsapi.payables.serializers import (
    SupplierDisplayMiniSerializer,
    SupplierCategoryDisplayShortSerializer)
from rest_framework.views import APIView
from rest_framework import mixins
from rest_framework import generics
from rest_framework.response import Response
from rest_framework import status

class CreatePayableInvoiceFormData(APIView):
    """
    form data
    """
    def get(self, request, format=None):
        """
        Return a list of all users.
        """
        gl_accounts = GLAccount.objects.filter(is_active=True, category__id=5)
        gl_accounts_serializer = GLAccountDisplayMiniSerializer(
            gl_accounts,
            many=True,
            context={'request': request })

        suppliers = Supplier.objects.filter(is_active=True)
        suppliers_serializer = SupplierDisplayMiniSerializer(
            suppliers,
            many=True,
            context={'request': request })

        return Response(
            data={
                'gl_accounts': gl_accounts_serializer.data,
                'suppliers': suppliers_serializer.data},
            status=status.HTTP_200_OK)


class SupplierListView(mixins.ListModelMixin, generics.GenericAPIView):
    queryset = Supplier.objects.all()
    serializer_class = SupplierDisplayMiniSerializer

class SupplierFormData(APIView):
    def get(self, request, format=None):
        supplier_gl_account = GLAccount.objects.filter(is_active=True, category__id=5)
        gl_accounts_serializer = GLAccountDisplayMiniSerializer(
            supplier_gl_account,
            many=True,
            context={'request': request })

        supplier_gl_account_pay_over = GLAccount.objects.filter(is_active=True, category__id=5)
        supplier_gl_account_pay_over_serializer = GLAccountDisplayMiniSerializer(
            supplier_gl_account_pay_over,
            many=True,
            context={'request': request })

        supplier_categories = SupplierCategory.objects.filter(is_active=True)
        supplier_categories_serializer = SupplierCategoryDisplayShortSerializer(
            supplier_categories,
            many=True,
            context={'request': request})

        return Response(
            data={
                'supplier_gl_account': gl_accounts_serializer.data,
                'supplier_gl_account_pay_over': supplier_gl_account_pay_over_serializer.data,
                'categories': supplier_categories_serializer.data
                },
            status=status.HTTP_200_OK)
