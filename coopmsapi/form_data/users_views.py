from django.contrib.auth.models import Permission, Group
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

class CreateUserFormData(APIView):
    """
    form data
    """
    def get(self, request, format=None):
        """
        Return a list of all users.
        """
        group = Group.objects.all().values()
        perm = Permission.objects.all().values('id', 'codename')

        return Response(
            data={
                'permissions': perm,
                'groups': group},
            status=status.HTTP_200_OK)

