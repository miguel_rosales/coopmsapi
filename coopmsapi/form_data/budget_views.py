from coopmsapi.accounting.models import GLAccount
from coopmsapi.accounting.serializers import GLAccountDisplayMiniSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

class CreateBudgetFormData(APIView):
    """
    form data
    """
    def get(self, request, format=None):
        """
        Return a list of all users.
        """
        gl_accounts = GLAccount.objects.get_active_budget_accounts()
        serializer = GLAccountDisplayMiniSerializer(
            gl_accounts,
            many=True,
            context={'request': request})

        return Response(
            data={
                'gl_accounts': serializer.data},
            status=status.HTTP_200_OK)
