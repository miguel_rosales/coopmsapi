from django.conf.urls import url
from .users_views import CreateUserFormData
from .accounting_views import CreateGlAccountFormData, TestView
from .payables_views import (
    CreatePayableInvoiceFormData,
    SupplierFormData,
    SupplierListView)
from .budget_views import CreateBudgetFormData

urlpatterns = [
    url(r'^budget_form_data/', CreateBudgetFormData.as_view()),
    url(r'^user_creation/', CreateUserFormData.as_view()),
    url(r'^create_gl_account/', CreateGlAccountFormData.as_view()),
    url(r'^test/', TestView.as_view()),
    url(r'^create_payable_invoice/', CreatePayableInvoiceFormData.as_view()),
    url(r'^supplier/', SupplierFormData.as_view()),
    url(r'^supplier_list/', SupplierListView.as_view()),
]
