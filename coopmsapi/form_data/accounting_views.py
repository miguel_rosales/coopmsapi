from coopmsapi.accounting.models import GLAccount
from coopmsapi.accounting.serializers import GLAccountDisplayMiniSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import AllowAny

class CreateGlAccountFormData(APIView):
    """
    form data
    """
    def get(self, request, format=None):
        """
        Return a list of all users.
        """
        gl_accounts = GLAccount.objects.filter(is_active=True)
        serializer = GLAccountDisplayMiniSerializer(
            gl_accounts,
            many=True,
            context={'request': request})

        return Response(
            data={
                'gl_accounts': serializer.data},
            status=status.HTTP_200_OK)

class TestView(APIView):

    permission_classes = (AllowAny,)

    def get(self, request, format=None):
        json_dict = {
                    "soir": {
                        "active": 1,
                        "end_date": "2017-02-23T23:29:59.997",
                        "convoyeur": 1,
                        "Vitesse": 15,
                        "nbr_emp": 21,
                        "Cedule": 3,
                        "start_date": "2017-02-23T15:00:00",
                        "pi_total": 44,
                        "Vitesse_moy": 14,
                        "date": "2017-02-23T15:07:00",
                        "Vitesse_max": 15,
                        "Vitesse_min": 14,
                        "th_max": 25,
                        "th_min": 0,
                        "ced_now": 3,
                        "gun": 1,
                        "Programme": 163},
                    "jour": {
                        "active": 0,
                        "end_date":
                        "2017-02-23T14:59:59.997",
                        "convoyeur": 0,
                        "Vitesse": 0,
                        "nbr_emp": 26,
                        "Cedule": 2,
                        "start_date":
                        "2017-02-23T06:30:00",
                        "pi_total": 6687,
                        "Vitesse_moy": 17,
                        "date": "2017-02-23T14:59:00",
                        "Vitesse_max": 18,
                        "Vitesse_min": 15,
                        "th_max": 25,
                        "th_min": 0,
                        "ced_now": 3,
                        "gun": 0,
                        "Programme": 163}
                    }
        return Response(
            data=json_dict,
            status=status.HTTP_200_OK)
