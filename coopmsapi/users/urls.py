from django.conf.urls import url, include
from rest_framework_nested import routers
from .views import UserViewSet

# user router
user_router = routers.SimpleRouter()
user_router.register(
            r'users',
            UserViewSet,
            base_name='users')

# reset password

urlpatterns = [
    url(r'', include(user_router.urls)),
]
