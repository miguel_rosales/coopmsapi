# -*- coding: utf-8 -*-
from django.http import Http404
from django.conf import settings
from rest_framework.response import Response
from django.contrib.auth import get_user_model
from rest_framework import status, permissions, viewsets
# filters
from rest_framework import filters
# transaction
# from django.db import transaction, IntegrityError
# serializers
# filters
# models
# from .models import CoopUser
# update client_model
from .serializers import (
                UserInsertSerializer,
                UserModificationSerializer,
                UserDisplaySerializer)
from .filters import UserFilter


class UserViewSet(viewsets.ModelViewSet):
    serializer_class = UserDisplaySerializer
    #filter_backends = (filters.DjangoFilterBackend,)
    queryset = get_user_model().objects.exclude(id=1)
    #filter_class = UserFilter PROBLEM HERE

    def create(self, request):
        request.META
        serializer = UserInsertSerializer(data=request.data)
        if serializer.is_valid():
            serializer.validated_data['by_user'] = request.user
            user = serializer.create(validated_data=serializer.validated_data)
            return Response(
                data=UserDisplaySerializer(user).data,
                status=status.HTTP_201_CREATED)
        else:
            return Response(
                data=serializer.errors,
                status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk):
        return Response(
                {'detail': 'not allowed'},
                status.HTTP_405_METHOD_NOT_ALLOWED)

    def update(self, request, pk):
        serializer = UserModificationSerializer(data=request.data)
        if serializer.is_valid():
            try:
                user = self.get_object()
                serializer.update(user=user)
                return Response(status=status.HTTP_204_NO_CONTENT)

            except Http404:
                pass
        else:
            return Response(
                    data=serializer.errors,
                    status=status.HTTP_400_BAD_REQUEST)

    def get_serializer_class(self):
        if self.action == 'list' or\
                self.action == 'retrieve':

            return UserDisplaySerializer
        else:
            return UserInsertSerializer
