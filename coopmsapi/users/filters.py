from rest_framework import filters
import django_filters
from .models import CoopUser


class UserFilter(filters.FilterSet):
    gte_member_since = django_filters.DateFilter(
                name='member_since',
                lookup_type='gte')
    lte_member_since = django_filters.DateFilter(
                name='member_since',
                lookup_type='lte')

    class Meta:
        model = CoopUser
        fields = [
            'email',
            'first_name',
            'last_name',
            'gte_member_since',
            'lte_member_since']
