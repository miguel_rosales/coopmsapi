from django.utils import timezone
from django.db import models
from coopmsapi.totalizers.models import Balance
from custom_user.models import AbstractEmailUser
from django.contrib.auth.models import BaseUserManager


class CoopUserManager(BaseUserManager):

    def create(self, user_dict):

        balance = Balance.objects.create(
            balance_type='MEMBER')

        user = CoopUser(
            email=user_dict['email'],
            first_name=user_dict['first_name'],
            last_name=user_dict['last_name'],
            is_superuser=user_dict.get('is_superuser', False),
            dob=user_dict.get('dob', timezone.now()),
            is_active=user_dict.get('is_active', True),
            member_since=user_dict.get('member_since', timezone.now()),
            phone_number=user_dict.get('phone_number'),
            subsidy=user_dict.get('subsidy', 'NONE'),
            language=user_dict.get('language', 'EN'),
            balance=balance,
            created_by=user_dict.get('by_user'),
            modified_by=user_dict.get('by_user'))

        user.save()

        password = user_dict.get('password')

        if password is None:
            print('PASSWORD IS NONE!!!!!')
            password = 'ksdfjhsndfksdul3nj1ksfkjlfdsklsjdfl' +\
                'sfd#ksdfk#4pou4ncofRRfiksdfkjlsdfl'

        user.set_password(password)
        user.save()

        return user

    def update(self, user, user_dict):
        user.email = user_dict.get('email', user.email)
        user.first_name = user_dict.get('first_name', user.first_name)
        user.last_name = user_dict.get('last_name', user.last_name)
        user.is_superuser = user_dict.get('is_superuser', user.is_superuser)
        user.is_active = user_dict.get('is_active', user.is_active)
        user.member_since = user_dict.get('member_since', user.member_since)
        user.phone_number = user_dict.get('phone_number', user.phone_number)
        user.subsidy = user_dict.get('subsidy', user.subsidy)
        user.language = user_dict.get('language', user.language)
        user.modified_by = user_dict.get('modified_by', user.modified_by)
        user.save()

        password = user_dict.get('password', None)
        if password is not None:
            user.set_password(password)
            user.save()

        return user


class CoopUser(AbstractEmailUser):

    NONE = 'NONE'
    SCHL = 'SCHL'
    SHQ = 'SHQ'

    subidy_choices = (
        (NONE, 'NONE'),
        (SCHL, 'SCHL'),
        (SHQ, 'SHQ'),
        )

    FR = 'FR'
    EN = 'EN'

    language_choices = (
            (FR, 'FRANÇAIS'),
            (EN, 'ENGLISH')
            )
    first_name = models.CharField(max_length=128)
    last_name = models.CharField(max_length=128)
    dob = models.DateTimeField(default=timezone.now)
    member_since = models.DateTimeField(default=timezone.now)
    phone_number = models.CharField(
                        max_length=16,
                        null=True)
    subsidy = models.CharField(
                        max_length=8,
                        choices=subidy_choices,
                        default=NONE)
    language = models.CharField(
                        max_length=8,
                        choices=language_choices,
                        default=FR)
    balance = models.OneToOneField(
        Balance,
        related_name='%(class)s_balance',
        null=True)
    objects = CoopUserManager()
    created_by = models.ForeignKey(
                        'CoopUser',
                        related_name='%(class)s_created_by',
                        null=True)
    created_date = models.DateTimeField(editable=False)
    modified_by = models.ForeignKey(
                        'CoopUser',
                        related_name='%(class)s_modified_by',
                        null=True)
    modified_date = models.DateTimeField(null=True)

    def __str__(self):
        return str(self.id) + ' ' + self.first_name + ' ' + str(self.last_name)

    def get_fullname(self):
        return self.last_name + ', ' + self.first_name

    def save(self, *args, **kwargs):
        if not self.id:
            if self.email != 'AnonymousUser' and \
                    self.balance is None:
                raise Exception('Null balance')
            self.created_date = timezone.now()

        self.modified_date = timezone.now()
        return super(CoopUser, self).save(*args, **kwargs)
