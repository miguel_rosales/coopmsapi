import psycopg2
from django.conf import settings
from coopmsapi.coop.models import Coop


def inactivate_user_public(user_email):

    current_coop = Coop.objects.get()

    conn = psycopg2.connect(
                    database=settings.DATABASES['default']['NAME'],
                    user=settings.DATABASES['default']['USER'],
                    password=settings.DATABASES['default']['PASSWORD'],
                    host=settings.DATABASES['default']['HOST'],
                    port=settings.DATABASES['default']['PORT'])
    cursor = conn.cursor()
    cursor.execute(
        "UPDATE public.clients_clientuserslist SET is_active=FALSE " +
        "WHERE email =(%s) and client_id=(%s);",
        (user_email, current_coop.tenant_id))
    conn.commit()
    cursor.close()

    return cursor.rowcount


def activate_user_public(user_email):

    current_coop = Coop.objects.get()

    conn = psycopg2.connect(database=settings.DATABASES['default']['NAME'],
                            user=settings.DATABASES['default']['USER'],
                            password=settings.DATABASES['default']['PASSWORD'],
                            host=settings.DATABASES['default']['HOST'],
                            port=settings.DATABASES['default']['PORT'])
    cursor = conn.cursor()
    cursor.execute(
        "UPDATE public.clients_clientuserslist SET " +
        "is_active=TRUE WHERE email =(%s) and client_id=(%s);",
        (user_email, current_coop.tenant_id))
    conn.commit()
    cursor.close()

    return cursor.rowcount


def create_user_public(user_email):

    current_coop = Coop.objects.get()

    conn = psycopg2.connect(database=settings.DATABASES['default']['NAME'],
                            user=settings.DATABASES['default']['USER'],
                            password=settings.DATABASES['default']['PASSWORD'],
                            host=settings.DATABASES['default']['HOST'],
                            port=settings.DATABASES['default']['PORT'])
    cursor = conn.cursor()
    cursor.execute(
        "INSERT into public.clients_clientuserslist(" +
        "is_active,email,client_id,password_reset_request) values(" +
        "TRUE, (%s), (%s), TRUE);",
        (user_email, current_coop.tenant_id))
    conn.commit()
    cursor.close()

    return cursor.rowcount
