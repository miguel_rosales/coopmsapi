from django.contrib import admin
from .models import CoopUser

admin.site.register(CoopUser)
