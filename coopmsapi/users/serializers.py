from rest_framework import serializers
from django.contrib.auth import get_user_model
from coopmsapi.totalizers.serializers import BalanceDisplayShortSerializer
from .public_schema import (
                    create_user_public,
                    activate_user_public,
                    inactivate_user_public)


class UserInsertSerializer(serializers.ModelSerializer):
    dob = serializers.DateTimeField(required=False)
    member_since = serializers.DateTimeField(required=False)
    subsidy = serializers.CharField(max_length=8, required=False)

    class Meta:
        model = get_user_model()

        fields = (
            'id',
            'email',
            'password',
            'is_superuser',
            'first_name',
            'last_name',
            'phone_number',
            'dob',
            'member_since',
            'subsidy',
            'date_joined',
            'groups',
            'user_permissions'
        )

    def create(self, validated_data):
        new_user = get_user_model().objects.create(user_dict=validated_data)
        create_user_public(self.validated_data['email'])
        return new_user

    def update(self, user, user_dict):
        pass


class UserDisplaySerializer(serializers.ModelSerializer):
    balance = BalanceDisplayShortSerializer()

    class Meta:
        model = get_user_model()
        fields = (
            'id',
            'email',
            'is_superuser',
            'is_active',
            'first_name',
            'last_name',
            'phone_number',
            'dob',
            'member_since',
            'subsidy',
            'date_joined',
            'balance',
            'groups',
            'user_permissions'
        )


class UserModificationSerializer(serializers.ModelSerializer):
    email = serializers.CharField(max_length=128, read_only=True)
    password = serializers.CharField(max_length=128, required=False)
    dob = serializers.DateTimeField(required=False)
    member_since = serializers.DateTimeField(required=False)
    subsidy = serializers.CharField(max_length=8, required=False)
    first_name = serializers.CharField(max_length=128, required=False)
    last_name = serializers.CharField(max_length=128, required=False)
    date_joined = serializers.DateTimeField(required=False)

    class Meta:
        model = get_user_model()

        fields = [
            'email',
            'password',
            'first_name',
            'last_name',
            'phone_number',
            'dob',
            'member_since',
            'is_active',
            'subsidy',
            'date_joined',
        ]

    def update(self, user):
        get_user_model().objects.update(user, self.validated_data)



class UserDisplayShortSerializer(serializers.ModelSerializer):
    email = serializers.CharField(max_length=128, read_only=True)
    first_name = serializers.CharField(max_length=128, read_only=True)
    last_name = serializers.CharField(max_length=128, read_only=True)
    balance = BalanceDisplayShortSerializer()

    class Meta:
        model = get_user_model()

        fields = (
            'id',
            'email',
            'first_name',
            'last_name',
            'balance')


class UserDisplayMiniSerializer(serializers.ModelSerializer):
    email = serializers.CharField(max_length=128, read_only=True)
    first_name = serializers.CharField(max_length=128, read_only=True)
    last_name = serializers.CharField(max_length=128, read_only=True)

    class Meta:
        model = get_user_model()

        fields = (
            'id',
            'email',
            'first_name',
            'last_name',)
