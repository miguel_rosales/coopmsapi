from rest_framework import serializers
from django.contrib.auth import get_user_model



class AdminUserInsertSerializer(serializers.ModelSerializer):
    is_active = serializers.BooleanField(required=False)

    class Meta:
        model = get_user_model()
        fields = (
            'email',
            'password',
            'first_name',
            'last_name',
            'is_active')

    def create(self, super_user_dict):
        return get_user_model().objects.create(super_user_dict)

class AdminUserUpdateSerializer(serializers.ModelSerializer):
    is_active = serializers.BooleanField(required=False)

    class Meta:
        model = get_user_model()
        fields = (
            'email',
            'first_name',
            'last_name',
            'is_active')

    def create(self, super_user_dict):
        return get_user_model().objects.create(super_user_dict)


class AdminUserDisplaySerializer(serializers.ModelSerializer):
    
    class Meta:
        model = get_user_model()
        fields = (
            'id',
            'email',
            'first_name',
            'last_name',
            'password',
            'is_active')


class AdminUserDisplayShortSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = get_user_model()
        fields = (
            'id',
            'email',
            'first_name',
            'last_name',
            'is_active')
