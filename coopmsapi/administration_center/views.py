from datetime import timedelta
from django.utils import timezone
from django.contrib.auth import authenticate
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView
from rest_framework.response import Response
from coopmsapi import settings
from coopmsapi.tools.security import (
    random_token_generator)
from oauth2_provider.models import AccessToken, Application, RefreshToken


class AuthenticationView(APIView):

    """
    View to list all users in the system.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """
    permission_classes = (AllowAny,)

    def post(self, request, format=None):
        client_id = request.data.get('client_id', 123)
        client_secret = request.data.get('client_secret', 123)
        email = request.data.get('username', None)
        password = request.data.get('password', None)
        grant_type = request.data.get('grant_type', 'password')

        if email is None:
            return Response(
                data={'detail': 'invalid email'},
                status=status.HTTP_400_BAD_REQUEST)

        if password is None:
            return Response(
                data={'detail': 'invalid email'},
                status=status.HTTP_400_BAD_REQUEST)

        user = authenticate(username=email, password=password)

        if user is None:
            return Response(
                {'detail': 'ivalid credentials'},
                status.HTTP_401_UNAUTHORIZED)

        try:
            application = Application.objects.get(
                client_id=client_id,
                client_secret=client_secret)
        except Application.DoesNotExist:
            return Response(
                {'detail': 'invalid client_id/client_secret'},
                status.HTTP_401_UNAUTHORIZED)

        scopes = settings.OAUTH2_PROVIDER['SCOPES']
        expire_seconds = settings.PASSWORD_RESET_TOKEN_EXPIRE
        expires = timezone.now() + timedelta(seconds=expire_seconds)
        access_token = AccessToken.objects.create(
            user=user,
            expires=expires,
            token=random_token_generator(
                'sha256',
                str(request) + 'access_token'),
            application=application,
            scope=scopes)

        refresh_token = RefreshToken.objects.create(
            user=user,
            token=random_token_generator(
                'sha256',
                str(request) + 'refresh_token'),
            access_token=access_token,
            application=application)

        token = {
            'access_token': access_token.token,
            'token_type': 'Bearer',
            'expires_in': expire_seconds,
            'refresh_token': refresh_token.token}

        return Response(token, status=status.HTTP_200_OK)

