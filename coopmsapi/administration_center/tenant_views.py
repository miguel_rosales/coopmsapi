from rest_framework import viewsets, status
from rest_framework.response import Response
from coopmsapi.clients.serializers import (
    TenantInsertSerializer,
    TenantDisplaySerializer,
    TenantUpdateSerializer,
    TenantUserDisplaySerializer)
from coopmsapi.clients.models import Client, ClientUsersList
# from django.contrib.auth import get_user_model

class TenantViewSet(viewsets.ModelViewSet):
    serializer_class = TenantDisplaySerializer
    queryset = Client.objects.filter()

    def create(self, request):
        try:
            tenant_serializer = TenantInsertSerializer(data=request.data)

            if tenant_serializer.is_valid():

                if ClientUsersList.objects.\
                    filter(
                        email=tenant_serializer.
                            validated_data['superuser']['email'],
                        is_active=True).\
                        count() >= 1:

                    return Response({'detail': 'email already exists, contact us'},
                                status.HTTP_400_BAD_REQUEST)

                new_tenant = tenant_serializer.create(
                    data=tenant_serializer.validated_data)
                return Response(
                    data=TenantDisplaySerializer(new_tenant).data,
                    status=status.HTTP_201_CREATED)
            else:
                return Response(
                    data=tenant_serializer.errors,
                    status=status.HTTP_400_BAD_REQUEST)

        except IntegrityError as e:
            return Response(
                data=str(e),
                status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk):
        try:
            tenant = Client.objets.get(id=pk)

            serializer = TenantUpdateSerializer(data=request.data)
            if serializer.is_valid():
                tenant_update = serializer.update(data=serializer.validated_data)
                return Response(
                    data=TenantDisplaySerializer(tenant_update).data,
                    status=status.HTTP_200_OK)
            else:
                return Response(
                    data=serializer.errors,
                    status=status.HTTP_400_BAD_REQUEST)

        except Client.DoesNotExist:
            return Response(
                status=status.HTTP_404_NOT_FOUND)

    def partial_update(self, request, pk):
        """
        METHOD NOT IMPLEMENTED
        """
        return Response(
            data={'detail': 'method not allowed'},
            status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def get_serializer_class(self):
        if self.action == 'list':
            return TenantDisplaySerializer

        elif self.action == 'retrieve':
            return TenantDisplaySerializer

        else:
            return TenantDisplaySerializer


class TenantUserViewSet(viewsets.ModelViewSet):
    serializer_class = TenantUserDisplaySerializer
    queryset = ClientUsersList.objects.filter()

    def create(self, request):
        pass

    def update(self, request):
        pass

    def get_serializer_class(self):
        if self.action == 'list':
            return TenantUserDisplaySerializer

        elif self.action == 'retrieve':
            return TenantUserDisplaySerializer

        else:
            return TenantUserDisplaySerializer
