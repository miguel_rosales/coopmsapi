from rest_framework import viewsets, status
from rest_framework.response import Response
from django.db import IntegrityError
from django.contrib.auth import get_user_model
from .serializers import (
    AdminUserInsertSerializer,
    AdminUserDisplaySerializer)

class AdminUserViewSet(viewsets.ModelViewSet):
    queryset = get_user_model().objects.filter()
    serializer_class = AdminUserDisplaySerializer

    def create(self, request):
        serializer = AdminUserInsertSerializer(data=request.data)
        if serializer.is_valid():
            new_admin_user = serializer.create(serializer.validated_data)
            
            return Response(
                data=AdminUserDisplaySerializer(new_admin_user).data,
                status=status.HTTP_200_OK)
        else:
            return Response(
                data=serializer.errors,
                status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk):
        try:
            user = get_user_model().objects.get(id=pk)
            serializer = AdminUserUpdateSerializer(data=request.data)            

            if serializer.is_valid():
                serializer.update(
                    obj=user,
                    data=serializer.validated_data)

            else:
                return Response(
                    data=serializer.errors,
                    status=status.errors)
        except get_user_model().DoesNotExist:
            return Response(
                status=status.HTTP_404_NOT_FOUND)
        except IntegrityError as e:
            return Response(
                data={'detail': str(e)},
                status=status.HTTP_400_BAD_REQUEST)


    def destroy(self, request, pk):
        try:
            get_user_model().objects.get(id=pk)

        except get_user_model().DoesNotExist:
            return Response(
                status=status.HTTP_404_NOT_FOUND)