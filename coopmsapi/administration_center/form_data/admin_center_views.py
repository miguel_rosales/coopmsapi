from coopmsapi.clients.models import Client
from coopmsapi.clients.serializers import (
    TenantDisplayMiniSerializer)
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

class DetailTenantUserForm(APIView):
    """
    form data
    """
    def get(self, request, format=None):
        """
        Return a list of all users.
        """
        tenants = Client.objects.filter(is_active=True)
        serializer = TenantDisplayMiniSerializer(tenants, many=True)

        return Response(
            data={
                'tenant_choices': serializer.data},
            status=status.HTTP_200_OK)