from django.conf.urls import url, include
from rest_framework_nested import routers
from .views import AuthenticationView
from .tenant_views import TenantViewSet, TenantUserViewSet
from .admin_user_views import AdminUserViewSet
from .views import AuthenticationView
from .form_data.admin_center_views import DetailTenantUserForm


tenant_router = routers.SimpleRouter()
tenant_router.register(r'tenants', TenantViewSet, base_name='tenant')

tenant_user_router = routers.SimpleRouter()
tenant_user_router.register(r'tenant_users', TenantUserViewSet, base_name='tenant_user')

admin_user_router = routers.SimpleRouter()
admin_user_router.register(r'users', AdminUserViewSet, base_name='user')

urlpatterns = [
	url(r'^form_data/detail_tenant_user_form',
		DetailTenantUserForm.as_view(),
		name='detail_tenant_user'),
    url(r'^', include(tenant_router.urls)),
    url(r'^', include(admin_user_router.urls)),
    url(r'^auth/', AuthenticationView.as_view(), name='authenticate'),
    url(r'^', include(tenant_user_router.urls))
]
