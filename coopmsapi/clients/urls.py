from django.conf.urls import url, include
from rest_framework_nested import routers
from .views import registration, ClientViewSet
from .views import userResetPassword, userResetTokenValidator
from .views import RequestTokenViewSet
from django.contrib import admin

client_router = routers.SimpleRouter()
client_router.register(r'clients', ClientViewSet, base_name='client')


token_request_router = routers.SimpleRouter()
token_request_router.register(
                        r'request_token',
                        RequestTokenViewSet,
                        base_name='request_token')

urlpatterns = [
    url(r'^registration/', registration, name='registration'),
    url(r'^admin_center/', view=include('coopmsapi.administration_center.urls',
        namespace='admin_center')),
    url(r'^admin/', admin.site.urls),
    url(r'^', include(client_router.urls)),
    url(r'', include(token_request_router.urls)),
    url(r'reset_password', userResetPassword),
    url(r'reset_token_validator', userResetTokenValidator)
]
