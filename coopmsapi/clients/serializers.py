from rest_framework import serializers
from coopmsapi.clients.models import Client, ClientUsersList
from coopmsapi.coop.serializers import CoopInsertSerializer
from coopmsapi.users.serializers import UserInsertSerializer


'''
TENANTS SERIALIZERS
'''


class TenantInsertSerializer(serializers.ModelSerializer):

    coop = CoopInsertSerializer()
    superuser = UserInsertSerializer()
    name = serializers.CharField(max_length=128)
    paid_until = serializers.DateTimeField(required=False)
    is_active = serializers.BooleanField(required=False)
    on_trial = serializers.BooleanField(required=False)

    class Meta:
        model = Client
        fields = (
            'coop',
            'superuser',
            'name',
            'paid_until',
            'is_active',
            'on_trial')

    def create(self, data):
        return Client.objects.create(data)


class TenantUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = (
            'name',
            'paid_until',
            'is_active',
            'on_trial')

    def update(self, obj, data):
        return Client.objects.update(client=obj, client_dict=data)


class TenantDisplaySerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = (
            'id',
            'account_no',
            'name',
            'domain_url',
            'schema_name',
            'paid_until',
            'is_active',
            'on_trial')


class TenantDisplayShortSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = (
            'id',
            'account_no',
            'name',
            'domain_url',
            'paid_until',
            'is_active',
            'on_trial')

class TenantDisplayMiniSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = (
            'id',
            'account_no',
            'name',
            'is_active')


'''
TENANT USERS SERIALIZERS
'''

class TenantUserDisplaySerializer(serializers.ModelSerializer):

    client = TenantDisplayMiniSerializer()

    class Meta:
        model = ClientUsersList
        fields = (
            'id',
            'email',
            'client',
            'is_active',
            'password_reset_request',
            'password_reset_token',
            'token_expires')

'''
 {
    "on_trial":false,
    "domain_url": "cooptest",
    "name": "Cooperative test",
    "paid_until": "2016-12-05 00:00:00",

    "superuser":{
            "first_name":"Miguel",
            "last_name":"Rosales",
            "email":"miguel.f.rosales@gmail.com",
            "password":"4n4m4ri4"
        },
    "coop":{
            "civil_number": "test",
            "street":"address",
            "city": "Montreal",
            "postal_code": "H1X1W6"
        }
}
'''