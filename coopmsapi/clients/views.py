from datetime import timedelta
from django.core.mail import send_mail
from django.db import IntegrityError, transaction
from django.core.exceptions import ValidationError
from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth import authenticate
from django.contrib.auth import get_user_model
from django.utils import timezone
from django.template import RequestContext
from tenant_schemas.utils import tenant_context
# rest framework
from rest_framework.decorators import api_view
from rest_framework import viewsets, mixins, status
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
# oauth2
from oauth2_provider.models import AccessToken, Application, RefreshToken
# misc
from coopmsapi import settings
# from coopmsapi.coop.models import Coop
from .serializers import TenantInsertSerializer, TenantDisplaySerializer
from .models import Client, ClientUsersList
from .forms import RegistrationForm
from .generators import random_token_generator, random_id


@transaction.atomic
def registration(request):
    if request.method == 'GET':
        form = RegistrationForm()
        context = {'form': form}
        return render(request, 'clients_register.html', context)

    elif request.method == 'POST':
        registration_form = RegistrationForm(request.POST or None)

        try:
            if registration_form.is_valid():
                if ClientUsersList\
                    .objects\
                    .filter(
                        email=registration_form.cleaned_data['email'],
                        is_active=True).count() > 0:
                    raise IntegrityError('User already exists')

                coop_name_data = random_id()
                client_dict = {}
                client_dict['name'] = registration_form.\
                    cleaned_data['coop_name']
                client_dict['domain_url'] = coop_name_data
                client_dict['schema_name'] = coop_name_data
                client_dict['paid_until'] = timezone.now()
                client_dict['on_trial'] = False

                user_dict = {}
                user_dict['email'] = registration_form.cleaned_data['email']
                user_dict['first_name'] = registration_form\
                    .cleaned_data['first_name']
                user_dict['last_name'] = registration_form\
                    .cleaned_data['last_name']
                user_dict['password'] = registration_form\
                    .cleaned_data['password']

                coop_dict = {}
                coop_dict['name'] = registration_form.cleaned_data['coop_name']
                coop_dict['civil_number'] = registration_form\
                    .cleaned_data['civil_number']
                coop_dict['street'] = registration_form\
                    .cleaned_data['street']
                coop_dict['city'] = registration_form\
                    .cleaned_data['city']
                coop_dict['postal_code'] = registration_form\
                    .cleaned_data['postal_code']

                client_serializer = ClientSerializer(data=client_dict)
                user_serializer = UserInsertSerializer(data=user_dict)
                coop_serializer = CoopSerializer(data=coop_dict)

                if client_serializer.is_valid():
                    if user_serializer.is_valid():
                        if coop_serializer.is_valid():
                            new_client = client_serializer.create(
                                validated_data=client_serializer.validated_data,
                                coop_validated_data=coop_serializer.validated_data,
                                    user_validated_data=user_serializer.validated_data)
                            context = {'account_no': new_client.account_no}
                            return render(request, 'clients_success.html', context)
                        else:
                            variables = RequestContext(
                                request,
                                {'form': registration_form})
                            return render(
                                request,
                                'clients_register.html', variables)
                    else:
                        variables = RequestContext(
                            request,
                            {'form': registration_form})
                        return render(
                            request,
                            'clients_register.html', variables)
                else:
                    print(client_serializer.errors)
                    variables = RequestContext(
                        request,
                        {'form': registration_form})
                    return render(request, 'clients_register.html', variables)
            else:
                variables = RequestContext(
                    request,
                    {'form': registration_form})
                return render(request, 'clients_register.html', variables)

        except IntegrityError as e:
            variables = RequestContext(request, {'form': form})
            return render(request, 'clients_register.html', context)


class ClientViewSet(mixins.CreateModelMixin,
                    mixins.ListModelMixin,
                    mixins.RetrieveModelMixin,
                    viewsets.GenericViewSet):

    serializer_class = TenantDisplaySerializer
    queryset = Client.objects.filter()
    permission_classes = (AllowAny,)

    def validate_request(self, request):
        try:
            if request.META['HTTP_AUTH_KEY'] != settings.AUTH_KEY:
                return False
            else:
                return True
        except KeyError:
            return False

    def create(self, request):
        """
        PUBLIC SCHEMA ENDPOINT CREATION OF NEW COOP WORK GROUP
        """
        try:
            if self.validate_request(request) == False:
                return Response(
                            {'detail': "invalid 'Auth-key' header"},
                            status.HTTP_400_BAD_REQUEST)

            tenant_serializer = TenantInsertSerializer(data=request.data)

            if tenant_serializer.is_valid():

                if ClientUsersList.objects.\
                    filter(
                        email=tenant_serializer.
                            validated_data['superuser']['email'],
                        is_active=True).\
                        count() >= 1:

                    return Response({'detail': 'email already exists, contact us'},
                                status.HTTP_400_BAD_REQUEST)

                new_tenant = tenant_serializer.create(
                    data=tenant_serializer.validated_data)
                return Response(
                    data=TenantDisplaySerializer(new_tenant).data,
                    status=status.HTTP_201_CREATED)
            else:
                return Response(
                    data=tenant_serializer.errors,
                    status=status.HTTP_400_BAD_REQUEST)

        except IntegrityError as e:
            return Response(
                data=str(e),
                status=status.HTTP_400_BAD_REQUEST)


    def update(self, request, pk):
        """
        PUBLIC SCHEMA UPDATE WORK GROUP INFORMATION
        """
        try:
            client = Client.objects.get(pk)
        
            serializer = TenantUpdateSerializer(request.data)
            if serializer.is_valid():
                new_client = serializer.update(
                    obj=client,
                    data=serializer.validated_data)
                return Response(
                    data=TenantDisplaySerializer(new_client).data,
                    status=status.HTTP_200_OK)
            else:
                return Response(
                    data=serializer.errors,
                    status=status.HTTP_400_BAD_REQUEST)
        
        except Client.DoesNotExist as e:
            return Response(
                data=str(e),
                status=status.HTTP_404_NOT_FOUND)
        except IntegrityError as e:
            return Response(
                data=str(e),
                status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def userResetPassword(request):

    if request.method == 'POST':

        email = request.data.get('email', None)
        try:
            current_user = ClientUsersList.objects.get(
                email=email,
                is_active=True)
        except ClientUsersList.DoesNotExist:
            return Response(data={'detail': 'user does not exist'},
                            status=status.HTTP_404_NOT_FOUND)
        except ClientUsersList.MultipleObjectsReturned:
            return Response(
                data={'detail': 'multiple active accounts contact us'},
                status=status.HTTP_400_BAD_REQUEST)

        try:
            with tenant_context(current_user.client):
                local_user = get_user_model().objects.get(email=email)
                if local_user.is_active is False:
                    return Response({'detail': 'user inactive'},
                                    status.HTTP_401_UNAUTHORIZED)

        except get_user_model().DoesNotExist:
            return Response({'detail': 'user does not exist'},
                            status.HTTP_404_NOT_FOUND)
        except Exception as e:
            return Response({'detail': 'invalid data'},
                            status.HTTP_400_BAD_REQUEST)
        expire_seconds = settings.PASSWORD_RESET_TOKEN_EXPIRE
        current_user.password_reset_request = True
        current_user.password_reset_token = \
            random_token_generator('sha256', str(email))
        current_user.token_expires = timezone.now() +\
            timedelta(seconds=expire_seconds)
        current_user.save(update_fields=[
            'password_reset_request',
            'password_reset_token',
            'token_expires'])
        return Response(status=status.HTTP_204_NO_CONTENT)

@api_view(['POST'])
def userResetTokenValidator(request):
    if request.method == 'POST':
        email = request.data.get('email', None)
        token = request.data.get('token', None)
        password = request.data.get('password', None)

        if email is None:
            return Response(
                {'detail': 'email missing'},
                status.HTTP_400_BAD_REQUEST)

        if token is None:
            return Response(
                {'detail': 'token missing'},
                status.HTTP_400_BAD_REQUEST)

        if password is None:
            return Response(
                {'detail': 'new password missing'},
                status.HTTP_400_BAD_REQUEST)

        try:
            current_user = ClientUsersList.\
                objects.get(email=email, is_active=True)
            if current_user.password_reset_token is not token or\
                    timezone.now() > current_user.token_expires or\
                    current_user.password_reset_request is False:
                return Response({'detail': 'invalid token'})

            with tenant_context(current_user.client):
                try:
                    local_user = get_user_model().objects.get(email=email)
                    local_user.modified_date = timezone.now()
                    local_user.modified_by = local_user
                    local_user.save(
                        update_fields=[
                            'modified_date',
                            'modified_by'])
                    local_user.set_password(password)
                    local_user.save()
                except get_user_model().DoesNotExist:
                    return Response({'detail': 'user not found'},
                                    status.HTTP_404_NOT_FOUND)
                except get_user_model().MultipleObjectsReturned:
                    return Response(
                        {'detail': 'multiple user occurrences active'},
                        status.HTTP_400_BAD_REQUEST)
            current_user.password_reset_request = False
            current_user.save(update_fields=['password_reset_request'])

        except ClientUsersList.DoesNotExist:
            return Response({'detail': 'user not found'},
                            status.HTTP_404_NOT_FOUND)
        except ClientUsersList.MultipleObjectsReturned:
            return Response({'detail': 'multiple user occurrences active'},
                            status.HTTP_400_BAD_REQUEST)


class RequestTokenViewSet(
                mixins.CreateModelMixin,
                viewsets.GenericViewSet):

    permission_classes = (AllowAny,)
    serializer_class = TenantDisplaySerializer
    queryset = Client.objects.filter()

    def create(self, request):
        client_id = request.data.get('client_id', request.query_params.get('client_id', None))
        client_secret = request.data.get('client_secret', request.query_params.get('client_secret', None))
        email = request.data.get('username', request.data.get('email', None))
        password = request.data.get('password', None)
        grant_type = request.data.get('grant_type', 'password')

        if client_id is None:
            return Response(
                {'detail': 'missing client_id'},
                status.HTTP_400_BAD_REQUEST)

        if client_secret is None:
            return Response(
                {'detail': 'missing client_secret'},
                status.HTTP_400_BAD_REQUEST)

        if email is None:
            return Response(
                {'detail': 'missing username'},
                status.HTTP_400_BAD_REQUEST)

        if password is None:
            return Response(
                {'detail': 'missing password'},
                status.HTTP_400_BAD_REQUEST)

        if grant_type is None:
            return Response(
                {'detail': 'missing grant_type'},
                status.HTTP_400_BAD_REQUEST)

        try:
            user_tenant_association = ClientUsersList.objects.get(
                email=email,
                is_active=True)
            if user_tenant_association.password_reset_request is True:
                return Response(
                    {'detail': 'complete password reset request'},
                    status.HTTP_401_UNAUTHORIZED)

            current_client = Client.objects.get(
                id=user_tenant_association.client.id)
            if not current_client.is_active:
                return Response(
                    {'detail': 'co-op account not active'},
                    status.HTTP_401_UNAUTHORIZED)
        except ClientUsersList.DoesNotExist:
            return Response(
                {'detail': 'invalid credentials'},
                status.HTTP_401_UNAUTHORIZED)
        except ClientUsersList.MultipleObjectsReturned:
            return Response(
                {'detail': 'multiple user occurrences active'},
                status.HTTP_401_UNAUTHORIZED)

        expire_seconds = settings.\
            OAUTH2_PROVIDER['ACCESS_TOKEN_EXPIRE_SECONDS']
        scopes = settings.OAUTH2_PROVIDER['SCOPES']

        with tenant_context(current_client):

            user = authenticate(username=email, password=password)

            if user is None:
                return Response(
                    {'detail': 'ivalid credentials'},
                    status.HTTP_401_UNAUTHORIZED)

            if not user.is_active:
                return Response(
                    {'detail': 'user account deactivated'},
                    status.HTTP_401_UNAUTHORIZED)
            try:
                application = Application.objects.get(
                    client_id=client_id,
                    client_secret=client_secret)
            except Application.DoesNotExist:
                return Response(
                    {'detail': 'invalid client_id/client_secret'},
                    status.HTTP_401_UNAUTHORIZED
                )
            expires = timezone.now() + timedelta(seconds=expire_seconds)
            access_token = AccessToken.objects.create(
                user=user,
                expires=expires,
                token=random_token_generator(
                    'sha256',
                    str(request) + 'access_token'),
                application=application,
                scope=scopes
            )

            refresh_token = RefreshToken.objects.create(
                user=user,
                token=random_token_generator(
                    'sha256',
                    str(request) + 'refresh_token'),
                access_token=access_token,
                application=application
            )

            token = {
                'access_token': access_token.token,
                'token_type': 'Bearer',
                'expires_in': expire_seconds,
                'refresh_token': refresh_token.token,
                'domain_url': current_client.domain_url
                }

            return Response(token, status=status.HTTP_200_OK)


"""
 {
    "on_trial":false,
    "name": "Cooperative test",
    "paid_until": "2016-12-05 00:00:00",

    "superuser":{
            "first_name":"Miguel",
            "last_name":"Rosales",
            "email":"miguel.f.rosales@gmail.com",
            "password":"4n4m4ri4"
        },
    "coop":{
            "civil_number": "4563",
            "street":"saint-joseph",
            "postal_code": "H1X1W6"
        }
}
"""
