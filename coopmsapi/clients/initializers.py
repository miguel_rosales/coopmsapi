from coopmsapi.accounting.models import (
                            GLAccount,
                            Setting,
                            GLAccountCategory)
from coopmsapi.coop.models import Coop
from coopmsapi.locale.models import LocalizedText, Locale
from coopmsapi.receivables.models import(
    Setting as ArSetting)
from oauth2_provider.models import Application


class Initializer(object):

    @staticmethod
    def launch(
            user,
            tenant,
            coop_dict):
        # oauth2 application
        new_application = Application(
            client_id='123',
            client_secret='123',
            client_type='confidential',
            authorization_grant_type='password',
            name='coopms',
            skip_authorization=False,
            user_id=2,
            )
        new_application.save()

        english_locale = Locale.objects.create(locale='en')
        french_locale = Locale.objects.create(locale='fr')

        ar_settings = ArSetting.objects.create(
            inv_default_locale=french_locale)



        # LIABILITIES
        text_fr = LocalizedText.objects.create(french_locale, 'Passif')
        text_en = LocalizedText.objects.create(english_locale, 'Liability')
        category_liability = GLAccountCategory.objects.create()
        category_liability.name.add(text_fr, text_en)

        text_fr = LocalizedText.objects.create(
            french_locale,
            'Découvert bancaire')
        text_en = LocalizedText.objects.create(english_locale, 'Overdraft')
        GLAccount.objects.create(
            code=2000,
            user=user,
            category=category_liability).name.add(text_fr, text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Emprunt - autre')
        text_en = LocalizedText.objects.create(english_locale, 'Loan - other')
        GLAccount.objects.create(
            code=2075,
            user=user,
            category=category_liability).name.add(text_fr, text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Créditeurs')
        text_en = LocalizedText.objects.create(english_locale, 'Accounts payables')
        ap_default_gl_account = GLAccount.objects.create(code=2100,user=user,category=category_liability)
        ap_default_gl_account.name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Fournisseurs')
        text_en = LocalizedText.objects.create(english_locale,'Suppliers')
        GLAccount.objects.create(code=2110,user=user,category=category_liability).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Assurances')
        text_en = LocalizedText.objects.create(english_locale, 'Insurances')
        GLAccount.objects.create(code=2115,user=user,category=category_liability).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Électricité')
        text_en = LocalizedText.objects.create(english_locale, 'Electricity')
        GLAccount.objects.create(code=2120,user=user,category=category_liability).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Chauffage')
        text_en = LocalizedText.objects.create(english_locale, 'Heating')
        GLAccount.objects.create(code=2125,user=user,category=category_liability).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Salaires')
        text_en = LocalizedText.objects.create(english_locale, 'Wages')
        GLAccount.objects.create(code=2130,user=user,category=category_liability).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Sommes à remettre à l''état')
        text_en = LocalizedText.objects.create(english_locale, 'Remittances to the state')
        GLAccount.objects.create(code=2131,user=user,category=category_liability).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'TPS – TVQ – commercial')
        text_en = LocalizedText.objects.create(english_locale, 'GST - QST - commercial')
        GLAccount.objects.create(code=2135,user=user,category=category_liability).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Dépôts')
        text_en = LocalizedText.objects.create(english_locale, 'Deposits')
        GLAccount.objects.create(code=2140,user=user,category=category_liability).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Impôts fonciers')
        text_en = LocalizedText.objects.create(english_locale, 'Property taxes')
        GLAccount.objects.create(code=2150,user=user,category=category_liability).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Intérêts courus sur l''emprunt hypothécaire')
        text_en = LocalizedText.objects.create(english_locale, 'Accrued interest on the mortgage')
        GLAccount.objects.create(code=2160,user=user,category=category_liability).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Subventions – SHQ')
        text_en = LocalizedText.objects.create(english_locale, 'Subsidies - SHQ')
        GLAccount.objects.create(code=2170,user=user,category=category_liability).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Subventions - SCHL')
        text_en = LocalizedText.objects.create(english_locale, 'Subsidies - CMHC')
        GLAccount.objects.create(code=2175,user=user,category=category_liability).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Frais courus')
        text_en = LocalizedText.objects.create(english_locale, 'Accrued liabilities')
        GLAccount.objects.create(code=2180,user=user,category=category_liability).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Autres créditeurs')
        text_en = LocalizedText.objects.create(english_locale, 'Others creditors')
        GLAccount.objects.create(code=2190,user=user,category=category_liability).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Revenus reportés')
        text_en = LocalizedText.objects.create(english_locale, 'Deferred revenue')
        GLAccount.objects.create(code=2200,user=user,category=category_liability).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Dette à long terme échéant au cours du prochain exercice')
        text_en = LocalizedText.objects.create(english_locale, 'Long-term debt maturing over the next year')
        GLAccount.objects.create(code=2400,user=user,category=category_liability).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Emprunt - prêt de sauvetage échéant au cours du prochain exercice')
        text_en = LocalizedText.objects.create(english_locale, 'Loan - rescue loan maturing over the next year')
        GLAccount.objects.create(code=2410,user=user,category=category_liability).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Dette à long terme')
        text_en = LocalizedText.objects.create(english_locale, 'Long  - term debt')
        GLAccount.objects.create(code=2500,user=user,category=category_liability).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Dette à long terme relative aux immobilisations (Dette à long terme)')
        text_en = LocalizedText.objects.create(english_locale, 'Long-term debt related to capital assets (long-term debt)')
        GLAccount.objects.create(code=2510,user=user,category=category_liability).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Autre dette à long terme (Dette à long terme)')
        text_en = LocalizedText.objects.create(english_locale, 'Other long-term debt (long-term debt)')
        GLAccount.objects.create(code=2515,user=user,category=category_liability).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Prêt - Fonds de stabilisation')
        text_en = LocalizedText.objects.create(english_locale, 'Loaning - Stabilization Fund')
        GLAccount.objects.create(code=2550,user=user,category=category_liability).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Dette à long terme relative aux immobilisations (Fonds de stabilisation)')
        text_en = LocalizedText.objects.create(english_locale, 'Long-term debt related to capital assets (Stabilization fund)')
        GLAccount.objects.create(code=2560,user=user,category=category_liability).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Autre dette à long terme (Fond de stabilization)')
        text_en = LocalizedText.objects.create(english_locale, 'Other long-term debt (Stabilization fund)')
        GLAccount.objects.create(code=2565,user=user,category=category_liability).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Intérêts cumulés (Fond de stabilization)')
        text_en = LocalizedText.objects.create(english_locale, 'Accumulated interest (Stabilization fund)')
        GLAccount.objects.create(code=2570,user=user,category=category_liability).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Prêt - Plan de sauvetage')
        text_en = LocalizedText.objects.create(english_locale, 'Loaning - Rescue Plan')
        GLAccount.objects.create(code=2600,user=user,category=category_liability).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Dette à long terme relative aux immobilisations (Plan de sauvetage)')
        text_en = LocalizedText.objects.create(english_locale, 'Long-term debt related to capital assets (Rescue plan)')
        GLAccount.objects.create(code=2610,user=user,category=category_liability).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Autre dette à long terme (Plan de sauvetage)')
        text_en = LocalizedText.objects.create(english_locale, 'Other long-term debt (Rescue plan)')
        GLAccount.objects.create(code=2615,user=user,category=category_liability).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Intérêts cumulés (Plan de sauvetage)')
        text_en = LocalizedText.objects.create(english_locale, 'Accumulated interest (Rescue plan)')
        GLAccount.objects.create(code=2620, user=user, category=category_liability).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Apports reportés (Plan de sauvetage)')
        text_en = LocalizedText.objects.create(english_locale, 'Deferred contributions (Rescue plan)')
        GLAccount.objects.create(code=2750,user=user,category=category_liability).name.add(text_fr,text_en)

        # ASSETS
        text_fr = LocalizedText.objects.create(french_locale, 'Actif')
        text_en = LocalizedText.objects.create(english_locale, 'Asset')
        category_asset = GLAccountCategory.objects.create()
        category_asset.name.add(text_fr, text_en)


        text_fr = LocalizedText.objects.create(french_locale, 'Encaisse')
        text_en = LocalizedText.objects.create(english_locale, 'Cash')
        default_asset_account = GLAccount.objects.create(code=1100,category=category_asset,user=user)
        default_asset_account.name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Encaisse réservée pour taxes')
        text_en = LocalizedText.objects.create(english_locale, 'Cash reserved for taxes')
        GLAccount.objects.create(code=1200,category=category_asset,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Placements')
        text_en = LocalizedText.objects.create(english_locale, 'Investments')
        GLAccount.objects.create(code=1250,category=category_asset,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Débiteurs')
        text_en = LocalizedText.objects.create(english_locale, 'Accounts receivables')
        default_ar_account = GLAccount.objects.create(code=1300,category=category_asset,user=user)
        default_ar_account.name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Clients')
        text_en = LocalizedText.objects.create(english_locale, 'Clients')
        ar_client_default_asset_gl_account = GLAccount.objects.create(
            code=1305,
            category=category_asset,
            user=user)
        ar_client_default_asset_gl_account.name.add(text_fr, text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Loyers')
        text_en = LocalizedText.objects.create(english_locale, 'Rent')
        ar_rent_default_gl_account = GLAccount.objects.create(code=1310,category=category_asset,user=user)
        ar_rent_default_gl_account.name.add(text_fr, text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Provision pour créances douteuses')
        text_en = LocalizedText.objects.create(english_locale, 'Allowance for doubtful accounts')
        GLAccount.objects.create(code=1311,category=category_asset,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'TPS – TVQ')
        text_en = LocalizedText.objects.create(english_locale, 'GST - QST')
        GLAccount.objects.create(code=1320,category=category_asset,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Subventions – SCHL')
        text_en = LocalizedText.objects.create(english_locale, 'Subsidies - CMHC')
        GLAccount.objects.create(code=1350,category=category_asset,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Subventions - SCHL - Aide enrichie')
        text_en = LocalizedText.objects.create(english_locale, 'Subsidies - CMHC - Help enriched')
        GLAccount.objects.create(code=1355,category=category_asset,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Subventions – SHQ')
        text_en = LocalizedText.objects.create(english_locale, 'Subsidies -SHQ')
        GLAccount.objects.create(code=1360,category=category_asset,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Intérêts courus')
        text_en = LocalizedText.objects.create(english_locale, 'Accrued interest')
        GLAccount.objects.create(code=1370,category=category_asset,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Autres débiteurs')
        text_en = LocalizedText.objects.create(english_locale, 'Others receivables')
        GLAccount.objects.create(code=1390,category=category_asset,user=user).name.add(text_fr,text_en)
        text_fr = LocalizedText.objects.create(french_locale, 'Frais payés d''avance')
        text_en = LocalizedText.objects.create(english_locale, 'Prepaid expenses')
        default_over_payment_ap = GLAccount.objects.create(code=1500,category=category_asset,user=user)
        default_over_payment_ap.name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Impôts fonciers')
        text_en = LocalizedText.objects.create(english_locale, 'Property taxes')
        GLAccount.objects.create(code=1510,category=category_asset,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Assurances')
        text_en = LocalizedText.objects.create(english_locale, 'Insurance')
        GLAccount.objects.create(code=1520,category=category_asset,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Électricité')
        text_en = LocalizedText.objects.create(english_locale, 'Electricity')
        GLAccount.objects.create(code=1530,category=category_asset,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Chauffage')
        text_en = LocalizedText.objects.create(english_locale, 'Heating')
        GLAccount.objects.create(code=1535,category=category_asset,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'C.S.S.T.')
        text_en = LocalizedText.objects.create(english_locale, 'C.S.S.T.')
        GLAccount.objects.create(code=1540,category=category_asset,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Association et cotisation')
        text_en = LocalizedText.objects.create(english_locale, 'Association dues')
        GLAccount.objects.create(code=1545,category=category_asset,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Autres')
        text_en = LocalizedText.objects.create(english_locale, 'Others receivables')
        GLAccount.objects.create(code=1550,category=category_asset,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Encaisse et dépôts assujettis à des restrictions')
        text_en = LocalizedText.objects.create(english_locale, 'Cash and deposits subject to restrictions')
        GLAccount.objects.create(code=1600,category=category_asset,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Réserve de remplacement')
        text_en = LocalizedText.objects.create(english_locale, 'Replacement Reserve')
        GLAccount.objects.create(code=1610,category=category_asset,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Réserve de subventions excédentaires')
        text_en = LocalizedText.objects.create(english_locale, 'Subsidy surplus reserve')
        GLAccount.objects.create(code=1620,category=category_asset,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Placements')
        text_en = LocalizedText.objects.create(english_locale, 'Investments')
        GLAccount.objects.create(code=1700,category=category_asset,user=user).name.add(text_fr,text_en)
        text_fr = LocalizedText.objects.create(french_locale, 'Immobilisations corporelles')
        text_en = LocalizedText.objects.create(english_locale, 'Fixed assets')
        GLAccount.objects.create(code=1800,category=category_asset,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Terrain')
        text_en = LocalizedText.objects.create(english_locale, 'Land')
        GLAccount.objects.create(code=1805,category=category_asset,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Bâtiment')
        text_en = LocalizedText.objects.create(english_locale, 'Building')
        GLAccount.objects.create(code=1810,category=category_asset,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Amortissement cumulé –  Bâtiment')
        text_en = LocalizedText.objects.create(english_locale, 'Accumulated Depreciation - Building')
        GLAccount.objects.create(code=1811,category=category_asset,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Biens meubles et équipements')
        text_en = LocalizedText.objects.create(english_locale, 'Movable property and equipment')
        GLAccount.objects.create(code=1820,category=category_asset,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Amortissement cumulé - biens meubles et équipements')
        text_en = LocalizedText.objects.create(english_locale, 'Accumulated amortization - movable property and equipment')
        GLAccount.objects.create(code=1821,category=category_asset,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Autres immobilisations')
        text_en = LocalizedText.objects.create(english_locale, 'Others properties')
        GLAccount.objects.create(code=1830,category=category_asset,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Amortissement cumulé - autres immobilisations')
        text_en = LocalizedText.objects.create(english_locale, 'Accumulated Depreciation - others proporties')
        GLAccount.objects.create(code=1831,category=category_asset,user=user).name.add(text_fr,text_en)

        #Net assets
        text_fr = LocalizedText.objects.create(french_locale, 'Actif net')
        text_en = LocalizedText.objects.create(english_locale, 'Net Asset')
        category_net_asset = GLAccountCategory.objects.create()
        category_net_asset.name.add(text_fr, text_en)

        text_fr = LocalizedText.objects.create(french_locale,'Autres')
        text_en = LocalizedText.objects.create(english_locale, 'Others')
        GLAccount.objects.create(3505,category=category_net_asset,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Réserve de remplacement')
        text_en = LocalizedText.objects.create(english_locale, 'Replacement Reserve')
        GLAccount.objects.create(3210,category=category_net_asset,user=user).name.add(text_fr,text_en)
        text_fr = LocalizedText.objects.create(french_locale, 'Contributions supplémentaires')
        text_en = LocalizedText.objects.create(english_locale, 'Additional contributions')
        GLAccount.objects.create(3230,category=category_net_asset,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Réserve de subventions excédentaires')
        text_en = LocalizedText.objects.create(english_locale, 'Subsidy surplus reserve')
        GLAccount.objects.create(3240,category=category_net_asset,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Réserve de sécurité d''occupation')
        text_en = LocalizedText.objects.create(english_locale, 'Occupation safety reserve')
        GLAccount.objects.create(3250,category=category_net_asset,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Réserve spéciale')
        text_en = LocalizedText.objects.create(english_locale, 'Special reserve')
        GLAccount.objects.create(3270,category=category_net_asset,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Autres affectations internes (à préciser)')
        text_en = LocalizedText.objects.create(english_locale, 'Other internal assignments (specify)')
        GLAccount.objects.create(3290,category=category_net_asset,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Parts sociales')
        text_en = LocalizedText.objects.create(english_locale, 'Shares')
        GLAccount.objects.create(3300,category=category_net_asset,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Actif net investi en immobilisation (ANII)')
        text_en = LocalizedText.objects.create(english_locale, 'Net assets invested in capital (NAIC)')
        GLAccount.objects.create(3350,category=category_net_asset,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Déficit des activités non résidentielles')
        text_en = LocalizedText.objects.create(english_locale, 'Nonresidential activities dificit')
        GLAccount.objects.create(3370,category=category_net_asset,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Intérêts cumulés')
        text_en = LocalizedText.objects.create(english_locale, 'Accumulated interest')
        GLAccount.objects.create(3400,category=category_net_asset,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Actif net non affecté autre (ANNAA)')
        text_en = LocalizedText.objects.create(english_locale, 'Unrestricted net assets other (UNAO)')
        GLAccount.objects.create(3500,category=category_net_asset,user=user).name.add(text_fr,text_en)
        text_fr = LocalizedText.objects.create(french_locale, 'Surplus (déficit)cumulé')
        text_en = LocalizedText.objects.create(english_locale, 'Surplus (Deficit)')
        GLAccount.objects.create(3502,category=category_net_asset,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Surplus (déficit)cumulé résiduel pour un OSBL')
        text_en = LocalizedText.objects.create(english_locale, 'Surplus (deficit)accumulated residual for Non profits organization')
        GLAccount.objects.create(3503,category=category_net_asset,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Déficit cumulé financé par une autre dette à long terme')
        text_en = LocalizedText.objects.create(english_locale, 'Accumulated deficit financed by other long-term debt')
        GLAccount.objects.create(3504,category=category_net_asset,user=user).name.add(text_fr,text_en)

        #Revenues

        text_fr = LocalizedText.objects.create(french_locale, 'Produits')
        text_en = LocalizedText.objects.create(english_locale, 'Revenues')
        category_revenues = GLAccountCategory.objects.create()
        category_revenues.name.add(text_fr, text_en)


        text_fr = LocalizedText.objects.create(french_locale, 'Revenus de loyers résidentiels')
        text_en = LocalizedText.objects.create(english_locale, 'Income residential rental')
        GLAccount.objects.create(4000,category=category_revenues,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Potentiel annuel des loyers')
        text_fr = LocalizedText.objects.create(english_locale, 'Potential annual rents')
        GLAccount.objects.create(4005,category=category_revenues,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Perte relative aux logements vacants')
        text_en = LocalizedText.objects.create(english_locale, 'Loss on vacant housing')
        GLAccount.objects.create(4010,category=category_revenues,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Perte relative aux mois gratuits')
        text_en = LocalizedText.objects.create(english_locale, 'Loss on free months')
        GLAccount.objects.create(4015,category=category_revenues,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Rabais membres  (pour les coopératives)')
        text_en = LocalizedText.objects.create(english_locale, 'Member discount (for cooperatives)')
        GLAccount.objects.create(4020,category=category_revenues,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Subventions annuelles – Locataires (AACR)')
        text_en = LocalizedText.objects.create(english_locale, 'Annual subsidies - Tenants (RGI)')
        GLAccount.objects.create(4025,category=category_revenues,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Utilisation de la réserve de subventions excédentaires')
        text_en = LocalizedText.objects.create(english_locale, 'Use of the reserve of surplus subsidies')
        GLAccount.objects.create(4030,category=category_revenues,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Utilisation de la réserve de sécurité d''occupation')
        text_en = LocalizedText.objects.create(english_locale, 'Using the occupation safety reserve')
        GLAccount.objects.create(4035,category=category_revenues,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Programme de supplément au loyer - SHQ')
        text_en = LocalizedText.objects.create(english_locale, 'Rent Supplement Program - SHQ')
        GLAccount.objects.create(4040,category=category_revenues,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Subventions - SCHL')
        text_en = LocalizedText.objects.create(english_locale, 'Subsidies - CMHC')
        GLAccount.objects.create(4200,category=category_revenues,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Aide prédéterminée')
        text_en = LocalizedText.objects.create(english_locale, 'Predetermined aid')
        GLAccount.objects.create(4210,category=category_revenues,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Aide assujettie au contrôle du revenu (AACR)')
        text_en = LocalizedText.objects.create(english_locale, 'Rent-geared to income subsidey (RGI)')
        GLAccount.objects.create(4220,category=category_revenues,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Subvenions – SHQ (Programme de supplément au loyer)')
        text_en = LocalizedText.objects.create(english_locale, 'Subsidies - SHQ (Rent supplement program)')
        GLAccount.objects.create(4300,category=category_revenues,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Subventions autres')
        text_en = LocalizedText.objects.create(english_locale, 'Others subsidies')
        GLAccount.objects.create(4350,category=category_revenues,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Aide enrichie')
        text_en = LocalizedText.objects.create(english_locale, 'Enriched aid')
        GLAccount.objects.create(4500,category=category_revenues,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Intérêts')
        text_en = LocalizedText.objects.create(english_locale, 'interests')
        GLAccount.objects.create(4600,category=category_revenues,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Amortissement - apports reportés')
        text_en = LocalizedText.objects.create(english_locale, 'Amortissement - Apports Reportes')
        GLAccount.objects.create(4850,category=category_revenues,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Autres revenus')
        text_en = LocalizedText.objects.create(english_locale, 'Others income')
        GLAccount.objects.create(4900,category=category_revenues,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Revenus de location')
        text_en = LocalizedText.objects.create(english_locale, 'Rental income')
        GLAccount.objects.create(4905,category=category_revenues,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Revenus de stationnement')
        text_en = LocalizedText.objects.create(english_locale, 'Parking revenues')
        GLAccount.objects.create(4910,category=category_revenues,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Revenus de garage')
        text_en = LocalizedText.objects.create(english_locale, 'Garage income')
        GLAccount.objects.create(4915,category=category_revenues,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Revenus de buanderie')
        text_en = LocalizedText.objects.create(english_locale, 'Laundry income')
        GLAccount.objects.create(4920,category=category_revenues,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Surcharge de loyers')
        text_en = LocalizedText.objects.create(english_locale, 'Overload rents')
        GLAccount.objects.create(4925,category=category_revenues,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Recouvrement des créances douteuses')
        text_en = LocalizedText.objects.create(english_locale, 'Recovery of bad debts')
        GLAccount.objects.create(4950,category=category_revenues,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'TPS - TVQ – réserve de remplacement')
        text_en = LocalizedText.objects.create(english_locale, 'GST -QST - Replacement Reserve')
        GLAccount.objects.create(4960,category=category_revenues,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'TPS – TVQ – travaux plan de sauvetage')
        text_en = LocalizedText.objects.create(english_locale, 'GST - QST - Work rescue plan')
        GLAccount.objects.create(4965,category=category_revenues,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Produit de disposition d''immobilisations')
        text_en = LocalizedText.objects.create(english_locale, 'Proceeds from disposal of fixed assets')
        GLAccount.objects.create(4990,category=category_revenues,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Autres revenus (si aucun poste existant)')
        text_en = LocalizedText.objects.create(english_locale, 'Other income (if no post exists)')
        GLAccount.objects.create(4999,category=category_revenues,user=user).name.add(text_fr,text_en)

        # Expenses
        text_fr = LocalizedText.objects.create(french_locale, 'Charges')
        text_en = LocalizedText.objects.create(english_locale, 'Expenses')
        category_expenses = GLAccountCategory.objects.create()
        category_expenses.name.add(text_fr, text_en)


        text_fr = LocalizedText.objects.create(french_locale, 'Location d''équipement')
        text_en = LocalizedText.objects.create(english_locale, 'Equipment rental')
        GLAccount.objects.create(9400,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Taxes et permis')
        text_en = LocalizedText.objects.create(english_locale, 'taxes and permits')
        GLAccount.objects.create(5000,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Assurances')
        text_en = LocalizedText.objects.create(english_locale, 'insurances')
        GLAccount.objects.create(5100,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Électricité')
        text_en = LocalizedText.objects.create(english_locale, 'electricity')
        GLAccount.objects.create(5300,category=category_expenses,user=user).name.add(text_fr,text_en)


        text_fr = LocalizedText.objects.create(french_locale, 'Chauffage')
        text_fr = LocalizedText.objects.create(english_locale, 'heating')
        GLAccount.objects.create(5400,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Entretien et réparations')
        text_fr = LocalizedText.objects.create(english_locale, 'Maintenance and repairs')
        GLAccount.objects.create(6000,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Travaux plan de sauvetage')
        text_fr = LocalizedText.objects.create(english_locale, 'Rescue plan')
        GLAccount.objects.create(6005,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Fournitures utilisées')
        text_en = LocalizedText.objects.create(english_locale, 'Supplies used')
        GLAccount.objects.create(6010,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Entretien du terrain et des bâtiments')
        text_en = LocalizedText.objects.create(english_locale, 'Maintenance of land and buildings')
        GLAccount.objects.create(6020,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Entretien des logements')
        text_en = LocalizedText.objects.create(english_locale, 'Housing maintenance')
        GLAccount.objects.create(6030,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Entretien des ascenseurs')
        text_en = LocalizedText.objects.create(english_locale, 'Maintenance of elevators')
        GLAccount.objects.create(6040,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Entretien des équipements')
        text_en = LocalizedText.objects.create(english_locale, 'Maintenance of equipment')
        GLAccount.objects.create(6050,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Autres (si aucun poste existant)')
        text_en = LocalizedText.objects.create(english_locale, 'Other (if no existing position)')
        GLAccount.objects.create(6099,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Conciergerie')
        text_en = LocalizedText.objects.create(english_locale, 'Janitor')
        GLAccount.objects.create(6200,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Salaires')
        text_en = LocalizedText.objects.create(english_locale, 'Wages')
        GLAccount.objects.create(6210,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Avantages sociaux')
        text_en = LocalizedText.objects.create(english_locale, 'Social advantages')
        GLAccount.objects.create(6215,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Sous-traitance')
        text_en = LocalizedText.objects.create(english_locale, 'Subcontracting')
        GLAccount.objects.create(6220,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Autres (si aucun poste existant)')
        text_en = LocalizedText.objects.create(english_locale, 'Other (if no existing position)')
        GLAccount.objects.create(6299,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Frais d''administration')
        text_en = LocalizedText.objects.create(english_locale, 'Administration fees')
        GLAccount.objects.create(7000,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Salaires')
        text_en = LocalizedText.objects.create(english_locale, 'Wages')
        GLAccount.objects.create(7010,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Avantages sociaux')
        text_en = LocalizedText.objects.create(english_locale, 'Social advantages')
        GLAccount.objects.create(7012,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Honoraires de tenue de livres')
        text_en = LocalizedText.objects.create(english_locale, 'Bookkeeping fees')
        GLAccount.objects.create(7015,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Honoraires de gestion')
        text_en = LocalizedText.objects.create(english_locale, 'Management fees')
        GLAccount.objects.create(7020,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Honoraire de secrétariat')
        text_en = LocalizedText.objects.create(english_locale, 'Secretariat fees')
        GLAccount.objects.create(7025,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Papeterie et fournitures de bureau')
        text_en = LocalizedText.objects.create(english_locale, 'Stationery and office supplies')
        GLAccount.objects.create(7030,category=category_expenses,user=user).name.add(text_fr,text_en)


        text_fr = LocalizedText.objects.create(french_locale, 'Télécommunications')
        text_en = LocalizedText.objects.create(english_locale, 'Telecommunications')
        GLAccount.objects.create(7040,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Frais de réunion')
        text_en = LocalizedText.objects.create(english_locale, 'meeting fees')
        GLAccount.objects.create(7050,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Frais de gardiennage')
        text_en = LocalizedText.objects.create(english_locale, 'Babysitting fees')
        GLAccount.objects.create(7055,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Associations et cotisations')
        text_en = LocalizedText.objects.create(english_locale, 'Associations and contributions')
        GLAccount.objects.create(7060,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Frais bancaires')
        text_en = LocalizedText.objects.create(english_locale, 'Bank charges')
        GLAccount.objects.create(7070,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Intérêts des fournisseurs')
        text_en = LocalizedText.objects.create(english_locale, 'Interests of suppliers')
        GLAccount.objects.create(7075,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Intérêts - autres emprunts')
        text_en = LocalizedText.objects.create(english_locale, 'Interest - other loans')
        GLAccount.objects.create(7085,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(
            french_locale,
            'Formation')
        text_en = LocalizedText.objects.create(
            english_locale,
            'Training')
        GLAccount.objects.create(
            7087,
            category=category_expenses,
            user=user).name.add(text_fr, text_en)

        text_fr = LocalizedText.objects.create(
            french_locale,
            'Frais de représentation')
        text_en = LocalizedText.objects.create(
            english_locale,
            'representation fees')
        GLAccount.objects.create(7090,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Autres (si aucun poste existant)')
        text_en = LocalizedText.objects.create(english_locale, 'Other (if no existing position)')
        GLAccount.objects.create(7099,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Intérêts cumulés sur le prêt de sauvetage')
        text_en = LocalizedText.objects.create(english_locale, 'Accumulated interest on the rescue loan')
        GLAccount.objects.create(7400,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Intérêts sur les dettes à long terme')
        text_en = LocalizedText.objects.create(english_locale, 'Interest on long-term debt')
        GLAccount.objects.create(7500,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Hypothèque 1er rang')
        text_en = LocalizedText.objects.create(english_locale, 'Mortgage 1st row')
        GLAccount.objects.create(7505,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Hypothèque 2e rang')
        text_en = LocalizedText.objects.create(english_locale, 'Mortgage 2nd place')
        GLAccount.objects.create(7510,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Prêt de sauvetage - FSFCH')
        text_en = LocalizedText.objects.create(english_locale, 'Rescue loan - FSFCH')
        GLAccount.objects.create(7512,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Prêt de sauvetage - FAH')
        text_en = LocalizedText.objects.create(english_locale, 'Rescue loan - FAH')
        GLAccount.objects.create(7515,category=category_expenses,user=user).name.add(text_fr,text_en)


        text_fr = LocalizedText.objects.create(french_locale, 'Amortissement des immobilisations corporelles')
        text_en = LocalizedText.objects.create(english_locale, 'Amortization of capital assets')
        GLAccount.objects.create(7600,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Bâtiment')
        text_en = LocalizedText.objects.create(english_locale, 'Building')
        GLAccount.objects.create(7605,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Biens meubles et équipements')
        text_en = LocalizedText.objects.create(english_locale, 'Movable property and equipment')
        GLAccount.objects.create(7610,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Autres immobilisations')
        text_en = LocalizedText.objects.create(english_locale, 'Other properties')
        GLAccount.objects.create(7615,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Créances douteuses et irrécouvrables')
        text_en = LocalizedText.objects.create(english_locale, 'Bad and doubtful debts')
        GLAccount.objects.create(7800,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Honoraires professionnels')
        text_en = LocalizedText.objects.create(english_locale, 'Professional fees')
        GLAccount.objects.create(8000,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Honoraires d''audit')
        text_en = LocalizedText.objects.create(english_locale, 'Audit fees')
        GLAccount.objects.create(8010,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Honoraires légaux')
        text_en = LocalizedText.objects.create(english_locale, 'Legal fees')
        GLAccount.objects.create(8020,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Honoraires autres')
        text_en = LocalizedText.objects.create(english_locale, 'Other fees')
        GLAccount.objects.create(8030,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Publicité, frais de location et de recouvrement')
        text_en = LocalizedText.objects.create(english_locale, 'Advertising, rental fees and collection')
        GLAccount.objects.create(8200,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Publicité')
        text_en = LocalizedText.objects.create(english_locale, 'Advertissement')
        GLAccount.objects.create(8210,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Frais liés à la location et au recouvrement')
        text_en = LocalizedText.objects.create(english_locale, 'Costs related to the lease and recovery')
        GLAccount.objects.create(8220,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Autres (si aucun poste existant)')
        text_en = LocalizedText.objects.create(english_locale, 'Other (if no existing position)')
        GLAccount.objects.create(8299,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Déneigement et entretien du terrain')
        text_en = LocalizedText.objects.create(english_locale, 'Snow removal and grounds maintenance')
        GLAccount.objects.create(8400,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Enlèvement des ordures')
        text_en = LocalizedText.objects.create(english_locale, 'Garbage collection')
        GLAccount.objects.create(8500,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Autres charges')
        text_en = LocalizedText.objects.create(english_locale, 'Other expenses')
        GLAccount.objects.create(9000,category=category_expenses,user=user).name.add(text_fr,text_en)
        
        text_fr = LocalizedText.objects.create(french_locale, 'Activités sociales')
        text_en = LocalizedText.objects.create(english_locale, 'Social activities')
        GLAccount.objects.create(9100,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Frais de déplacement')
        text_en = LocalizedText.objects.create(english_locale, 'Travelling expenses')
        GLAccount.objects.create(9200,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Frais communs')
        text_en = LocalizedText.objects.create(english_locale, 'Common cost')
        GLAccount.objects.create(9300,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Loyer et bail emphytéotique')
        text_en = LocalizedText.objects.create(english_locale, 'Rent and emphyteotique lease')
        GLAccount.objects.create(9500,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Perte sur disposition d''immobilisations')
        text_en = LocalizedText.objects.create(english_locale, 'Loss on disposal of fixed assets')
        GLAccount.objects.create(9660,category=category_expenses,user=user).name.add(text_fr,text_en)

        text_fr = LocalizedText.objects.create(french_locale, 'Autres (si aucun poste existant)')
        text_en = LocalizedText.objects.create(english_locale, 'Other (if no existing position)')
        GLAccount.objects.create(9670,category=category_expenses,user=user).name.add(text_fr,text_en)

        # Settings
        Setting(
            ap_default_gl_account=ap_default_gl_account,
            ap_default_pay_over_gl_account=default_over_payment_ap,
            ar_default_gl_account=default_ar_account,
            ar_member_default_pay_over_gl_account=ap_default_gl_account,
            ar_member_default_asset_gl_account=default_ar_account,
            ar_rent_default_pay_over_gl_account=ap_default_gl_account,
            ar_rent_default_asset_gl_account=ar_rent_default_gl_account,
            ar_client_default_pay_over_gl_account=ap_default_gl_account,
            ar_client_default_asset_gl_account=ar_client_default_asset_gl_account,
            ).save()
        # coop information
        Coop.objects.create(
            tenant_id=tenant.id,
            name=coop_dict['name'],
            civil_number=coop_dict['civil_number'],
            street=coop_dict['street'],
            city=coop_dict['city'],
            postal_code=coop_dict['postal_code'],
            user=user)
        # master accounts
        GLAccount.objects.filter(
            code__in=[
                1300,
                1500,
                1600,
                1800,
                2100,
                3500,
                4000,
                4900,
                6000,
                6200,
                7000,
                7500,
                7600,
                8200,
                9000]
            ).update(is_master=True)
        GLAccount.objects.filter(
            code__range=(1301, 1399)).\
            update(master_account=GLAccount.objects.get(code=1300))
        GLAccount.objects.filter(
            code__range=(1501, 1599)).\
            update(master_account=GLAccount.objects.get(code=1500))
        GLAccount.objects.filter(
            code__range=(1801, 1899)).\
            update(master_account=GLAccount.objects.get(code=1800))
        GLAccount.objects.filter(
            code__range=(2101, 2199)).\
            update(master_account=GLAccount.objects.get(code=2100))
        GLAccount.objects.filter(
            code__range=(3501, 3505)).\
            update(master_account=GLAccount.objects.get(code=3500))
        GLAccount.objects.filter(
            code__range=(4001, 4050)).\
            update(master_account=GLAccount.objects.get(code=4000))
        GLAccount.objects.filter(
            code__range=(4901, 4999)).\
            update(master_account=GLAccount.objects.get(code=4900))
        GLAccount.objects.filter(
            code__range=(6001, 6099)).\
            update(master_account=GLAccount.objects.get(code=6000))
        GLAccount.objects.filter(
            code__range=(6201, 6299)).\
            update(master_account=GLAccount.objects.get(code=6200))
        GLAccount.objects.filter(
            code__range=(7001, 7099)).\
            update(master_account=GLAccount.objects.get(code=7000))
        GLAccount.objects.filter(
            code__range=(7501, 7515)).\
            update(master_account=GLAccount.objects.get(code=7500))
        GLAccount.objects.filter(
            code__range=(7601, 7615)).\
            update(master_account=GLAccount.objects.get(code=7600))
        GLAccount.objects.filter(
            code__range=(8001, 8099)).\
            update(master_account=GLAccount.objects.get(code=8000))
        GLAccount.objects.filter(
            code__range=(8201, 8299)).\
            update(master_account=GLAccount.objects.get(code=8200))
        GLAccount.objects.filter(
            code__range=(9001, 9700)).\
            update(master_account=GLAccount.objects.get(code=9000))

        