from django import forms
from coopmsapi.clients.models import ClientUsersList

class RegistrationForm(forms.Form):
    first_name = forms.CharField(max_length=32)
    last_name = forms.CharField(max_length=32)
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput())
    retype_password = forms.CharField(widget=forms.PasswordInput())
    coop_name = forms.CharField(max_length=128)
    civil_number = forms.CharField(max_length=32)
    street = forms.CharField(max_length=128)
    postal_code = forms.CharField(max_length=6)
    city = forms.CharField(max_length=128)
    
    def clean_password2(self):
        if 'password' in self.cleaned_data:
            password1 = self.cleaned_data['password']
            password2 = self.cleaned_data['retype_password']
            if password1 == password2:
                return password2
        raise forms.ValidationError('Passwords do not match.')
    
    def clean_username(self):
        #username = self.cleaned_data['username']
        #if not re.search(r'^\w+$', username):
         #   raise forms.ValidationError('Username can only contain' +
          #                      'alphanumeric characters and the underscore.')
        if ClientUsersList.objects.filter(
            email=self.cleaned_data['email'],
            is_active=True).count() > 0: 
            raise forms.ValidationError('Username is already taken.')
