from django.utils import timezone
import hashlib, binascii
import string
import random


def random_id(size=16, chars=string.ascii_lowercase):
    return ''.join(random.SystemRandom().choice(chars) for _ in range(size))


def random_token_generator(hash_type, request_str):
    token_seed = str(timezone.now()) + request_str
    if hash_type == 'sha256':

        dk = hashlib.pbkdf2_hmac(
            hash_type,
            bytes(token_seed, 'utf-8'),
            b'C8003NYUCF6JPRMKES50',
            101110
            )
    else:
        dk = hashlib.pbkdf2_hmac(
            'sha',
            bytes(token_seed, 'utf-8'),
            b'C8003NYUCF6JPRMKES50',
            101110
            )
    return binascii.hexlify(dk).decode("utf-8")
