from django.db import models
from django.contrib.auth import get_user_model
from tenant_schemas.utils import tenant_context
from django.utils import timezone
from tenant_schemas.models import TenantMixin
from coopmsapi.tools.security import random_id
from coopmsapi.clients.initializers import Initializer
from sequences import get_next_value


class ClientManager(models.Manager):

    def create(self, client_dict):
        name_str = random_id()
        new_tenant = Client(
            name=client_dict['name'],
            domain_url=name_str,
            schema_name=name_str,
            account_no=get_next_value(
                'tenant_account',
                initial_value=50000),
            paid_until=client_dict.get('paid_until', timezone.now()),
            is_active=client_dict.get('is_active', False),
            on_trial=client_dict.get('on_trial', True))

        new_tenant.save()

        tenant_user_ref = ClientUsersList(
            email=client_dict['superuser']['email'],
            client=new_tenant,
            is_active=True
            )
        tenant_user_ref.save()
        
        #add name to coop
        client_dict['coop']['name'] = client_dict['name']

        with tenant_context(new_tenant):
            client_dict['superuser']['is_superuser'] = True
            new_user = get_user_model().objects.create(
                user_dict=client_dict['superuser'])
            Initializer.launch(
                tenant=new_tenant,
                user=new_user,
                coop_dict=client_dict['coop'])

        return new_tenant

    def update(self, client, client_dict):
        client.paid_until = client_dict.get('client_dict', client.paid_until)
        client.is_active = client_dict.get('is_active', client.is_active)
        client.on_trial = client_dict.get('on_trial', client.on_trial)
        return client_save()



class Client(TenantMixin):
    name = models.CharField(max_length=128)
    paid_until = models.DateTimeField(default=timezone.now)
    on_trial = models.BooleanField(default=True)
    is_active = models.BooleanField(default=True)
    account_no = models.IntegerField()
    created_on = models.DateTimeField(auto_now_add=True)
    objects = ClientManager()

    #def save(self, *args, **kwargs):


class ClientUsersList(models.Model):
    email = models.CharField(max_length=128)
    client = models.ForeignKey('Client', related_name='%(class)s_client')
    is_active = models.BooleanField(default=True)
    password_reset_request = models.BooleanField(default=False)
    password_reset_token = models.CharField(
                        max_length=256,
                        null=True)
    token_expires = models.DateTimeField(null=True)
