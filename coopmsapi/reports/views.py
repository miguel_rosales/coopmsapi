import os

from io import BytesIO
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4, cm

from django.http import HttpResponse


def report(request):

    if request.method == 'GET':

        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=test.pdf'
        buffer = BytesIO()
        c = canvas.Canvas(buffer, pagesize=A4)

        # Report Header
        c.setLineWidth(.3)
        c.setFont('Helvetica', 22)
        c.drawString(30, 750, 'Budget')
        # c.setFont('Helvetica', 12)
        # c.drawString(30, 700, 'Report')

        c.setFont('Helvetica-Bold', 12)
        c.drawString(480, 750, '01/07/2016')
        c.line(460, 747, 560, 747)

        # FIRST BLUE LINE
        c.setFillColorRGB(0.0625, 0.37890625, 0.6953125) #choose fill colour
        c.rect(1*cm,24*cm,19*cm,1*cm, fill=1, stroke=False) #draw rectangle

        # WRITING ON TOP OF BLUE LINE
        c.setFillColorRGB(255, 255, 255)
        c.setFont('Helvetica', 22)
        c.drawString(1.2*cm, 24.3*cm, 'REVENUS')


        c.showPage()

        c.save()
        pdf = buffer.getvalue()
        response.write(pdf)
        return response

