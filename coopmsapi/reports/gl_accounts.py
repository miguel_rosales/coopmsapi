from io import BytesIO
# django imports
from django.http import Http404
from django.http import HttpResponse
# reportlab imports
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus import Paragraph, Table, TableStyle
from reportlab.lib.enums import TA_CENTER
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4, cm
from reportlab.lib import colors
# coopmsapi models
from coopmsapi.accounting.models import GLAccount


def gl_account(request):

    if request.method == 'GET':

        locale = request.GET.get('locale', 'fr')
        if locale not in ('fr', 'en'):
            raise Http404

        if locale == 'fr':
            DOC_HEADER_TITLE = 'COMPTES DU GRAND LIVRE'
            headers = ['NOM', 'CODE']
        else:
            DOC_HEADER_TITLE = 'GENERAL LEDGER ACCOUNTS'
            headers = ['NAME', 'CODE']

        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=test.pdf'
        buffer = BytesIO()
        c = canvas.Canvas(buffer, pagesize=A4)

        # data
        queryset = GLAccount.objects.filter()
        page_count = get_page_count(queryset, 35)
        # end data
        i=0
        styles = getSampleStyleSheet()
        while i < page_count:
            # !----PAGE START----!
            gl_account_list = get_queryset_page(queryset, 35, i)        
            c = add_header(_canvas=c, text=DOC_HEADER_TITLE)
            c = add_page_number(
                _canvas=c,
                current_page=i+1,
                page_count=page_count,
                locale=locale)
            data = add_table_header(headers, styles)
            # TABLE CONTENT STYLE
            styleN = styles['BodyText']
            styleN.alignment = TA_CENTER
            styleN.fontSize = 8
            #loading data
            high = 720
            for gl_acc in gl_account_list:
                this_gl_account = [gl_acc.code, gl_acc.get_name(locale)]
                data.append(this_gl_account)
                high -= 18
        
            # TABLE SIZE
            width, height = A4
            table = Table(data, colWidths=[1.9*cm, 13*cm])
            table.setStyle(TableStyle([
                ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                ('BOX', (0, 0), (-1, -1), 0.25, colors.black)            
                ]))

            table.wrapOn(c, width, height)
            table.drawOn(c, 120, high)
            
            # save page(switch page)
            c.showPage()
            # !----PAGE END----!
            i = i + 1

        #save PDF
        c.save()
        pdf = buffer.getvalue()
        response.write(pdf)
        return response


def add_header(_canvas, text):
    # FIRST BLUE LINE
    _canvas.setFillColorRGB(0.0625, 0.37890625, 0.6953125) # choose fill colour
    _canvas.rect(1*cm,27*cm,19*cm,1*cm, fill=1, stroke=False) # draw rectangle

    # WRITING ON TOP OF BLUE LINE
    _canvas.setFillColorRGB(255, 255, 255)
    _canvas.setFont('Helvetica', 22)
    _canvas.drawString(1.2*cm, 27.25*cm, text)
    return _canvas

def add_page_number(_canvas, current_page=None, page_count=None, locale=None):
    if locale is None:
        locale = 'fr'
    if locale == 'fr':
        separator_keyword = ' de '
    else:
        separator_keyword = ' of '
    if current_page is None:
        current_page = 1
    if page_count is None:
        page_count = current_page
    page_tag = str(current_page) + separator_keyword + str(page_count)
    _canvas.setFillColorRGB(0, 0, 0)
    _canvas.setFont('Helvetica', 10)
    _canvas.drawString(17.5*cm, 1.5*cm, page_tag)
    return _canvas

# def get_page(items_per_page, ):

def add_table_header(headers, styles):
    styleBH = get_table_header_styles(styles)
    data = []
    element_list = []
    for header in reversed(headers):
        element_list.append(Paragraph(header, styleBH))

    data.append(element_list)

    return data

def get_table_header_styles(styles):
    styleBH = styles['Normal']
    styleBH.alignment = TA_CENTER
    styleBH.fontSize = 10
    return styleBH

def get_queryset_page(queryset, items_per_page, page):
    total = queryset.count()
    x = page*items_per_page
    if x > total:
        return None
    if x + items_per_page <= total:
        y = x + items_per_page
    else:
        y = total
    return queryset[x:y]

def get_page_count(queryset, items_per_page):
    total = queryset.count()
    if total % items_per_page == 0:
        return int(total/items_per_page)
    else:
        return int(total/items_per_page) + 1

