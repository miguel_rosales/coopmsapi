import os

from io import BytesIO
from reportlab.pdfgen import canvas
from django.http import Http404
from reportlab.lib.pagesizes import A4, cm
from reportlab.pdfbase.pdfmetrics import stringWidth
from django.http import HttpResponse
# models
from coopmsapi.receivables.models import Invoice, InvoiceItem, Payment


def rent_pdf(request):

    if request.method == 'GET':

        pk = request.GET.get('invoice')

        try:
            invoice = Invoice.objects.get(id=pk)
        except Invoice.DoesNotExist:
            raise Http404

        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=test.pdf'
        buffer = BytesIO()
        c = canvas.Canvas(buffer, pagesize=A4)

        # Report Header
        #c.setLineWidth(.3)
        #c.setFont('Helvetica', 22)
        #c.drawString(30, 750, 'Budget')
        # c.setFont('Helvetica', 12)
        # c.drawString(30, 700, 'Report')

        c.setFont('Helvetica-Bold', 12)
        c.drawString(480, 750, str(invoice.date.date()))
        c.line(460, 747, 560, 747)
        
        # FIRST BLUE LINE
        c.setFillColorRGB(0.0625, 0.37890625, 0.6953125) #choose fill colour
        c.rect(1*cm,25*cm,19*cm,1*cm, fill=1, stroke=False) #draw rectangle
        
        # WRITING ON TOP OF BLUE LINE
        c.setFillColorRGB(255, 255, 255)
        c.setFont('Helvetica', 22)
        c.drawString(1.2*cm, 25.2*cm, 'RENT')

        #__****INVOICE HEADER****__

        #DUE_DATE
        DUE_DATE_LABEL= 'DUE DATE: '
        c.setFillColorRGB(0, 0, 0)
        c.setFont('Helvetica-Bold', 12)
        c.drawString(2*cm, 24*cm, DUE_DATE_LABEL)

        text_width = stringWidth(DUE_DATE_LABEL, 'Helvetica-Bold', 12) 

        DUE_DATE_DATA= str(invoice.date.date())
        c.setFillColorRGB(0, 0, 0)
        c.setFont('Helvetica', 12)
        c.drawString(2*cm + text_width + 1, 24*cm, DUE_DATE_DATA)

        #RENT TOTAL AMOUNT
        TOTAL_AMOUNT= 'TOTAL AMOUNT: ' + str(invoice.amount)
        c.setFillColorRGB(0, 0, 0)
        c.setFont('Helvetica', 12)
        c.drawString(2*cm, 23*cm, TOTAL_AMOUNT)

        #USER
        MEMBER= 'MEMBER: ' + \
            str(invoice.user.first_name + ' ' + invoice.user.last_name)
        c.setFillColorRGB(0, 0, 0)
        c.setFont('Helvetica', 12)
        c.drawString(2*cm, 22*cm, MEMBER)


        c.showPage()

        c.save()
        pdf = buffer.getvalue()
        response.write(pdf)
        return response

