from django.conf.urls import url, include
from .views import report
from .rent_pdf import rent_pdf
from .gl_accounts import gl_account

urlpatterns = [
    url(r'budget/', report, name='report'),
    url(r'rent/$', rent_pdf, name='rent'),
    url(r'gl_account/$', gl_account, name='gl_account')
]
