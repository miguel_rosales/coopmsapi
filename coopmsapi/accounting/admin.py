from django.contrib import admin
from solo.admin import SingletonModelAdmin
from .models import GLAccountCategory, GLAccount, GLEntry, Transaction, Setting

admin.site.register(GLAccountCategory)
admin.site.register(GLAccount)
admin.site.register(GLEntry)
admin.site.register(Transaction)
admin.site.register(Setting, SingletonModelAdmin)
