from django.conf.urls import url, include
from rest_framework_nested import routers
from coopmsapi.accounting.views import (
    GLAccountViewSet,
    GLAccountCategoryView,
    BankViewSet,
    BankAccountViewSet,
    TransactionView,
    TransactionDetailView,
    GLEntryView,
    GLEntryDetailView)

bank_router = routers.SimpleRouter()
bank_router.register(
    r'banks',
    BankViewSet,
    base_name='bank')

bank_account_router = routers.SimpleRouter()
bank_account_router.register(
    r'bank_accounts',
    BankAccountViewSet,
    base_name='bank_account')

gl_account_router = routers.SimpleRouter()
gl_account_router.register(
    r'accounts',
    GLAccountViewSet,
    base_name='account')

urlpatterns = [
    url(r'^', include(bank_router.urls)),
    url(r'^', include(bank_account_router.urls)),
    url(r'^', include(gl_account_router.urls)),
    url(r'^account_categories/$',
        GLAccountCategoryView.as_view(),
        name="accounts_categories"),
    url(r'^transactions/(?P<pk>[0-9]+)/$',
        TransactionDetailView.as_view(),
        name="transaction"),
    url(r'^transactions/$', TransactionView.as_view(), name="transactions"),
    url(r'^gl_entries/(?P<pk>[0-9]+)/$',
        GLEntryDetailView.as_view(),
        name="gl_entry"),
    url(r'^gl_entries/$', GLEntryView.as_view(), name="gl_entries"),
]
