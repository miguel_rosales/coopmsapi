# -*- coding: utf-8 -*-
from django.utils import timezone
from rest_framework.response import Response
from rest_framework.views import APIView
from django.contrib.auth import get_user_model
from rest_framework import status, permissions
from rest_framework.parsers import MultiPartParser
from rest_framework import generics, viewsets
from oauth2_provider.models import AccessToken
# filters
from rest_framework import filters
# transaction
from django.db import transaction, IntegrityError
# serializers
from .serializers import (
    GLAccountInsertSerializer,
    GLAccountDisplaySerializer,
    GLAccountDisplayShortSerializer,
    GLAccountCategorySerializer,
    TransactionSerializer,
    GLEntrySerializer)
# coopms
from .models import (
    GLAccount,
    GLAccountCategory,
    GLEntry,
    Transaction)

from .banking_models import(
    BankAccount,
    Bank)

from .banking_serializers import(
    BankSerializer,
    BankAccountSerializer)

"""
TRANSACTION VIEW
"""
class TransactionView(generics.ListAPIView):
    """
    Transaction visualization of the data that model contains.
    """
    permission_classes = [permissions.IsAuthenticated]
    queryset = Transaction.objects.all()
    serializer_class = TransactionSerializer


class TransactionDetailView(generics.RetrieveAPIView):
    """
    Transaction detail visualization of the data that model contains.
    """
    lookup_field = 'pk'
    queryset = Transaction.objects.all()
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = TransactionSerializer


"""
GL ENTRY VIEW
"""

class GLEntryView(generics.ListAPIView):
    """
    Gl entry visualization of the data that model contains.
    """
    permission_classes = [permissions.IsAuthenticated]
    queryset = GLEntry.objects.all()
    serializer_class = GLEntrySerializer


class GLEntryDetailView(generics.RetrieveAPIView):
    """
    Gl entry detail visualization of the data that model contains.
    """
    permission_classes = [permissions.IsAuthenticated]
    lookup_field = 'pk'
    queryset = GLEntry.objects.all()
    serializer_class = GLEntrySerializer


class GLAccountCategoryView(generics.ListCreateAPIView):
    """
    GL account category vizualization of the data that model contains.
    """
    permission_classes = [permissions.IsAuthenticated]
    queryset = GLAccountCategory.objects.all()
    serializer_class = GLAccountCategorySerializer
    filter_backend = filters.DjangoFilterBackend
    # filter_fields = ('name_fr', 'name_en')

# deprecated


class GLAccountView(generics.ListCreateAPIView):
    """
    Gl account visualization of the data that model contains.
    """
    permission_classes = [permissions.IsAuthenticated]
    queryset = GLAccount.objects.all()
    serializer_class = GLAccountDisplaySerializer
    filter_backend = filters.DjangoFilterBackend
    filter_fields = ('code', 'active', 'category')

    def perform_create(self, serializer):
        serializer.save(modified_by=self.request.user,
                        created_by=self.request.user)

# deprecated


class GLAccountDetailView(generics.RetrieveUpdateDestroyAPIView):
    """
    Gl account detail visualization of the data that model contains.
    """
    lookup_field = 'pk'
    queryset = GLAccount.objects.all()
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = GLAccountDisplaySerializer


"""
GL ACCOUNT CATEGORY viewset
"""

# pass

"""
GL ACCOUNT viewset
"""


class GLAccountViewSet(viewsets.ModelViewSet):
    queryset = GLAccount.objects.all()
    serializer_class = GLAccountDisplaySerializer

    def create(self, request):
        try:
            serializer = GLAccountInsertSerializer(data=request.data)
            if serializer.is_valid():
                serializer.validated_data['by_user'] = request.user
                gl_account = serializer.create(data=serializer.validated_data)
                return Response(
                    data=GLAccountDisplaySerializer(gl_account).data,
                    status=status.HTTP_201_CREATED)
            else:
                return Response(
                    data=serializer.errors,
                    status=status.HTTP_400_BAD_REQUEST
                    )
        except IntegrityError as e:
            return Response(
                data=str(e),
                status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk):
        serializer = GLAccountInsertSerializer(data=request.data)
        try:
            gl_account = GLAccount.objects.get(id=pk)
            if serializer.is_valid():
                serializer.validated_data['by_user'] = request.user
                new_gl_account = serializer.update(
                    obj=gl_account,
                    data=serializer.validated_data)
                return Response(
                    data=GLAccountDisplaySerializer(new_gl_account).data,
                    status=status.HTTP_200_OK)
            else:
                return Response(
                    data=serializer.errors,
                    status=status.HTTP_400_BAD_REQUEST)


        except GLAccount.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def get_serializer_class(self):
        if self.action == 'list':
            return GLAccountDisplayShortSerializer
        elif self.action == 'retrieve':
            return GLAccountDisplaySerializer
        else:
            return GLAccountInsertSerializer


"""
BANK viewset
"""


class BankViewSet(viewsets.ModelViewSet):

    queryset = Bank.objects.all()
    serializer_class = BankSerializer

    def get_serializer_context(self):
        if self.action == 'retrieve' or\
                self.action == 'update' or\
                self.action == 'partial_update' or\
                self.action == 'destroy':
            return {'request': self.request, 'pk_present': True}
        else:
            return {'request': self.request, 'pk_present': False}



"""
BANK ACCOUNT viewset
"""


class BankAccountViewSet(viewsets.ModelViewSet):

    queryset = BankAccount.objects.filter(is_inactive=False)
    serializer_class = BankAccountSerializer

    def get_serializer_context(self):
        if self.action == 'retrieve' or\
                self.action == 'update' or\
                self.action == 'partial_update' or\
                self.action == 'destroy':
            return {'request': self.request, 'pk_present': True}
        else:
            return {'request': self.request, 'pk_present': False}
