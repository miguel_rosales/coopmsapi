from django.utils import timezone
from django.db import models, IntegrityError
from solo.models import SingletonModel
from coopmsapi.settings import AUTH_USER_MODEL
from coopmsapi.locale.models import LocalizedText
from coopmsapi.common.abstract_models import AbstractAuditableModel

'''
QUERYSET
'''

class GLAccountQueryset(models.query.QuerySet):
    def active(self):
        return self.filter(is_active=True)

    def active_budget_accounts(self):
        return self.filter(is_active=True,category__id__in=[4,5])

'''
END QUERY SET
'''

class GLAccountManager(models.Manager):

    def get_queryset(self):
        return GLAccountQueryset(self.model, using=self._db)

    def get_active_budget_accounts(self):
        return self.get_queryset().active_budget_accounts()

    def createGLAccountCategory(self, name):
        current_glaccountcategory = GLAccountCategory(
            name=name)
        current_glaccountcategory.save()
        return current_glaccountcategory


    def create(self, code, user, category):
        gl_account = GLAccount(
            is_master=False,
            code=code,
            master_account=None,
            is_active=True,
            category=category,
            created_by=user,
            modified_by=user)
        gl_account.save()

        return gl_account

    def create_serialized(self, gl_account_dict):

        gl_account = GLAccount(
            is_master=gl_account_dict.get('is_master', False),
            code=gl_account_dict['code'],
            master_account=gl_account_dict.get('master_account'),
            is_active=gl_account_dict.get('dict', True),
            category=gl_account_dict['category'],
            created_by=gl_account_dict['by_user'],
            modified_by=gl_account_dict['by_user'])
        gl_account.save()

        for localized_text in gl_account_dict['name']:
            name_text = LocalizedText.objects.create(
                locale=localized_text['locale'],
                text=localized_text['text'])
            gl_account.name.add(name_text)


        description = gl_account_dict.get('description')

        if description is not None:
            for localized_text in description:
                desc_text = localized_text.objects.create(
                    locale=localized_text['locale'],
                    text=localized_text['text'])
                gl_account.name.add(desc_text)

        return gl_account

    def update(self, gl_account, gl_account_dict):

        if gl_account_dict.get('is_master') is not None:
            gl_account.is_master = gl_account_dict.get('is_master')

        gl_account.code = gl_account_dict['code']
        gl_account.master_account = gl_account_dict.get('master_account')
        gl_account.category = gl_account_dict['category']
        gl_account_dict.modified_by = gl_account_dict['by_user']
        gl_account.save()

        gl_account.name.clear()
        gl_account.description.clear()

        for localized_text in gl_account_dict['name']:
            name_text = LocalizedText.objects.create(
                locale=localized_text['locale'],
                text=localized_text['text'])
            gl_account.name.add(name_text)

        description = gl_account_dict.get('description')
        if description is not None:
            for localized_text in gl_account_dict['description']:
                desc_text = LocalizedText.objects.create(
                    locale=localized_text['locale'],
                    text=localized_text['text'])
                gl_account.name.add(desc_text)

        return gl_account

    def delete(self, gl_account, user):
        gl_account.is_active = False
        gl_account.modified_by = user
        gl_account.save()
        return True


class GLEntryManager(models.Manager):

    # VERIFICATIONS ARE DONE ONE LEVEL-UP

    def doubleEntry(
            self,
            transaction,
            date,
            first_gl_account,
            first_amount,
            second_gl_account,
            second_amount):

        first_entry = GLEntry(
            gl_account=first_gl_account,
            transaction=transaction,
            amount=first_amount,
            date=date)
        first_entry.save()
        second_entry = GLEntry(
            gl_account=second_gl_account,
            transaction=transaction,
            amount=second_amount,
            date=date)
        second_entry.save()


class TransactionManager(models.Manager):

    def reverseTransaction(
            self,
            transaction_target,
            transaction,
            transaction_type,
            user,
            date=timezone.now()):

        if transaction is None:

            transaction = Transaction(
                linked_transaction=transaction_target,
                transaction_type=transaction_type,
                transaction_date=date,
                created_by=user,
                modified_by=user)

        gl_entries_list = GLEntry.objects.filter(
            transaction=transaction_target)

        for gl_entry in gl_entries_list:
            corrected_gl_entry = GLEntry(
                gl_account=gl_entry.gl_account,
                transaction=transaction,
                amount=-gl_entry.amount,
                date=date,
                correction=True)
            corrected_gl_entry.save()


class AccountingInfo(models.Model):
    first_day_start = models.IntegerField()
    fiscal_month_start = models.IntegerField()
    fiscal_month_end = models.IntegerField()
    fiscal_day_end = models.IntegerField()
    # audit
    created_by = models.ForeignKey(
        AUTH_USER_MODEL,
        related_name='%(class)s_created_by')
    created_date = models.DateTimeField(editable=False)
    modified_by = models.ForeignKey(
        AUTH_USER_MODEL,
        related_name='%(class)s_modified_by')
    modified_date = models.DateTimeField()

    def save(self, *args, **kwargs):
        if not self.id:
            self.created_date = timezone.now()
        self.modified_date = timezone.now()
        return super(AccountingInfo, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.id)


class GLAccountCategory(models.Model):
    class Meta:
        verbose_name_plural = 'GL account categories'

    name = models.ManyToManyField(LocalizedText, related_name='%(class)s_name')

    def get_name(self, locale=None):
        names = self.name.all()
        if names is None or\
                locale is None:
            return '<no translation>'
        for _name in names:
            if _name.locale == locale:
                return _name.text
        if locale == 'en':
            return '<no translation>'
        elif locale == 'fr':
            return '<pas de traduction>'
        elif locale == 'es':
            return '<no hay traduccion>'
        else:
            return '<no translation>'

    def __str__(self):
        return str(self.id) + ' ' + str(self.get_name)


class GLAccount(models.Model):
    class Meta:
        verbose_name_plural = 'GL accounts'
        ordering = ['code']

    is_master = models.BooleanField(default=False)
    master_account = models.ForeignKey(
        'GLAccount',
        related_name='%(class)s_master_code',
        null=True,
        on_delete=models.SET_NULL,
        blank=True)
    code = models.IntegerField(unique=True)
    name = models.ManyToManyField(LocalizedText, related_name='%(class)s_name')
    description = models.ManyToManyField(
        LocalizedText,
        related_name='%(class)s_description')
    category = models.ForeignKey(
        'GLAccountCategory',
        related_name='%(class)s_category',
        on_delete=models.PROTECT)
    is_active = models.BooleanField(default=True)
    # manager
    objects = GLAccountManager()
    # audit
    created_by = models.ForeignKey(
        AUTH_USER_MODEL,
        related_name='%(class)s_created_by',
        on_delete=models.PROTECT)
    created_date = models.DateTimeField(editable=False)
    modified_by = models.ForeignKey(
        AUTH_USER_MODEL,
        related_name='%(class)s_modified_by',
        on_delete=models.PROTECT)
    modified_date = models.DateTimeField()

    def get_name(self, locale=None):
        names = self.name.all()
        if names is None or\
                locale is None:
            return '<no translation>'
        for _name in names:
            if _name.locale == locale:
                return _name.text
        if locale == 'en':
            return '<no translation>'
        elif locale == 'fr':
            return '<pas de traduction>'
        elif locale == 'es':
            return '<no hay draduccion>'
        else:
            return '<no translation>'

    def get_full_name(self, locale=None):
        name = self.get_name(locale)
        category = self.category.get_name(locale)
        return name + ' - ' + category + ' - '

    def get_description(self, locale=None):
        descriptions = self.description.all()
        if descriptions is None or\
                locale is None:
            return '<no translation>'
        for _description in descriptions:
            if _description.locale == locale:
                return _description.text
        if locale == 'en':
            return '<no translation>'
        elif locale == 'fr':
            return '<pas de traduction>'
        elif locale == 'es':
            return '<no hay draduccion>'
        else:
            return '<no translation>'

    def save(self, *args, **kwargs):
        if not self.id:
            self.created_date = timezone.now()
        else:
            if self.id == self.master_account:
                self.master_account = None

        self.modified_date = timezone.now()
        return super(GLAccount, self).save(*args, **kwargs)

    def __str__(self):
        if self.is_master is True:
            return '**MASTER ACCOUNT**' +\
                str(self.id) +\
                ' ' +\
                str(self.code) +\
                ' ' + str(self.name) +\
                '**MASTER ACCOUNT**'
        else:
            return str(self.id) +\
                    ' ' +\
                    str(self.code) +\
                    ' ' +\
                    str(self.name)

    def get_classification(self):
        if self.category.id == 1:
            return 'A'
        elif self.category.id == 2:
            return 'L'
        elif self.category.id == 3:
            return 'N'
        elif self.category.id == 4:
            return 'R'
        elif self.category.id == 5:
            return 'E'
        else:
            raise Exception('category not found')

class GLEntry(models.Model):
    CREDIT = 'C'
    DEBIT = 'D'
    ENTRY_TYPES = (
        (CREDIT, 'CREDIT'),
        (DEBIT, 'DEBIT'))

    class Meta:
        verbose_name_plural = 'GL entries'

    gl_account = models.ForeignKey(
        'GLAccount',
        related_name='%(class)s_gl_account',
        on_delete=models.PROTECT)
    transaction = models.ForeignKey(
        'Transaction',
        related_name='%(class)s_transaction',
        on_delete=models.PROTECT)
    entry_type = models.CharField(max_length=1, choices=ENTRY_TYPES)
    amount = models.DecimalField(max_digits=16, decimal_places=2)
    date = models.DateField()
    correction = models.BooleanField(default=False)
    objects = GLEntryManager()
    # audit
    created_date = models.DateTimeField(editable=False)

    def is_credit(self):
        if self.entry_type == GLEntry.CREDIT:
            return True
        else:
            return False

    def is_debit(self):
        if self.entry_type == GLEntry.DEBIT:
            return True
        else:
            return False

    def __str__(self):
        return str(self.gl_account) +\
                str(self.transaction) +\
                ' ' +\
                str(self.amount) +\
                ' ' +\
                str(self.date)


class Transaction(models.Model):
    AP_CR_RPT = 'AP_CR_RPT'
    AP_CA_RPT = 'AP_CA_RPT'

    TRS_CODES = (
        (AP_CR_RPT, 'Account''s payables RECEIPT creation'),
        (AP_CA_RPT, 'Account''s payables RECEIPT cancelation'),)

    transaction_date = models.DateTimeField()
    transaction_type = models.CharField(max_length=9, choices=TRS_CODES)
    linked_transaction = models.ForeignKey(
        'Transaction',
        related_name='%(class)s_linked',
        null=True,
        blank=True)
    accounting_document = models.ManyToManyField(
        'AccountingDocument',
        related_name='%(class)s')
    objects = TransactionManager()
    created_by = models.ForeignKey(
        AUTH_USER_MODEL,
        related_name='%(class)s_created_by')
    created_date = models.DateTimeField(editable=False)
    modified_by = models.ForeignKey(
        AUTH_USER_MODEL,
        related_name='%(class)s_modified_by')
    modified_date = models.DateTimeField()

    def add_debit_entry(self, **kwargs):
        kwargs['entry_type'] = GLEntry.DEBIT
        return self.add_gl_entry(**kwargs)

    def add_credit_entry(self, **kwargs):
        kwargs['entry_type'] = GLEntry.CREDIT
        return self.add_gl_entry(**kwargs)

    def add_gl_entry(self, **kwargs):
        kwargs['transaction'] = self
        new_gl_entry = GLEntry(**kwargs)
        new_gl_entry.save()
        return new_gl_entry

    def __str__(self):
        return str(self.id) +\
                ' ' + \
                str(self.transaction_type) +\
                ' ' +\
                str(self.transaction_date)

    def save(self, *args, **kwargs):
        if not self.id:
            self.created_date = timezone.now()
        else:
            if self.id == self.linked_transaction:
                self.linked_transaction = None

        self.modified_date = timezone.now()
        return super(Transaction, self).save(*args, **kwargs)


class AccountingDocument(models.Model):
    AR_INVOICE = 'AR_INVOICE'
    AR_INVOICE_PAYMENT = 'AR_INVOICE_PAYMENT'
    AP_INVOICE = 'AP_INVOICE'
    AP_RECEIPT = 'AP_RECEIPT'
    AP_INVOICE_PAYMENT = 'AP_INVOICE_PAYMENT'

    DOCUMENT_TYPES = (
        (AR_INVOICE, 'AR_INVOICE'),
        (AR_INVOICE_PAYMENT, 'AR_INVOICE_PAYMENT'),
        (AP_RECEIPT, 'AP_RECEIPT'),
        (AP_INVOICE, 'AP_INVOICE'),
        (AP_INVOICE_PAYMENT, 'AP_INVOICE_PAYMENT'))

    document_type = models.CharField(max_length=32, choices=DOCUMENT_TYPES)
    date = models.DateField()
    is_canceled = models.BooleanField(default=False)
    created_by = models.ForeignKey(
                                AUTH_USER_MODEL,
                                related_name='%(class)s_created_by')
    created_date = models.DateTimeField(editable=False)
    modified_by = models.ForeignKey(
                                AUTH_USER_MODEL,
                                related_name='%(class)s_modified_by')
    modified_date = models.DateTimeField()

    def get_all_gl_accounts(self):
        print('self.transaction.filter()')
        print(self.transaction.filter())
        print('self.transaction.filter()')
        return GLEntry.objects.filter(
            transaction__in=self.transaction.filter())


class Setting(SingletonModel):

    ap_default_gl_account = models.ForeignKey(
                    'GLAccount',
                    related_name='%(class)s_ap_default_gl_account')
    ap_default_pay_over_gl_account = models.ForeignKey(
                    'GLAccount',
                    related_name='%(class)s_ap_default_pay_over_gl_account')
    ar_default_gl_account = models.ForeignKey(
                        'GLAccount',
                        related_name='%(class)s_ar_default_gl_account')

    ar_member_default_pay_over_gl_account = models.ForeignKey(
                GLAccount,
                related_name='%(class)s_member_default_pay_over_gl_account')
    ar_member_default_asset_gl_account = models.ForeignKey(
                GLAccount,
                related_name='%(class)s_member_default_liability_gl_account')
    ar_rent_default_pay_over_gl_account = models.ForeignKey(
                GLAccount,
                related_name='%(class)s_ar_rent_default_pay_over_gl_account')

    ar_rent_default_asset_gl_account = models.ForeignKey(
                GLAccount,
                related_name='%(class)s_ar_rent_default_asset_gl_account')
    ar_client_default_pay_over_gl_account = models.ForeignKey(
        GLAccount, related_name='%(class)s_client_default_pay_over_gl_account')
    ar_client_default_asset_gl_account = models.ForeignKey(
        GLAccount, related_name='%(class)s_client_default_asset_gl_account')
