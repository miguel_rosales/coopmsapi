from rest_framework import serializers
from django.contrib.auth import get_user_model
from coopmsapi.locale.models import Locale, LocalizedText
from coopmsapi.locale.serializers import LocalizedTextSerializer
from .models import (
    GLAccount,
    GLAccountCategory,
    GLEntry,
    Transaction)

"""
TRANSACTION serializers
"""


class TransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transaction
        fields = (
            'id',
            'transaction_date',
            'transaction_type',
            'linked_transaction',
            'created_by',
            'modified_by',
            'created_date',
            'modified_date')


"""
GLENTRY serializers
"""


class GLEntrySerializer(serializers.ModelSerializer):
    class Meta:
        model = GLEntry
        fields = (
            'id',
            'gl_account',
            'transaction',
            'amount',
            'date',
            'correction',
            'created_date')

"""
GLACCOUNT CATEGORY serializers
"""


class GLAccountCategorySerializer(serializers.ModelSerializer):
    name = LocalizedTextSerializer(many=True, required=False)
    class Meta:
        model = GLAccountCategory
        fields = ('id', 'name')


class GLAccountCategoryShortSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()

    class Meta:
        model = GLAccountCategory
        fields = ('id', 'name')

    def get_name(self, obj):
        return obj.get_name(
            self.context['request'].query_params.get('locale', None))


"""
GLACCOUNT serializers
"""


class GLAccountInsertSerializer(serializers.ModelSerializer):

    category = serializers.PrimaryKeyRelatedField(
        queryset=GLAccountCategory.objects.all(),
        write_only=True,
        required=True)
    name = LocalizedTextSerializer(many=True)
    description = LocalizedTextSerializer(many=True, required=False)

    class Meta:
        model = GLAccount
        fields = ('id',
                  'code',
                  'master_account',
                  'name',
                  'description',
                  'category',
                  'is_active')

    def create(self, data):
        return GLAccount.objects.create_serialized(
            gl_account_dict=data)

    def update(self, obj, data):
        return GLAccount.objects.update(
            gl_account=obj,
            gl_account_dict=data)


class GLAccountDisplaySerializer(serializers.ModelSerializer):

    category = GLAccountCategorySerializer()
    name = LocalizedTextSerializer(many=True)
    description = LocalizedTextSerializer(many=True)
    modified_by = serializers.PrimaryKeyRelatedField(read_only=True)
    created_by = serializers.PrimaryKeyRelatedField(read_only=True)
    created_date = serializers.DateTimeField(required=False)
    modified_date = serializers.DateTimeField(required=False)

    class Meta:
        model = GLAccount
        fields = ('id',
                  'code',
                  'master_account',
                  'name',
                  'description',
                  'category',
                  'is_active',
                  'modified_by',
                  'created_by',
                  'modified_date',
                  'created_date')


class GLAccountDisplayShortSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    category = GLAccountCategoryShortSerializer()

    class Meta:
        model = GLAccount
        fields = (
            'id',
            'code',
            'name',
            'category',
            'is_active')

    def get_name(self, obj):
        return obj.get_name(
            self.context['request'].query_params.get('locale', None))

#    def to_representation(self, instance):
 #       serializer = GLAccountCategoryShortSerializer(instance, context=self.context)
  #      return serializer.data


class GLAccountDisplayMiniSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    classification = serializers.SerializerMethodField()

    class Meta:
        model = GLAccount
        fields = (
            'id',
            'code',
            'name',
            'classification')

    def get_name(self, obj):
        return obj.get_name(
            self.context['request'].query_params.get('locale', None))

    def get_classification(self, obj):
        return obj.get_classification()

