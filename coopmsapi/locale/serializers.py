from rest_framework import (
        serializers)

from .models import LocalizedText


class LocalizedTextSerializer(serializers.ModelSerializer):
    locale = serializers.CharField(max_length=2)
    text = serializers.CharField(max_length=512)

    class Meta:
        model = LocalizedText
        fields = (
            'locale',
            'text'
            )
