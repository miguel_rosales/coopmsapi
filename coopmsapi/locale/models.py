from django.db import models


class LocalizedTextManager(models.Manager):

    def create(self, locale, text):
        if type(locale) is Locale:
            new_text = LocalizedText(locale=locale.locale, text=text)
        else:
            new_text = LocalizedText(locale=locale, text=text)
        new_text.save()
        return new_text


class Locale(models.Model):
    locale = models.CharField(max_length=2, unique=True)


class LocalizedText(models.Model):
    locale = models.CharField(max_length=2)
    text = models.CharField(max_length=512)
    objects = LocalizedTextManager()


def validate_locale(*args):
    list_of_locales = Locale.objects.all().values('locale')
    for dict_ in args:
        if dict_['locale'] not in list_of_locales:
            return False
    return True
