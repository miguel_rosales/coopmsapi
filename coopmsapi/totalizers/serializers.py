from rest_framework import serializers
from .models import Balance


class BalanceDisplaySerializer(serializers.ModelSerializer):
    balance_type = serializers.CharField(max_length=16)
    amount = serializers.DecimalField(max_digits=16, decimal_places=2)

    class Meta:
        model = Balance
        fields = (
            'id',
            'balance_type',
            'modified_date',
            'amount')


class BalanceDisplayShortSerializer():
    amount = serializers.DecimalField(max_digits=16, decimal_places=2)

    class Meta:
        model = Balance
        fields = (
            'amount')
