from django.db import models
from django.utils import timezone


class BalanceManager(models.Manager):

    def create(self, balance_type):
        balance = Balance(
            balance_type=balance_type)
        balance.save()
        return balance


class Balance(models.Model):
    CLIENT = 'CLIENT'
    MEMBER = 'MEMBER'
    SUPPLIER = 'SUPPLIER'

    TYPES = (
        (CLIENT, 'CLIENT'),
        (MEMBER, 'MEMBER'),
        (SUPPLIER, 'SUPPLIER')
        )

    balance_type = models.CharField(max_length=16, choices=TYPES)
    amount = models.DecimalField(
        max_digits=16,
        decimal_places=2,
        default=0)

    objects = BalanceManager()

    modified_date = models.DateTimeField()

    def save(self, *args, **kwargs):
        self.modified_date = timezone.now()
        return super(Balance, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.id) + ' ' + str(self.balance_type)
