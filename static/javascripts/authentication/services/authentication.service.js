/**
* Authentication
* @namespace thinkster.authentication.services
*/
(function () {
  'use strict';

  angular
    .module('thinkster.authentication.services')
    .factory('Authentication', Authentication);

  Authentication.$inject = ['$cookies', '$http'];

  /**
  * @namespace Authentication
  * @returns {Factory}
  */
  function Authentication($cookies, $http) {
    /**
    * @name Authentication
    * @desc The Factory to be returned
    */
    var Authentication = {
		getAuthenticatedAccount: getAuthenticatedAccount,
		isAuthenticated: isAuthenticated,
		login: login,
		register: register,
		setAuthenticatedAccount: setAuthenticatedAccount,
		unauthenticate: unauthenticate
    };

    return Authentication;

    ////////////////////


    /**
    * @name register
    * @desc Try to register a new user
    * @param {string} username The username entered by the user
    * @param {string} password The password entered by the user
    * @param {string} email The email entered by the user
    * @returns {Promise}
    * @memberOf thinkster.authentication.services.Authentication
    */
    function register(email, password, username) {
      return $http.post('/auth/token/', {
        username: username,
        password: password,
        email: email
      });

    }

	function login(email, password) {
	  return $http.post('/auth/token/', {
		email: email, password: password
	  }).then(loginSuccessFn,loginErrorFn);

		function loginSuccessFn(data, status, headers, config) {
			Authentication.setAuthenticatedAccount(data.data);

			window.location = '/';
		  }

		  /**
		   * @name loginErrorFn
		   * @desc Log "Epic failure!" to the console
		   */
		  function loginErrorFn(data, status, headers, config) {
			console.error('Epic failure!');
		  }
	}

	function getAuthenticatedAccount() {
	  if (!$cookies.authenticatedAccount) {
		return;
	  }

	  return JSON.parse($cookies.authenticatedAccount);
	}


	function isAuthenticated() {
	  return !!$cookies.authenticatedAccount;
	}

	function setAuthenticatedAccount(account) {
	  $cookies.authenticatedAccount = JSON.stringify(account);
	}

	function unauthenticate() {
	  delete $cookies.authenticatedAccount;
	}


  }
})();
