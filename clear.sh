#!/bin/bash
#clear migrations(first backup db/migrations
#cp -r ~/venvs/coopmsapi/lib/python3.4/site-packages/django/db/migrations ~/venvs/coopmsapi/lib/python3.4/site-packages/django/db/migrations_bk
#find ~/venvs/coopmsapi -name migrations -exec rm -rf {} \;
#find ~/venvs/coopmsapi -name __pycache__ -exec rm -rf {} \;
#cp -r ~/venvs/coopmsapi/lib/python3.4/site-packages/django/db/migrations_bk ~/venvs/coopmsapi/lib/python3.4/site-packages/django/db/migrations
#find ~/workspace/coopmsapi -name migrations -exec rm -rf {} \;
#find ~/workspace/coopmsapi -name __pycache__ -exec rm -rf {} \;
#psql -c 'drop database coopmsdev;'
#psql -c 'create database coopmsdev;'
#source ~/venvs/coopmsapi/bin/activate
python manage.py makemigrations contenttypes
python manage.py makemigrations clients
python manage.py makemigrations users
python manage.py makemigrations totalizers
python manage.py makemigrations sessions
python manage.py makemigrations auth
python manage.py makemigrations admin
python manage.py makemigrations oauth2_provider
python manage.py makemigrations guardian
python manage.py makemigrations locale
python manage.py makemigrations accounting
python manage.py makemigrations coop
python manage.py makemigrations budget
python manage.py makemigrations receivables
python manage.py makemigrations payables
python manage.py migrate_schemas

